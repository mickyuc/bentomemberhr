import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import { fonts, margin } from '../assets/styles';

import GroupAIcon from './../assets/images/category/corpA_off.svg';
import GroupBIcon from './../assets/images/category/corpB_off.svg';
import GroupCIcon from './../assets/images/category/corpC_off.svg';
import GroupDIcon from './../assets/images/category/corpD_off.svg';

import ActiveGroupAIcon from './../assets/images/category/corpA_on.svg';
import ActiveGroupBIcon from './../assets/images/category/corpB_on.svg';
import ActiveGroupCIcon from './../assets/images/category/corpC_on.svg';
import ActiveGroupDIcon from './../assets/images/category/corpD_on.svg';

import CheckIcon from './../assets/images/check-circle.svg';

TouchableOpacity.defaultProps = { activeOpacity: 0.6 };

export default class CategoryItem extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
    }
    chooseCategory() {
        this.props.customClickEvent(!this.props.active, this.props.category)
    }
    render() {
        return (
                <TouchableOpacity style={[styles.mainPanelSection]}
                onPress={() => this.chooseCategory()}>
                    {
                        !this.props.active ?
                        ( this.props.category == 1 ?
                        <GroupAIcon width={130} height={130} /> :
                        (
                            this.props.category == 2 ?
                            <GroupBIcon width={130} height={130} /> :
                            (
                                this.props.category == 3 ?
                                <GroupCIcon width={130} height={130} /> :
                                <GroupDIcon width={130} height={130} />
                            )
                        ) ) :
                        ( this.props.category == 1 ?
                            <ActiveGroupAIcon width={130} height={130} /> :
                            (
                                this.props.category == 2 ?
                                <ActiveGroupBIcon width={130} height={130} /> :
                                (
                                    this.props.category == 3 ?
                                    <ActiveGroupCIcon width={130} height={130} /> :
                                    <ActiveGroupDIcon width={130} height={130} />
                                )
                        ) )
                    }
                    <Text style={[fonts.size20, margin.mt4, this.props.active ? styles.activeTextColor : styles.defautTextColor]}>
                        { this.props.title }
                    </Text>
                </TouchableOpacity>
        )
    }
}

const styles = StyleSheet.create({
    mainPanelSection: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'flex-end',
      width: '50%',
      // height: 180,
      paddingVertical: 30,
      position: 'relative'
    },
    activeTextColor: {
      color: '#00BAFF'
    },
    defautTextColor: {
      color: 'black'
    },
    btnText: {
      padding: 18,
      width: "100%",
      color: '#fff',
      textAlign: 'center',
    },
    activeCheck: {
      position: 'absolute',
      left: 10,
      top: 30
    }
  });