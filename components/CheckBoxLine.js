import React from 'react';
import { StyleSheet, View } from 'react-native';
import { fonts, margin } from '../assets/styles';
import { CheckBox } from 'react-native-elements';

export default class CheckBoxLine extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isSelected: false
        };
    }
    componentDidMount(){
    }
    setSelection() {
        this.props.parentMethod(this.props.id, !this.state.isSelected)
        this.setState({isSelected: !this.state.isSelected})
    }
    render() {
        return (
            <View style={[styles.location_d, margin.mr12, styles.location_info, styles.location_bottom, styles.checkboxContainer]}>
                <CheckBox
                    title={this.props.title}
                    checked={this.state.isSelected}
                    containerStyle={{ backgroundColor: 'white' , borderWidth: 0 , marginLeft: -8}}
                    textStyle={[{ color: '#2a2e43', fontWeight: '400' }, fonts.size15]}
                    uncheckedColor='#707070'
                    size={22}
                    onPress={() => this.setSelection()}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    location_info: {
      marginLeft: 50
    },
    location_bottom: {
      borderBottomWidth: 1,
      borderColor: '#C3C9D6'
    },
    location_d: {
      paddingVertical: 6,
    }
  });