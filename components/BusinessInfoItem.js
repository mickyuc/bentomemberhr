import React from 'react';
import { StyleSheet, View, Text, Image} from 'react-native';
import { shared, fonts, margin, normalize } from '../assets/styles';
import { EvilIcons } from '@expo/vector-icons';
import Images from "./../assets/Images";
import BadgeComponent from './../components/BadgeComponent'
import BuildingIcon from './../assets/images/building.svg';
export default class ActionButton extends React.Component {
    constructor(props) {
        super(props);
    }
    renderText() {
        return (
            <View style={[styles.business_info_item, styles.flex_end, margin.px1, margin.py4]}>
                <View style={{width: '90%'}}>
                    <Text style={[fonts.size12, {color: '#2a2e43'}]}>{this.props.item}</Text>
                    <Text style={[fonts.size16, margin.mt2, {color: '#2a2e43'}]}>{this.props.value}</Text>
                </View>
                <EvilIcons
                    name='pencil'
                    size={30}
                    color='black'
                />
            </View>
        );
    }
    renderImage() {
        return (
            <View style={[styles.business_info_item, styles.flex_end, margin.px1, margin.py4]}>
                <View style={styles.business_img_item}>
                    <BuildingIcon width={normalize(32.58)} height={normalize(40)} />
                    <Text style={[fonts.size16, margin.mt2, margin.ml4, {color: '#2a2e43'}]}>{this.props.item}</Text>
                </View>
                <EvilIcons
                    name='pencil'
                    size={30}
                    color='black'
                />
            </View>
        );
    }
    renderBusinessItem() {
        return (
            <View style={[styles.business_info_item, styles.flex_center, margin.px1, margin.py4]}>
                <View style={styles.business_img_item}>
                    <BadgeComponent
                        width={61}
                        height={23}
                        bkColor={this.props.bkColor}
                        text={this.props.text}
                        color='white'
                        borderRadius = {0}
                    />
                    <Text style={[fonts.size16, margin.ml3, {color: '#454f63'}]}>{this.props.item}</Text>
                </View>
                <EvilIcons
                    name='pencil'
                    size={30}
                    color='black'
                />
            </View>
        );
    }
    render() {
        return (
            this.props.type == 'text' ?
                this.renderText() :
                ( this.props.type == 'business' ?
                this.renderBusinessItem() :
                this.renderImage())
        );
    }
}

const styles = StyleSheet.create({
    business_info_item: {
        display: 'flex' ,
        flexDirection: 'row' ,
        justifyContent: 'space-between' ,
        alignItems: 'flex-end' ,
        borderBottomWidth: 1,
        borderColor: '#F4F4F6'
    },
    flex_end: {
        alignItems: 'flex-end'
    },
    flex_center: {
        alignItems: 'center'
    },
    business_img_item: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center'
    },
    building_img: {
        width: normalize(32.58),
        height: normalize(40)
    }
});