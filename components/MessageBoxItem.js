import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { fonts, margin, normalize } from '../assets/styles';
import UserIcon from './../components/UseIcon';
export default class MessageBoxItem extends React.Component {
    constructor(props) {
        super(props);
    }
    renderSend() {
        return (
            <View style={[styles.message_box_item,
                margin.pt4,
                margin.pb4,
                margin.pl6,
                margin.pr4,
                margin.ml3,
                margin.mr10,
                margin.mt3,
                { backgroundColor: '#5773ff' }]}>
                <View style={styles.message_avatar}>
                    <UserIcon width={32} height={30} bkColor='#82A3EA' size={18} />
                </View>
                <Text style={[{ color: 'white' }, fonts.size14]}>
                    { this.props.message }
                </Text>
            </View>
        )
    }
    renderReceive() {
        return (
            <View style={[styles.message_box_item,
                margin.pt4,
                margin.pb4,
                margin.pl6,
                margin.pr4,
                margin.ml10,
                margin.mr3,
                margin.mt3,
                { backgroundColor: '#78849e' }]}>
                <View style={styles.message_avatar_receive}>
                    <UserIcon width={32} height={30} image={true} />
                </View>
                <Text style={[{ color: 'white' }, fonts.size14]}>
                    { this.props.message }
                </Text>
            </View>
        )
    }
    render() {
        return (
            <View>
                {
                    this.props.hasOwnProperty('send') && this.props.send ?
                    this.renderSend()
                    :
                    this.renderReceive()
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    message_box_item: {
        borderRadius: normalize(12),
        position: 'relative'
    },
    message_avatar: {
        left: -normalize(12),
        top: -normalize(12),
        position: 'absolute'
    },
    message_avatar_receive: {
        right: -normalize(12),
        top: -normalize(12),
        position: 'absolute'
    }
});