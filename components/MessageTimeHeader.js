import React from 'react';
import { View, Text } from 'react-native';
import { fonts } from '../assets/styles';
export default class MessageTimeHeader extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <View>
                <Text style={[fonts.size12, {color: '#78849e', textAlign: 'center'}]}>
                    { this.props.time }
                </Text>
            </View>
        );
    }
}