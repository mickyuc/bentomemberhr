import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, TouchableHighlight} from 'react-native';
import { shared, fonts, margin, normalize } from '../assets/styles';
import Images from "./../assets/Images";
import PlusButton from '../components/PlusButton';
import {Actions} from 'react-native-router-flux';
import { ASSET_URL } from './../constants/Global';
let asset_url = ASSET_URL;
TouchableOpacity.defaultProps = { activeOpacity: 0.6 };
export default class CompanyItem extends React.Component {
    constructor(props){
        super(props);
    }
    componentDidMount(){
    }
    businessScreen(company_id){
        Actions.push('businessscreen', {
            'companyId': company_id
        })
    }
    render() {
        return(
            <View>
                <TouchableOpacity style={ [{ display: 'flex' , flexDirection: 'row',
                        borderWidth: 1, borderColor: '#C3C9D6', alignItems: 'flex-start' },
                        margin.py4,
                        margin.px2] }
                        onPress={() => this.businessScreen(this.props.companyId)}>
                    <View style={[ { width: normalize(83), height: normalize(76) } ]}>
                        <Image source={{ uri: asset_url + this.props.image}} style={styles.companyImage} />
                    </View>
                    <View style={[{ flexShrink: 1 }, margin.ml3]}>
                        <View style={[{ borderBottomColor: 'black', borderBottomWidth: 1 }, margin.pb4]}>
                            <Text style={[fonts.size15]}>{ this.props.name }</Text>
                            <Text style={[fonts.size12, margin.mt2]}> { this.props.address }</Text>
                        </View>
                        <View style={[{display: 'flex', flexDirection: 'row', alignItems: 'center'}, margin.mt4]}>
                            <PlusButton
                                color="#3497FD"
                                width={34}
                                height={34}
                                size={20}
                                type="phone"
                                borderRadius={8}
                            />
                            <Text style={[fonts.size18, margin.ml3]}>{ this.props.phone }</Text>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    companyImage: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain'
    }
});
