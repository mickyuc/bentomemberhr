import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import { Button } from 'native-base';
import { Feather } from '@expo/vector-icons';
import { normalize } from '../assets/styles'
TouchableOpacity.defaultProps = { activeOpacity: 0.8 };

export default class PlusButton extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
    }
    buttonBKColor() {
        if(this.props.hasOwnProperty('color'))
            return {
                backgroundColor: this.props.color
            }
        else
            return {
                backgroundColor: '#82a3ea'
            }
    }
    buttonSize() {
        if(this.props.hasOwnProperty('borderRadius')) {
            return {
                width: normalize(this.props.width),
                height: normalize(this.props.height),
                borderRadius: normalize(this.props.borderRadius)
            }
        }
        else {
            return {
                width: normalize(this.props.width),
                height: normalize(this.props.height),
                borderRadius: normalize(this.props.width)
            }
        }
    }
    renderPlus() {
        return (
            <View>
                <Feather
                        name='plus'
                        size={this.props.hasOwnProperty('size')?this.props.size:30}
                        color='white'
                />
            </View>
        )
    }
    renderPhone() {
        return (
            <View>
                <Feather
                        name='phone'
                        size={this.props.hasOwnProperty('size')?this.props.size:30}
                        color='white'
                />
            </View>
        )
    }
    render() {
        return (
            <View>
                <Button style={[this.buttonBKColor(), this.buttonSize(), styles.contentButton]}>
                    {
                        this.props.hasOwnProperty('type') && this.props.type == 'phone' ?
                        this.renderPhone() :
                        this.renderPlus()
                    }
                </Button>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    contentButton: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }
});
