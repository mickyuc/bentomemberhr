import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Linking } from 'react-native';
import { fonts, margin, normalize } from '../assets/styles';
import PlusButton from '../components/PlusButton';
import Layout from '../constants/Layout';
import { Feather } from '@expo/vector-icons';
export default class OrderItem extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
    }
    statusColor() {
        if(this.props.status < 3)
            return {
                backgroundColor: '#fff'
            }
        else if(this.props.status == 3 || this.props.status == 4)
            return {
                backgroundColor: '#f5e200'
            }
        else if(this.props.status == 5 || this.props.status == 6)
            return {
                backgroundColor: '#62bc56'
            }
        else if(this.props.status > 6)
            return {
                backgroundColor: '#acacac'
            }
    }
    statusTextColor() {
        if(this.props.status == 'waiting')
            return {
                color: '#000'
            }
        else if(this.props.status == 'inprogress')
            return {
                color: '#ffb100'
            }
        else if(this.props.status == 'delivered')
            return {
                color: '#62bc56'
            }
        else if(this.props.status == 'undelivered')
            return {
                color: '#acacac'
            }
    }
    progressBar() {
        if(this.props.status == 'waiting' || this.props.status == 'inprogress') {
            return {
                borderLeftWidth: 0
            }
        }
        else {
            return {
                borderLeftWidth: 1,
                borderLeftColor: '#707070'
            }
        }
    }
    render() {
        return (
            <View style={[margin.pl6 , margin.pb3, styles.order_item, this.progressBar()]}>
                <View style={[styles.order_status, this.statusColor()]}>
                </View>
                <View style={[styles.order_process, margin.pb3]}>
                    <View>
                        <Text style={[fonts.size16, {color: '#454F63', width: Layout.window.width - 200}]}>
                            { this.props.business }
                        </Text>
                        <Text style={[fonts.size12, margin.mt2, {color: '#959DAD'}]}>
                            { this.props.time }
                        </Text>
                    </View>
                    <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={[fonts.size15, margin.mr8, this.statusTextColor(), {width: 60}]}>
                            { this.props.statusText }
                        </Text>
                        <TouchableOpacity onPress={() => Linking.openURL(`tel:${this.props.phone}`)} style={{backgroundColor: '#3497FD', width: 34, height: 34, borderRadius: 8, justifyContent: 'center', alignItems: 'center'}}>
                            <Feather
                                name='phone'
                                size={20}
                                color='white'
                            />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    order_item: {
        position: 'relative'
    },
    order_status: {
        width: normalize(16),
        height: normalize(16),
        borderColor: '#707070',
        borderWidth: 1,
        borderRadius: normalize(4),
        position: 'absolute',
        left: -normalize(8)
    },
    order_process: {
        display: 'flex', flexDirection: 'row',
        justifyContent: 'space-between', alignItems: 'center',
        borderBottomWidth: 1, borderColor: '#F4F4F6'
    }
});