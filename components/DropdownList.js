import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { fonts, margin, normalize } from '../assets/styles';
import DropDownPicker from 'react-native-dropdown-picker';
export default class DropdownList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            dropdownItem: null,
            isVisible: false
        }
        this.changeVisibility = this.changeVisibility.bind(this);
    }
    changeVisibility(state) {
        this.props.parentMethod(this.props.item_id)
    }
    zIndex() {
        return {
            zIndex: this.props.zIndex
        }
    }
    setVisibleState() {
        this.setState({
            isVisible: true
        });
    }
    setUnVisibleState() {
        this.setState({
            isVisible: false
        });
    }
    render() {
        return(
            <View style={this.zIndex()}>
                <Text style={[fonts.size15, {color: '#2A2E43'}, margin.mb3]}>
                    { this.props.title }
                </Text>
                <DropDownPicker
                    items={ this.props.items }
                    containerStyle={ styles.container_style }
                    style={ styles.style }
                    dropDownStyle={ styles.dropdown_style }
                    onChangeItem={item => this.setState({
                        dropdownItem: item.value
                    })}
                    labelStyle={ styles.label_style }
                    placeholderStyle={ styles.placeholder_style }
                    placeholder={this.props.placeholder}
                    zIndex={4000}

                    isVisible={this.state.isVisible}
                    onOpen={() => this.changeVisibility()}
                    onClose={() => this.setState({
                        isVisible: false
                    })}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container_style: {
      height: normalize(52)
    },
    style: {
        backgroundColor: '#fff',
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        borderBottomLeftRadius: 12,
        borderBottomRightRadius: 12
    },
    dropdown_style: {
        backgroundColor: 'white'
    },
    label_style: {
        fontSize: 15,
        color: '#2a2e43'
    },
    placeholder_style: {
        color: '#78849e'
    }
});