import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, TouchableHighlight } from 'react-native';
import { fonts, margin, normalize } from '../assets/styles';
import { Feather, Ionicons, EvilIcons } from '@expo/vector-icons';
import store from './../store/configuteStore';
import { connect } from "react-redux";
import { setUser } from './../actions';
import { Actions } from 'react-native-router-flux';
TouchableOpacity.defaultProps = { activeOpacity: 0.6 };

class ClipComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          collapseOff: true,
          hide: false
        }
    }
    onCollapse() {
      this.setState({
        collapseOff: !this.state.collapseOff
      })
    }
    onClose() {
      this.setState({
        hide: !this.state.hide
      })
      let user = store.getState().user;
      if(this.props.type == 1){
        //remove business
        let business_list = user.business_list;
        if(business_list != null)
          business_list.splice(this.props.key, 1)
        this.props.setUser({
            username: user.username,
            token: user.token,
            route_list: user.route_list,
            business_list: business_list
        });
      }
      else {
        let route_list = user.route_list;
        if(route_list != null)
          route_list.splice(this.props.key, 1)
        this.props.setUser({
            username: user.username,
            token: user.token,
            route_list: route_list,
            business_list: user.business_list
        });
      }
    }
    onSearch() {
      if(this.props.type == 1) {
        Actions.push(this.props.attr.type, {
          'category_id': this.props.attr.category_id,
          'state_id': this.props.attr.state_id,
          'cityIds': this.props.attr.cityIds,
          'businessTypeIds': this.props.attr.businessTypeIds,
          'predictTypeId': this.props.attr.predictTypeId,
          'state_name': this.props.attr.state_name
        })
      }
      else {
        Actions.push('mapshow', {ids: this.props.attr.count})
      }
    }
    render() {
        return (
            !this.state.hide ?
            (<View style={[{backgroundColor: 'white'} , margin.pt3 , margin.pb6 , margin.mt5 , {position: 'relative'}]}>
                <TouchableOpacity style={[styles.closeIcon]}
                onPress={() => this.onClose()}>
                  <Ionicons
                    name="md-close"
                    size={28}
                    color='#2A2E43'
                  />
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.collapseIcon]}
                  onPress={() => this.onCollapse()}>
                    {
                      this.state.collapseOff ?
                        <EvilIcons
                          name='chevron-down'
                          size={48}
                          color='#2A2E43'
                        /> :
                        <EvilIcons
                          name='chevron-up'
                          size={48}
                          color='#2A2E43'
                        />
                    }
                </TouchableOpacity>
                <View style={[{display: 'flex', flexDirection: 'row', alignItems: 'center'}, margin.ml5]}>
                  <TouchableOpacity
                  onPress={() => this.onSearch()}>
                    <Feather
                      name="search"
                      size={24}
                      color='#78849E'
                      style={margin.mr3}
                    />
                  </TouchableOpacity>
                  <Text style={[fonts.size15 , {color:'#2a2e43'}]}>2020/06/20</Text>
                </View>
                    {
                      this.props.type == 1?
                      <View style={[margin.mt6, margin.ml9, { width: '75%' }]}>
                        <Text style={[fonts.size14, margin.mb2]} numberOfLines={this.state.collapseOff?1:10}>区分：{this.props.category_name}</Text>
                        <Text style={[fonts.size14, margin.mb2]} numberOfLines={this.state.collapseOff?1:10}>地域：{this.props.citynames}</Text>
                        <Text style={[fonts.size14, margin.mb2]} numberOfLines={this.state.collapseOff?1:10}>業種：{this.props.businesstypenames}</Text>
                        <Text style={[fonts.size14]} numberOfLines={this.state.collapseOff?1:10}>見込：{this.props.predictnames}</Text>
                      </View> :
                      <View style={[margin.mt6, margin.ml9, { width: '75%' }]}>
                        <View style={{display: 'flex', flexDirection: 'row', width: '70%'}}>
                            <Text style={[fonts.size14, margin.mb2]} numberOfLines={this.state.collapseOff?1:10}>営業所名：</Text>
                            <Text style={[fonts.size14, margin.mb2]} numberOfLines={this.state.collapseOff?1:10}>{this.props.bname}</Text>
                        </View>
                        <View style={{display: 'flex', flexDirection: 'row', width: '70%'}}>
                          <Text style={[fonts.size14]} numberOfLines={this.state.collapseOff?1:10}>ルート名：</Text>
                          <Text style={[fonts.size14]} numberOfLines={this.state.collapseOff?1:10}>
                            {this.props.routes}
                          </Text>
                        </View>
                      </View>
                    }

            </View> )
            :
            (<View></View>)
        )
    }
}
const mapDispatchToProps = dispatch => {
  return {
      setUser : user => { dispatch(setUser(user)) }
  }
}

export default connect(null,mapDispatchToProps)(ClipComponent)

const styles = StyleSheet.create({
    closeIcon: {
      right: normalize(10),
      top: normalize(10),
      position: 'absolute',
      padding: normalize(10),
      zIndex: 1000
    },
    collapseIcon: {
      right: normalize(7),
      bottom: normalize(7),
      position: 'absolute'
    }
});