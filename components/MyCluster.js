import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
export const MyCluster = (props) => {
  const { count } = props;
  return (
    <View style={styles.cluster}>
      <Text style={{ fontSize: 16 }}>{count}</Text>
    </View>
  );
};
const styles = StyleSheet.create({
 cluster: {
    width: 50,
    height: 50,
    borderRadius: 25,
    backgroundColor: 'white',
    borderWidth: 5,
    borderColor: '#6B6FDE',
    justifyContent: 'center',
    alignItems: 'center'
 }
});