import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import { fonts, margin, normalize } from '../assets/styles';
import UserIcon from './../components/UseIcon';
import { Badge } from 'react-native-elements'
import { _e } from './../lang';

export default class MessageItem extends React.Component {
    constructor(props) {
        super(props);
    }

    goDetail = () => {
        const { navigation } = this.props;
        const messageid = this.props.message.id;
        if(messageid > 0) {
            navigation.navigate('message_detail', {messageid});
        }
    }

    render() {
        const propData = this.props.message;
        return (
            <TouchableOpacity onPress={() => this.goDetail()}>
                <View style={[styles.messageItem, margin.py6]}>
                    {
                        propData.id > 0 ? 
                        <UserIcon width={62} height={60} bkColor='#82A3EA' size={36} />
                        : null
                    }
                    <View style={[{display: 'flex', marginLeft: 16, flexShrink: 1}]}>
                        {
                            propData.title == "none" ? null
                            : <Text style={[fonts.size16, {color: '#454F63'}]}>{propData.title}</Text>
                        }
                        {
                            propData.title == "none" ? 
                            <Text style={[fonts.size14, {color: '#78849E', marginTop: 8, fontSize: 20, paddingTop: 20, textAlign: 'center'}]}>
                                {_e.NoMessages}
                            </Text> : 
                            <Text style={[fonts.size14, {color: '#78849E', marginTop: 8}]}>
                                {propData.content}
                            </Text>
                        }
                    </View>
                    {
                        propData.status == 1 ? 
                        <Badge status="error" containerStyle={styles.unreadBadge} />
                        : null
                    }
                </View>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    messageItem: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'flex-start',
        width: '100%',
        borderBottomWidth: 1,
        borderBottomColor: '#F4F4F6',
        position: 'relative'
    },
    unreadBadge: {
        position: 'absolute',
        right: normalize(0) ,
        top: normalize(24)
    }
});
