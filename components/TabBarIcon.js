import { Ionicons, SimpleLineIcons, Feather, MaterialIcons, AntDesign, Entypo } from '@expo/vector-icons';
import * as React from 'react';
import { StyleSheet , View , Text, Platform } from 'react-native';
import Colors from '../constants/Colors';

export default function TabBarIcon(props) {
  if(!props.hasOwnProperty('type') || props.type == 'Ionicons')
    return (
      <View style={styles.tab_item}>
        <Ionicons
          name={props.name}
          size={Platform.OS == 'ios' ? 30 : 28}
          style={{ marginBottom: -3, marginTop: 12 }}
          color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
        />
        <Text style={{color: props.focused ? Colors.tabIconSelected : Colors.tabIconDefault , marginTop: 4}}>{props.title}</Text>
      </View>
    );
  else if(props.type == 'SimpleLineIcons')
      return (
        <View style={styles.tab_item}>
          <SimpleLineIcons
            name={props.name}
            size={Platform.OS == 'ios' ? 30 : 28}
            style={{ marginBottom: -3 , marginTop: 12}}
            color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
          <Text style={{color: props.focused ? Colors.tabIconSelected : Colors.tabIconDefault , marginTop: 4}}>{props.title}</Text>
        </View>
      );
  else if(props.type == 'Feather')
      return (
        <View style={styles.tab_item}>
          <Feather
            name={props.name}
            size={Platform.OS == 'ios' ? 30 : 28}
            style={{ marginBottom: -3, marginTop: 12 }}
            color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
          <Text style={{color: props.focused ? Colors.tabIconSelected : Colors.tabIconDefault , marginTop: 4}}>{props.title}</Text>
        </View>
      );
  else if(props.type == 'MaterialIcons')
      return (
        <View style={styles.tab_item}>
          <MaterialIcons
            name={props.name}
            size={Platform.OS == 'ios' ? 30 : 28}
            style={{ marginBottom: -3, marginTop: 12 }}
            color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
          <Text style={{color: props.focused ? Colors.tabIconSelected : Colors.tabIconDefault , marginTop: 4}}>{props.title}</Text>
        </View>
      );
  else if(props.type == 'AntDesign')
      return (
        <View style={styles.tab_item}>
          <AntDesign
            name={props.name}
            size={Platform.OS == 'ios' ? 30 : 28}
            style={{ marginBottom: -3, marginTop: 12 }}
            color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
          <Text style={{color: props.focused ? Colors.tabIconSelected : Colors.tabIconDefault , marginTop: 4}}>{props.title}</Text>
        </View>
      );
  else if(props.type == 'Entypo')
      return (
        <View style={styles.tab_item}>
          <Entypo
            name={props.name}
            size={Platform.OS == 'ios' ? 30 : 28}
            style={{ marginBottom: -3, marginTop: 12,     transform: [{ rotate: '270deg'}] }}
            color={props.focused ? Colors.tabIconSelected : Colors.tabIconDefault}
          />
          <Text style={{color: props.focused ? Colors.tabIconSelected : Colors.tabIconDefault , marginTop: 4}}>{props.title}</Text>
        </View>
      );
}

const styles = StyleSheet.create({
  tab_item: {
    display:'flex',
    alignItems: 'center'
  }
});