import React from 'react';
import { Feather } from '@expo/vector-icons';
import { fonts, margin, normalize } from '../assets/styles';
import { View } from 'native-base';
import { Image } from 'react-native';
import Images from "./../assets/Images";
export default class UserIcon extends React.Component {
    constructor(props) {
        super(props);
    }
    bkSize() {
        return {
            width: normalize(this.props.width),
            height: normalize(this.props.height)
        }
    }
    bkColor() {
        return {
            backgroundColor: this.props.bkColor
        }
    }
    renderUserIcon() {
        return (
            <View style={[this.bkSize(), this.bkColor(), {borderRadius: 8, display: 'flex', alignItems: 'center', justifyContent: 'center'}]}>
                <Feather
                            name='user'
                            size={this.props.size}
                            color='white'
                />
            </View>
        );
    }
    renderUserAvatar() {
        return (
            <View>
                <Image
                            source={Images.avatar_img}
                            style={this.bkSize()}
                />
            </View>
        );
    }
    render() {
        return (
            <View>
                {
                    this.props.hasOwnProperty("image") && this.props.image ?
                    this.renderUserAvatar() :
                    this.renderUserIcon()
                }
            </View>
        );
    }
}