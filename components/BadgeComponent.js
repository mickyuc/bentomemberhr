import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { fonts, margin, normalize } from '../assets/styles';

export default class BadgeComponent extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
    }
    render() {
        return (
            <View style={{ width: normalize(this.props.width),
                           height: normalize(this.props.height),
                           backgroundColor: this.props.bkColor,
                           borderRadius: normalize(this.props.hasOwnProperty('borderRadius') ? this.props.borderRadius : this.props.width),
                           display: 'flex',
                           alignItems: 'center',
                           justifyContent: 'center' }}>
                <Text style={[fonts.size12, {color: this.props.color}]}>
                    { this.props.text }
                </Text>
            </View>
        )
    }
}



