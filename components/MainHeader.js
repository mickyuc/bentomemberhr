import React from 'react';
import { StatusBar, TouchableOpacity, StyleSheet, Platform } from 'react-native';
import { Header, View , Text} from 'native-base';
import { EvilIcons, SimpleLineIcons } from '@expo/vector-icons';
import { shared, normalize } from './../assets/styles';
import Constants from "expo-constants";
import { Actions } from 'react-native-router-flux';
export default class MainHeader extends React.Component {
    constructor(props) {
        super(props);
    }
    backButtonPressed() {
    }
    headerBKColor() {
        if(this.props.hasOwnProperty('transparent') && this.props.transparent)
            return {
                backgroundColor: 'transparent',
                zIndex: 1
            }
        else
            return {
                backgroundColor: 'white'
            }
    }
    render() {
        return (
            <View>
                <View style={[this.headerBKColor(),
                                styles.header ,
                                {display:'flex' , flexDirection:'row' , alignItems:'center' , justifyContent: 'center'}]} 
                                androidStatusBarColor='#27cccd' iosBarStyle="dark-content">
                    <View style={{ position: 'absolute' , left: 10 }}>
                        <TouchableOpacity onPress={() => Actions.pop()} style={{ alignItems: 'flex-start' }} >
                            <EvilIcons
                                name='chevron-left'
                                size={42}
                                color='black'
                                />
                        </TouchableOpacity>
                    </View>
                    <View style={{textAlign: 'center', width: '70%'}}>
                        <Text numberOfLines={1} style={[shared.userHeaderText, {textAlign: 'center'}]}>{this.props.title}</Text>
                    </View>
                    <View style={{ position: 'absolute' , right: 20 }}>
                        <TouchableOpacity onPress={() => Actions.reset("root")} style={{ alignItems: 'flex-start' }} >
                            <SimpleLineIcons
                                name='home'
                                size={ Platform.OS=='ios' ? 24 : 20}
                                color='black'
                                />
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
         )
    }
}

const styles = StyleSheet.create({
    header: {
        height:60,
        zIndex:10001,
        position: 'relative',
        marginTop:  Platform.OS === 'android' ?  0 : Constants.statusBarHeight
    },
});