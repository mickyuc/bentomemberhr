import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, Image} from 'react-native';
import { Ionicons, SimpleLineIcons, Feather, MaterialIcons, AntDesign, FontAwesome } from '@expo/vector-icons';
import { fonts } from '../assets/styles';
import Images from "./../assets/Images";
TouchableOpacity.defaultProps = { activeOpacity: 0.8 };
export default class ActionButton extends React.Component {
    constructor(props) {
        super(props);
    }
    buttonBKColor() {
        if(this.props.hasOwnProperty('color'))
            return {
                backgroundColor: this.props.color
            }
        else
            return {
                backgroundColor: '#3497FD'
            }
    }
    buttonBorderRadius() {
        if(this.props.hasOwnProperty('borderRadius'))
            return {
                borderRadius: this.props.borderRadius
            }
        else
            return {
                borderRadius: 0
            }
    }
    actionMethod() {
       this.props.customClickEvent()
    }
    renderIcon() {
        if(this.props.hasOwnProperty('iconType')) {
            switch(this.props.iconType) {
                case 'SimpleLineIcons':
                    return (
                       <View style={{marginRight: 8}}>
                            <SimpleLineIcons
                                    name={this.props.name}
                                    size={30}
                                    color='white'
                            />
                        </View>
                    );
                    break;
                case 'Ionicons':
                        return (
                           <View style={{marginRight: 8}}>
                                <Ionicons
                                        name={this.props.name}
                                        size={30}
                                        color='white'
                                />
                            </View>
                        );
                        break;
                case 'Feather':
                        return (
                            <View style={{marginRight: 8}}>
                                <Feather
                                        name={this.props.name}
                                        size={30}
                                        color='white'
                                />
                            </View>
                        );
                        break;
                case 'MaterialIcons':
                        return (
                           <View style={{marginRight: 8}}>
                                <MaterialIcons
                                        name={this.props.name}
                                        size={30}
                                        color='white'
                                />
                            </View>
                        );
                        break;
                case 'AntDesign':
                        return (
                           <View style={{marginRight: 8}}>
                                <AntDesign
                                        name={this.props.name}
                                        size={30}
                                        color='white'
                                />
                            </View>
                        );
                        break;
                case 'FontAwesome':
                    return (
                        <View style={{marginRight: 8}}>
                             <FontAwesome
                                     name={this.props.name}
                                     size={30}
                                     color='white'
                             />
                         </View>
                     );
                    break;
                case 'Image':
                    return (
                        <View style={{marginRight: 8}}>
                             <Image
                                     source={Images.list_icon}
                                     style={{width: 19, height: 19}}
                             />
                         </View>
                     );
                    break;
                default:
                    return (
                        <View style={{background: 'red'}}></View>
                    );
                    break;
            }
        }
        else
            return (
                <View></View>
            )
    }
    render() {
        return (
            <View>
                <TouchableOpacity style={[{borderRadius: 0, width: '100%', display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}, 
                                this.buttonBorderRadius(),
                                this.buttonBKColor()]}
                                onPress={ () => this.actionMethod() }>
                            { this.renderIcon() }
                          <Text style={[styles.btnText , fonts.size15]}>{this.props.title}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    btnText: {
      paddingVertical: 18,
      color: '#fff',
      textAlign: 'center',
    }
});