import { Dimensions } from 'react-native';

const width = Dimensions.get('window').width;
const height = Dimensions.get('window').height;
const method = [
  {value: 1, label: '訪問'},
  {value: 2, label: '電話'},
  {value: 3, label: 'メール'},
  {value: 4, label: 'その 他'}
]
const statusDescription = [
  '',
  'オーダー中',
  '確認済み',
  '配送中',
  '配送中',
  '配信済み',
  '完了',
  'キャンセル要求',
  '管理者によるキャンセル確認',
  'キャンセル拒否',
  '連絡なし',
  '会社からの報告',
  '管理者によりキャンセル'
]
export default {
  window: {
    width,
    height,
  },
  isSmallDevice: width < 375,
  serverLink: 'http://104.156.230.143:2083/',
  method: method,
  statusDescription: statusDescription
};
