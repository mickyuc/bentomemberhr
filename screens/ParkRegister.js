import React from 'react';
import { StyleSheet, Text, View,  Image, Dimensions, TouchableOpacity} from 'react-native';
import MainHeader from '../components/MainHeader';
import { shared, normalize, fonts } from '../assets/styles';
import { Container, Content, ActionSheet } from 'native-base';
import  MapView, { Polyline, Marker } from 'react-native-maps';
import { Actions } from 'react-native-router-flux';
import { changeCompanyPark, changeCompanyMain } from '../api';
import store from '../store/configuteStore';
let pageTitle = '駐車場登録'

export default class ParkRegister extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            region: {
                latitude: this.props.lat == null ? 35.652832 : this.props.lat, 
                longitude: this.props.lon == null  ? 139.839478 : this.props.lon,
                latitudeDelta: 0.05864195044303443,
                longitudeDelta: 0.050142817690068,
            },
            marker: {
                latitude: this.props.lat, 
                longitude: this.props.lon,
            },
            
        };
        pageTitle = this.props.title;
    }

    setMarkers(e){
        this.setState({marker: { latitude : parseFloat(e.nativeEvent.coordinate.latitude), longitude: parseFloat(e.nativeEvent.coordinate.longitude)} })
    }

    saveParkLatLng(){
        let token = store.getState().user.token;
        if(this.props.type == 2){
            changeCompanyPark(this.props.companyId, this.state.marker.latitude, this.state.marker.longitude, token)
            .then((response) => {
                this.setState({loaded: true});
                if(response.status) {
                  Actions.popTo("businesseditscreen", {refresh: {}})
                  Actions.refresh();
                }
                else {
                    showToast(response.msg);
                }
            })
            .catch((error) => {
                this.setState({loaded: true});
                showToast();
            })
        }
        else if(this.props.type == 1){
            changeCompanyMain(this.props.companyId, this.state.marker.latitude, this.state.marker.longitude, token)
            .then((response) => {
                this.setState({loaded: true});
                if(response.status) {
                  Actions.popTo("businesseditscreen", {refresh: {}})
                  Actions.refresh();
                }
                else {
                    showToast(response.msg);
                }
            })
            .catch((error) => {
                this.setState({loaded: true});
                showToast();
            })
        }
    }

    render() {
        return (
        <Container style={[shared.container , {backgroundColor: '#f2f2f2'}]}>
            <MainHeader title={pageTitle}></MainHeader>
            <MapView style={{width: '100%', height: '100%'}} 
                initialRegion={this.state.region}
                onPress={(e) => this.setMarkers(e)}
            >
                {
                    this.state.marker.latitude != null && this.state.marker.longitude != null ?
                    <Marker coordinate={this.state.marker} >
                        <Image source={require('../assets/images/park.png')} style={{height: 40, width: 40}} resizeMethod="resize" resizeMode="contain" />
                    </Marker>
                    :
                    null
                }
                
            </MapView>
            <View style={{paddingHorizontal: normalize(24), alignItems: 'center', justifyContent: 'center'}}>
                <TouchableOpacity onPress={() => this.saveParkLatLng()} style={styles.save}>
                    <Text style={[{color: 'white' }, fonts.size20]}>確定</Text>
                </TouchableOpacity>
            </View>
        </Container>
        );
    }
}

const styles = StyleSheet.create({
    marker: {
        width: 26,
        height: 26,
        borderRadius: 13,
        backgroundColor: "#5eba56",
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#707070'
    },
    save: {
        backgroundColor: '#41cce1',
        borderRadius: normalize(12),
        position: 'absolute',
        zIndex: 99999,
        bottom: normalize(90),
        width: '100%',
        paddingVertical: normalize(14),
        alignItems: 'center'
    }
});
