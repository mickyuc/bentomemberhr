import React from 'react';
import { StyleSheet, Text, View,  Image, Dimensions,} from 'react-native';
import MainHeader from '../components/MainHeader';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { shared } from '../assets/styles';
import { Container } from 'native-base';
import  MapView, { Polyline, Marker } from 'react-native-maps';
import { GetRoutes } from './../api';
import { showToast } from './../shared/global';
import store from './../store/configuteStore';
import { _e } from './../lang';
let pageTitle = '上野松が谷ルート'

export default class MapShow extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loaded: true,
            region: {
                latitude: 0, 
                longitude: 0,
                latitudeDelta: 0,
                longitudeDelta: 0,
            },
            markers: []
        };
    }
    componentDidMount(){
        this.setState({loaded: false});
        const userToken = store.getState().user.token;

        const mapIds = this.props.ids;
        let postBody = [];
        for(let i = 0; i < mapIds.length; i++) {
            var encodedKey = encodeURIComponent('routeIds[]');
            var encodedValue = encodeURIComponent(mapIds[i]);
            postBody.push(encodedKey + "=" + encodedValue);
        }
        postBody = postBody.join("&");

        GetRoutes(postBody, userToken)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                let markersList = [];
                let indArr = [];
                let maxLatitude = 0, maxLongitude = 0, minLatitude = 10000, minLongitude = 10000;
                const dataList = response.data;
                let latlngList = [];
                for(let i = 0; i < dataList.length; i++) {
                    if(indArr.indexOf(dataList[i].routeId) < 0) {
                        indArr.push(dataList[i].routeId);
                        let newItem = {};
                        newItem[dataList[i].routeId] = (dataList[i].companyMainLat && dataList[i].companyMainLon) ? [
                            {
                                latitude: parseFloat(dataList[i].companyMainLat),
                                longitude: parseFloat(dataList[i].companyMainLon)
                            }
                        ] : [];
                        
                        latlngList = [];
                        if(dataList[i].companyMainLat && dataList[i].companyMainLon) {
                            latlngList.push({
                                lat: dataList[i].companyMainLat,
                                lng: dataList[i].companyMainLon
                            });
                        }
                        
                        markersList.push(newItem);
                        maxLatitude = dataList[i].companyMainLat && parseFloat(dataList[i].companyMainLat) > maxLatitude ? parseFloat(dataList[i].companyMainLat) : maxLatitude
                        minLatitude = dataList[i].companyMainLat && parseFloat(dataList[i].companyMainLat) < minLatitude ? parseFloat(dataList[i].companyMainLat) : minLatitude
                        maxLongitude = dataList[i].companyMainLon && parseFloat(dataList[i].companyMainLon) > maxLongitude ? parseFloat(dataList[i].companyMainLon) : maxLongitude
                        minLongitude = dataList[i].companyMainLon && parseFloat(dataList[i].companyMainLon) < minLongitude ? parseFloat(dataList[i].companyMainLon) : minLongitude
                    } else {
                        for(let j = 0; j < markersList.length; j++) {
                            let keyVal = Object.keys(markersList[j])[0];
                            if(keyVal == dataList[i].routeId && dataList[i].companyMainLat && dataList[i].companyMainLon) {
                                let haviingThis = false;
                                for(let ii = 0; ii < latlngList.length; ii++) {
                                    if(latlngList[ii].lat == dataList[i].companyMainLat &&
                                        latlngList[ii].lng == dataList[i].companyMainLon) {
                                        haviingThis = true;
                                        break;
                                    }
                                }

                                if(!haviingThis) {
                                    markersList[j][keyVal].push({ 
                                        latitude: parseFloat(dataList[i].companyMainLat), 
                                        longitude: parseFloat(dataList[i].companyMainLon)
                                    });

                                    latlngList.push({
                                        lat: dataList[i].companyMainLat,
                                        lng: dataList[i].companyMainLon
                                    });

                                    maxLatitude = dataList[i].companyMainLat && parseFloat(dataList[i].companyMainLat) > maxLatitude ? parseFloat(dataList[i].companyMainLat) : maxLatitude
                                    minLatitude = dataList[i].companyMainLat && parseFloat(dataList[i].companyMainLat) < minLatitude ? parseFloat(dataList[i].companyMainLat) : minLatitude
                                    maxLongitude = dataList[i].companyMainLon && parseFloat(dataList[i].companyMainLon) > maxLongitude ? parseFloat(dataList[i].companyMainLon) : maxLongitude
                                    minLongitude = dataList[i].companyMainLon && parseFloat(dataList[i].companyMainLon) < minLongitude ? parseFloat(dataList[i].companyMainLon) : minLongitude
                                }
                            }
                        }
                    }
                }

                let latitude = (maxLatitude + minLatitude) / 2;
                let longitude = (maxLongitude + minLongitude) / 2;
                let latitudeDelta = maxLatitude - minLatitude + 0.01;
                let longitudeDelta = maxLongitude - minLongitude + 0.01;

                this.setState({
                    markers: markersList,
                    region: {
                        ...this.state.region,
                        latitude,
                        longitude,
                        latitudeDelta,
                        longitudeDelta
                    }
                });
            }
            else {
                showToast(_e.InvalidToken);
            }
        });
    }
    render() {
        const { markers, region } = this.state;
        let showMarkList = [];
        if(markers.length > 0) {
            for(let i = 0; i < markers.length; i++) {
                showMarkList.push(Object.values(markers[i])[0]);
            }
        }

        return (
            <Container style={[shared.container , {backgroundColor: '#f2f2f2'}]}>
                <MainHeader title={this.props.routes_names}></MainHeader>
                <MapView style={{width: '100%', height: '100%'}} 
                    initialRegion={region.latitude > 0 && region.longitude > 0 ? region : null}
                >
                    {
                        showMarkList.length > 0 ?
                        showMarkList.map((marker, ind) => 
                            <Polyline
                                key={ind}
                                coordinates={marker}
                                strokeColor="#000"
                                strokeWidth={3}
                            />
                        ) : null
                    }
                    {
                        showMarkList.length > 0 ?
                        showMarkList.map(markers => {
                            return (
                                markers.map((marker, ind) => 
                                    <Marker key={ind} coordinate={marker} anchor={{x: 0.5, y: 0.5}}>
                                        <View style={styles.marker} >
                                            <Text style={{color: 'white'}}>{ind+1}</Text>
                                        </View>
                                    </Marker>
                                )
                            );
                        }) : null
                    }
                </MapView>
                <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    marker: {
        width: 26,
        height: 26,
        borderRadius: 13,
        backgroundColor: "#5eba56",
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: 1,
        borderColor: '#707070'
    },
});
