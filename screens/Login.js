import React from 'react';
import { StyleSheet, TouchableWithoutFeedback, Keyboard, Image, View, TouchableOpacity } from 'react-native';
import { Container, Content, Item, Input, Text } from 'native-base';
import Images from "./../assets/Images";
import { normalize, fonts, margin, form} from './../assets/styles';
import { user_signin } from './../api';
import { connect } from "react-redux";
import { setUser } from './../actions';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import {Actions} from 'react-native-router-flux';
import { showToast } from './../shared/global';
import store from './../store/configuteStore';
import { _e } from './../lang';
import LoginIcon from "./../assets/images/login.svg";
import LogoIcon from "./../assets/images/logo_img.svg";
TouchableOpacity.defaultProps = { activeOpacity: 0.8 };

class Login extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            username: '',
            password: '',
            usernameError: false,
            pwdError: false,
            loaded: true
        };
    }
    componentDidMount(){
        let token = store.getState().user.token;
        if(token != undefined && token != null && token != '')
            Actions.reset("root")
    }
    loginUser(){
        Keyboard.dismiss();
        if(this.state.username == '') {
            this.setState({usernameError: true});
            return;
        }
        else
            this.setState({usernameError: false});
        if(this.state.password == '') {
            this.setState({pwdError: true});
            return;
        }
        else
            this.setState({pwdError: false});

        this.setState({loaded: false});
        user_signin(this.state.username, this.state.password)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                this.props.setUser({
                    username: response.data.username,
                    token: response.data.token
                });
                // const { navigation } = this.props;
                // navigation.replace('Root');
                Actions.reset('root');
            }
            else {
                showToast(_e.signinFailed);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        })
    }
    onChangeText(text , type) {
        if(type == 'username') {
            this.setState({username: text.replace(' ' , '')});
            if(this.state.username != '')
                this.setState({usernameError: false});
        }
        else {
            this.setState({password: text});
            if(this.state.password != '')
                this.setState({pwdError: false});
        }
    }
    render(){
        return (
            <Container style={styles.contentBg}>
                <Content contentContainerStyle={[styles.contentPD, {
                    flexGrow: 1,
                    flexDirection: 'column',
                    justifyContent: 'space-between'
                }]}>
                    <View style={{paddingBottom: 20}}>
                        <View style={{ flex:1 , flexDirection:'row' , justifyContent:'center' , alignItems: 'flex-end', marginTop: 78}}>
                            <LogoIcon width={normalize(76)} height={normalize(76)} />
                            <View style={[margin.ml3]}>
                                <Text style={[styles.logoText1, fonts.size12]}>
                                    営業・配達マッピングアプリ
                                </Text>
                                <Text style={[styles.logoText1, fonts.size43]}>
                                    salse map
                                </Text>
                            </View>
                        </View>
                        <View style={{ flex:1 , flexDirection:'row' , justifyContent:'center', marginTop: 50, marginBottom: 50}}>
                            <LoginIcon width={normalize(282.8)} height={normalize(190.2)}  />
                        </View>
                        <Item rounded style={ [form.item , {position: 'relative'}] }>
                            <Input
                                placeholder = "User name"
                                value = { this.state.username }
                                style = { [form.input] }
                                onChangeText = {(text) => this.onChangeText(text , 'username')}
                                placeholderTextColor = '#9da8bf'
                            />
                        </Item>
                        <Item rounded style={ [form.item , {position: 'relative'}] }>
                            <Input
                                secureTextEntry={true}
                                placeholder = "Password"
                                value = { this.state.password }
                                style = { [form.input] }
                                onChangeText = {(text) => this.onChangeText(text , 'password')}
                                placeholderTextColor = '#9da8bf'
                            />
                        </Item>
                        <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 8}}>
                            <TouchableOpacity onPress={() => this.loginUser()} style={{borderRadius: 12, width: '100%',backgroundColor:'#353a50' }}>
                                <Text style={[styles.btnText , fonts.size15]}>ログイン </Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss(); }}>
                            <View style={[margin.mt6 , { flexDirection:'row' , justifyContent:'space-between' }]}>
                                <Text onPress={() => { Actions.push("phonelogin") }} style={[fonts.size14, {color: 'white'}]}>
                                    電話番号からログイン
                                </Text>
                                <Text onPress={() => { Actions.push("forgotpwd") }} style={[fonts.size14, {color: 'white'}]}>
                                    パスワードを忘れた方
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </Content>
                <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
            </Container>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => { dispatch(setUser(user)) }
    }
}

export default connect(null,mapDispatchToProps)(Login)

const styles = StyleSheet.create({
    contentBg: {
        backgroundColor: '#3acce1'
    },
    logoImage: {
        width: normalize(76),
        height: normalize(76)
    },
    logoText1: {
        color: 'white'
    },
    logoBgImage: {
        width: normalize(282.8),
        height: normalize(190.2)
    },
    contentPD: {
        paddingLeft: normalize(26),
        paddingRight: normalize(22)
    },
    btnText: {
        padding: 18,
        width: "100%",
        color: '#fff',
        textAlign: 'center',
    }
});
