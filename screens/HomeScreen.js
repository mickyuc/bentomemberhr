import React from 'react';
import { StyleSheet, View, Platform, Text, Image, TouchableOpacity, StatusBar} from 'react-native';
import MainHeader from './../components/MainHeader';
import { shared, fonts, margin } from './../assets/styles';
import Images from "./../assets/Images";
import { ScrollView } from 'react-native-gesture-handler';
import { SimpleLineIcons,  Feather, MaterialIcons, AntDesign} from '@expo/vector-icons';
import store from './../store/configuteStore';
import { Container, Content } from 'native-base';
import { Actions } from 'react-native-router-flux';
import AvatarIcon from "./../assets/images/eigyotaro.svg";
import SettingIcon from './../assets/images/path.svg';
import Constants from "expo-constants";
let pageTitle = ''
export default class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
      console.log(Constants.statusBarHeight)
    }
    onMainMove(type) {
      switch(type) {
        case 1:
          Actions.reset('route_select_screen');
          break;
        case 2:
          Actions.reset('business_cate_screen');
          break;
        case 3:
          Actions.push('aftercare');
          break;
        case 4:
          Actions.reset('clip_screen');
          break;
      }
    }
    render(){
        return (
          <Container style={Platform.OS === 'ios' && Constants.statusBarHeight > 40 ? [shared.mainContainer, {marginTop: -Constants.statusBarHeight}] : [shared.mainContainer]}>
              {Platform.OS === 'ios' && <StatusBar barStyle="dark-content" backgroundColor="white" />}
              <Content contentContainerStyle={{ flex:1 , display: 'flex', zIndex: 1000}}>
                <ScrollView style={{padding: 0 , margin: 0}}>
                  <View style={styles.bannerContainer}>
                    <Image source={Images.company_img} style={styles.companyImage} />
                  </View>
                  <View style={[{flex: 1, position: 'relative', borderTopLeftRadius: 10,borderTopRightRadius: 10, backgroundColor: 'white'}, margin.pt10, margin.pb10, styles.mainPanelPosition]}>
                    <View style={{position: 'absolute', top: -73, left: 50}}>
                        <AvatarIcon width={99} height={99} />
                    </View>
                    <View style={[{display: 'flex',  flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}, margin.px8]}>
                      <View>
                        <Text style={[fonts.size32, {color: '#454f63'}]}>
                          営業 配達太郎
                        </Text>
                        <Text style={[fonts.size16, margin.mt2, {color: '#78849e'}]}>
                          株式会社EIGYOU
                        </Text>
                      </View>
                      <View style={{display: 'flex', alignItems: 'center'}}>
                        <TouchableOpacity onPress={() => Actions.push("changepassword")}>
                          <SettingIcon width={25} height={25} />
                        </TouchableOpacity>
                        <Text style={[fonts.size13, margin.mt3, {color: '#78849e'}]}>
                          設定
                        </Text>
                      </View>
                    </View>
                    <View style={[{display: 'flex', flexDirection: 'row', flexWrap: 'wrap', backgroundColor: 'white', borderRadius: 12}, margin.mt6, margin.mx4, margin.px4, margin.py4, styles.panelShadow]}>
                      <TouchableOpacity style={[{borderBottomWidth: 0.5, borderRightWidth: 0.5, borderColor: '#cfd6e5'}, margin.pt5, margin.pb6, styles.mainPanelSection]} activeOpacity={0.6}
                        onPress={() => this.onMainMove(1)}
                      >
                            <SimpleLineIcons
                              name='location-pin'
                              size={36}
                              color='#2a2e43'
                            />
                            <Text style={margin.mt3}>
                              ルート検索
                            </Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={[{borderBottomWidth: 0.5, borderLeftWidth: 0.5, borderColor: '#cfd6e5'}, margin.pt5, margin.pb6, styles.mainPanelSection]} activeOpacity={0.6}
                        onPress={() => this.onMainMove(2)}>
                            <Feather
                              name='search'
                              size={36}
                              color='#2a2e43'
                            />
                            <Text style={margin.mt3}>
                              企業検索
                            </Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={[{borderTopWidth: 0.5, borderRightWidth: 0.5, borderColor: '#cfd6e5'}, margin.pt6, margin.pb5, styles.mainPanelSection]} activeOpacity={0.6}
                        onPress={() => this.onMainMove(3)}>
                            <AntDesign
                              name='calendar'
                              size={36}
                              color='#2a2e43'
                            />
                            <Text style={margin.mt3}>
                              アフター
                            </Text>
                      </TouchableOpacity>
                      <TouchableOpacity style={[{borderTopWidth: 0.5, borderLeftWidth: 0.5, borderColor: '#cfd6e5'}, margin.pt6, margin.pb5, styles.mainPanelSection]} activeOpacity={0.6}
                        onPress={() => this.onMainMove(4)}>
                            <MaterialIcons
                              name='favorite-border'
                              size={36}
                              color='#2a2e43'
                            />
                            <Text style={margin.mt3}>
                              お気に入り
                            </Text>
                      </TouchableOpacity>
                    </View>
                  </View>
                </ScrollView>
              </Content>
          </Container>
        );
    }
}
HomeScreen.navigationOptions = {
  header: null
}
const styles = StyleSheet.create({
    companyImage: {
      width: '100%',
      resizeMode: 'stretch'
    },
    settingImage: {
      width: 20,
      height: 20
    },
    avatarImage: {
      width: 99,
      height: 99,
      borderRadius: 30
    },
    bannerContainer: {
      marginTop: Platform.OS === 'ios' ? 0 : 0
    },
    panelShadow: Platform.OS === 'ios' ? {
      shadowColor: "rgba(69, 91, 99, 0.3)",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.82,
      shadowRadius: 1.41,
      elevation: 4,
    } : {
      shadowColor: "rgba(69, 91, 99, 0.3)",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.20,
      shadowRadius: 0.41,
      elevation: 2,
    },
    mainPanelPosition: {
      marginTop: Platform.OS === 'ios' ? -35 : -40
    },
    mainPanelSection: {
      width: '50%',
      display: 'flex',
      alignItems: 'center'
    }
});