import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import MainHeader from '../components/MainHeader';
import { shared, fonts, margin, normalize } from '../assets/styles';
import { Container, Content } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import ClipComponent from '../components/ClipComponent';
import store from './../store/configuteStore';
let pageTitle = 'クリップボード'

export default class ClipScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      list: [],
      route_list: [],
      tab: 1
    }
  }
  componentDidMount(){
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        let user = store.getState().user;
        this.setState({
          list: user.business_list == null ? [] : user.business_list,
          route_list: user.route_list == null ? [] : user.route_list
        })
      }
    );
  }
  render() {
    return (
      <Container style={[shared.container , {backgroundColor: '#f2f2f2'}]}>
        <MainHeader title={pageTitle}></MainHeader>
        <View style={[{display: 'flex', flexDirection: 'row', backgroundColor: 'white'}, margin.mt3]}>
          <TouchableOpacity style={{width: '50%' , display: 'flex' , alignItems: 'center' , paddingVertical: 16 , borderRightWidth: 1, borderColor: '#F4F4F6'}}
                            onPress={() => this.setState({tab: 1})}>
              <Text style={[fonts.size13, this.state.tab == 1 ? styles.activeColor : styles.defaultColor]}>企業検索</Text>
          </TouchableOpacity>
          <TouchableOpacity style={{width: '50%' , display: 'flex' , alignItems: 'center' , paddingVertical: 16}}
                            onPress={() => this.setState({tab: 2})}>
              <Text style={[fonts.size13, this.state.tab == 2 ? styles.activeColor : styles.defaultColor]}>ルート検索</Text>
          </TouchableOpacity>
        </View>
        <Content contentContainerStyle={{ flex:1 , display: 'flex'}}>
          <ScrollView style={{paddingHorizontal: 23 , margin: 0}}>
            {
              this.state.tab == 1 ?
              <View style={[margin.mt4, margin.pb5]}>
                {
                  Object.values(this.state.list).map((item, key) => {
                    return (
                      <ClipComponent key={key}
                      businesstypenames={item.businesstypenames}
                      category_name={item.category_name}
                      citynames={item.citynames}
                      predictnames={item.predictnames}
                      attr={item.main_value}
                      type={1} />
                    );
                  })
                }
              </View> :
              <View style={[margin.mt4, margin.pb5]}>
                {
                  Object.values(this.state.route_list).map((item, key) => {
                    return (
                      <ClipComponent key={key}
                      bname={item.bname}
                      routes={item.routes}
                      attr={item.main_value}
                      type={2} />
                    );
                  })
                }
              </View>
            }
          </ScrollView>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  defaultColor: {
    color: '#959dad'
  },
  activeColor: {
    color: 'black'
  },
  closeIcon: {
    right: normalize(20),
    top: normalize(20),
    position: 'absolute'
  },
  collapseIcon: {
    right: normalize(7),
    bottom: normalize(7),
    position: 'absolute'
  }
});
