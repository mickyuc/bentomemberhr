import React from 'react';
import { View, Text, StyleSheet} from 'react-native';
import MainHeader from '../components/MainHeader';
import OrderItem from '../components/OrderItem';
import { Container, Content } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import { shared, fonts, margin, normalize } from '../assets/styles';
import { GetRoutes, changeOrderStatus, getRouteDetail } from './../api';
import { showToast } from './../shared/global';
import store from './../store/configuteStore';
import { _e } from './../lang';
import moment from 'moment';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import Layout from '../constants/Layout';
let pageTitle = ''

export default class OrderStatusScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loaded: true,
            coordinate: [],
            orders: []
        }
    }
    componentDidMount(){
        this.setState({loaded: false});
        const userToken = store.getState().user.token;

        const mapId = this.props.id;
        let postBody = [];
        var encodedKey = encodeURIComponent('routeIds[]');
        var encodedValue = encodeURIComponent(mapId);
        postBody.push(encodedKey + "=" + encodedValue);
        postBody = postBody.join("&");
        getRouteDetail(this.props.id[0], userToken)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                this.setState({orders: response.data})
            }
            else {
                showToast(_e.InvalidToken);
            }
        });
    }
    renderOrder(){
        return this.state.orders.map((order) => {
            return (
                <OrderItem
                    status={order.orderStatus}
                    time="2020/06/20 11:31"
                    statusText={Layout.statusDescription[order.orderStatus]}
                    business={order.companyTitle}
                    phone={order.phone}
                />
            )
        })
    }
    render() {
        return (
            <Container  style={shared.container}>
                <MainHeader title={pageTitle}></MainHeader>
                <Content contentContainerStyle={{ flex:1 , display: 'flex'}}>
                    <ScrollView style={{margin: 0}}>
                        <View style={[margin.pl7, margin.pr5, margin.pb5, margin.mt4]}>
                            {
                                this.state.orders.length > 0 ?
                                this.renderOrder()
                                :
                                null
                            }
                        </View>
                    </ScrollView>
                </Content>
                <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
            </Container>
        );
    }
}
