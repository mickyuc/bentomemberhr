import React from 'react';
import { View, Text } from 'react-native';
import MainHeader from '../components/MainHeader';
import ActionButton from '../components/ActionButton';
import CheckBoxLine from '../components/CheckBoxLine';
import { shared,  margin } from '../assets/styles';
import { Container, Content } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import { showToast } from '../shared/global';
import { _e } from './../lang';
import { Actions } from 'react-native-router-flux';
import store from './../store/configuteStore';
import { connect } from "react-redux";
import { setUser } from './../actions';

let pageTitle = '企業検索｜見込レベル'
var _self = null;
class BusinessLevelScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      data: [
        {
          id: 1,
          value: '見込みレベル・高'
        },
        {
          id: 2,
          value: '見込みレベル・中'
        },
        {
          id: 3,
          value: '見込みレベル・低'
        }
      ],
      choose: false
    }
  }
  componentDidMount(){
    _self = this;
  }
  bindCheckChangeTrigger(id, value) {
    let levels = _self.state.data, isChecked = false
    for (let i = 0; i < levels.length; i ++)
      if(levels[i]['id'] == id) {
        levels[i]['checked'] = value
        break;
      }
    _self.setState({
      data: levels
    })

    for(let i = 0; i < levels.length; i ++)
      if(levels[i]['checked']) {
        _self.setState({choose: true})
        isChecked  = true
        break;
      }
    if(!isChecked)
      _self.setState({choose: false})
  }
  getSelectedLevels() {
    let levels = [], levelsnames = []
    let _levels = _self.state.data
    for (let i = 0; i < _levels.length; i ++)
      if(_levels[i].hasOwnProperty('checked') && _levels[i]['checked']) {
        levels.push(_levels[i]['id'])
        levelsnames.push(_levels[i]['value'])
      }
    if(levels.length < 1) {
      showToast(_e.blevelNoSelect)
      return false
    }
    return {levels: levels, levelsnames: levelsnames}
  }
  showList() {
    let ret = this.getSelectedLevels()
    if(!ret)
      return


    let user = store.getState().user;
    let business_list = user.business_list;
    if(business_list == null)
      business_list = []

    business_list.push({
      'category_name': this.props.category_name.join(),
      'citynames': this.props.citynames.join(),
      'businesstypenames': this.props.typenames.join(),
      'predictnames': ret.levelsnames.join(),
      'main_value': {
        'category_id': this.props.category_id,
        'state_id': this.props.state_id,
        'cityIds': this.props.cityIds,
        'businessTypeIds': this.props.typeids,
        'predictTypeId': ret.levels,
        'state_name': this.props.state_name,
        'type': 'company_list_screen'
      }
    })

    if(business_list.length > 10)
      business_list.splice(0 , 1)

    this.props.setUser({
      username: user.username,
      token: user.token,
      route_list: user.route_list,
      business_list: business_list
    });


    // console.log(this.props.cityids)

    Actions.push('company_list_screen', {
      'category_id': this.props.category_id,
      'state_id': this.props.state_id,
      'cityIds': this.props.cityIds,
      'businessTypeIds': this.props.typeids,
      'predictTypeId': ret.levels
    })
  }
  showOnMap() {
    let ret = this.getSelectedLevels()
    if(!ret)
      return

    let user = store.getState().user;
    let business_list = user.business_list;
    if(business_list == null)
      business_list = []

    business_list.push({
      'category_name': this.props.category_name.join(),
      'citynames': this.props.citynames.join(),
      'businesstypenames': this.props.typenames.join(),
      'predictnames': ret.levelsnames.join(),
      'main_value': {
        'category_id': this.props.category_id,
        'state_id': this.props.state_id,
        'cityIds': this.props.cityIds,
        'businessTypeIds': this.props.typeids,
        'predictTypeId': ret.levels,
        'state_name': this.props.state_name,
        'type': 'company_map_screen'
      }
    })

    if(business_list.length > 10)
      business_list.splice(0 , 1)

    this.props.setUser({
      username: user.username,
      token: user.token,
      route_list: user.route_list,
      business_list: business_list
    });

    Actions.push('company_map_screen', {
      'category_id': this.props.category_id,
      'state_id': this.props.state_id,
      'cityIds': this.props.cityIds,
      'businessTypeIds': this.props.typeids,
      'predictTypeId': ret.levels,
      'state_name': this.props.state_name
    })
  }
  render() {
    return (
      <Container  style={shared.container}>
        <MainHeader title={pageTitle}></MainHeader>
        <Content contentContainerStyle={{ flex:1 , display: 'flex'}}>
          <ScrollView style={{paddingTop: 0 , margin: 0}}>
            <View style={[margin.pb5, margin.mt24]}>
              {
                Object.values(this.state.data).map((item, key) => {
                  return (
                        <CheckBoxLine key={key} title={ item.value } id={ item.id } 
                                      parentMethod={this.bindCheckChangeTrigger}></CheckBoxLine>
                  );
                })
              }
            </View>
          </ScrollView>
        </Content> 
        {
          this.state.choose ?
            <View style={[{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}, margin.p5]}>
              <View style={{width: '48%'}}>
                <ActionButton title="リストで表示" borderRadius={12} iconType="Image"
                customClickEvent={this.showList.bind(this)}></ActionButton>
              </View>
              <View style={{width: '48%'}}>
                <ActionButton color="#3ACCE1" title="地図で表示" borderRadius={12} iconType="SimpleLineIcons" name="location-pin"
                customClickEvent={this.showOnMap.bind(this)}></ActionButton>
              </View>
          </View>
          :
          <View></View>
        }
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
      setUser : user => { dispatch(setUser(user)) }
  }
}

export default connect(null,mapDispatchToProps)(BusinessLevelScreen)

BusinessLevelScreen.navigationOptions = {
  header: null
}