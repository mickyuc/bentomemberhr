import React from 'react';
import { View, TouchableOpacity, StyleSheet } from 'react-native';
import MainHeader from '../components/MainHeader';
import MessageItem from '../components/MessageItem';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Container, Content } from 'native-base';
import { ScrollView,  } from 'react-native-gesture-handler';
import { shared, margin } from '../assets/styles';
import { getNotificationList } from './../api';
import { showToast } from './../shared/global';
import store from './../store/configuteStore';
import { _e } from './../lang';
let pageTitle = 'メッセージ'
export default class MessageScreen extends React.Component {
  _isMounted = false;

  constructor(props){
    super(props);
    this.state = {
      loaded: true,
      messageList: []
    };
  }
  
  componentDidMount(){
    this._isMounted = true;
    this.getMessageList();
    this.willFocusSubscription = this.props.navigation.addListener(
      'willFocus',
      () => {
        this.getMessageList();
      }
    );
  }
  componentWillUnmount() {
    this._isMounted = false;
    this.willFocusSubscription.remove();
  }
  getMessageList = () => {
    this.setState({loaded: false});
    const userToken = store.getState().user.token;
    getNotificationList(userToken)
    .then((response) => {
      this.setState({loaded: true});
      if(response.status) {
        if(response.data && response.data.length > 0) {
          this.setState({messageList: response.data});
        } else {
          this.setState({messageList: [
            {
              id: 0,
              title: "none",
              content: "",
              status: "0"
            }
          ]});
        }
      }
      else {
        showToast(_e.InvalidToken);
      }
    })
  }

  render() {
    const { loaded, messageList } = this.state;
    return (
      <Container  style={shared.container}>
          <MainHeader title={pageTitle}></MainHeader>
          <Content contentContainerStyle={{ flex:1 , display: 'flex'}}>
            <ScrollView style={{margin: 0}}>
              <View style={[margin.px4, margin.pb5, {width: '100%'}]}>
                {
                  messageList.length > 0 ?
                  messageList.map(message => 
                    <MessageItem key={message.id} message={message} navigation={this.props.navigation} />
                  ) : null
                }
              </View>
            </ScrollView>
          </Content>
          <Spinner_bar color={'#27cccd'} visible={!loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
