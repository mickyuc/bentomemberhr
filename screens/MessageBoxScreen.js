import React from 'react';
import { View, Text, StyleSheet, KeyboardAvoidingView} from 'react-native';
import MainHeader from '../components/MainHeader';
import MessageTimeHeader from '../components/MessageTimeHeader';
import MessageBoxItem from '../components/MessageBoxItem';
import { Container, Content, Item, Input } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import { shared, fonts, margin, normalize } from '../assets/styles';
import { Feather } from '@expo/vector-icons';
let pageTitle = 'メッセージ'
export default class MessageBoxScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
          message: ''
        };

    }
    componentDidMount(){
    }
    onChangeText(text) {
            this.setState({message: text.replace(' ' , '')});
    }
    render() {
        return (
            <Container  style={shared.container}>
                <KeyboardAvoidingView  behavior={Platform.OS == "ios" ? "padding" : "height"}
                    style={{flex: 1}}>
                    <MainHeader title={pageTitle}></MainHeader>
                    <Content contentContainerStyle={{ flex:1 , display: 'flex'}}>
                        <ScrollView style={{margin: 0}}>
                            <View style={[margin.px5, margin.pb5, {width: '100%'}]}>
                                <View style={margin.mt3}>
                                    <MessageTimeHeader
                                        time="昨日, 2:30 PM"
                                    />
                                </View>
                                <View style={margin.mt3}>
                                    <MessageBoxItem
                                        send={true}
                                        message="ここにコメントここにコメントここにコメントここにコメントここにコメントここにコメント"
                                    />
                                </View>
                                <View style={margin.mt3}>
                                    <MessageBoxItem
                                        message="ここにコメントここにコメントここにコメントここにコメントここにコメントここにコメント"
                                    />
                                </View>
                                <View style={margin.mt3}>
                                    <MessageTimeHeader
                                        time="7:43 PM"
                                    />
                                </View>
                                <View style={margin.mt3}>
                                    <MessageBoxItem
                                        send={true}
                                        message="ここにコメントここにコメントここにコメントここにコメントここにコメントここにコメント"
                                    />
                                </View>
                                <View style={margin.mt3}>
                                    <MessageBoxItem
                                        send={true}
                                        message="ここにコメントここにコメントここにコメントここにコメントここにコメントここにコメント"
                                    />
                                </View>
                                <View style={margin.mt3}>
                                    <MessageBoxItem
                                        send={true}
                                        message="ここにコメントここにコメントここにコメントここにコメントここにコメントここにコメント"
                                    />
                                </View>
                                <View style={margin.mt3}>
                                    <MessageBoxItem
                                        send={true}
                                        message="ここにコメントここにコメントここにコメントここにコメントここにコメントここにコメント"
                                    />
                                </View>
                                <View style={margin.mt3}>
                                    <MessageBoxItem
                                        send={true}
                                        message="ここにコメントここにコメントここにコメントここにコメントここにコメントここにコメント"
                                    />
                                </View>
                            </View>
                        </ScrollView>
                    </Content>
                    <View style={[margin.px4, margin.py3]}>
                            <Item rounded style={ [styles.item_border , {position: 'relative'}] }>
                                    <Input
                                        placeholder = "コメント"
                                        value = {this.state.message}
                                        style = { [styles.input] }
                                        onChangeText = {(text) => this.onChangeText(text)}
                                        placeholderTextColor = '#78849E'
                                    />
                                    <Feather
                                        name='plus'
                                        size={30}
                                        color='#454f63'
                                        style={margin.px3}
                                    />
                            </Item>
                    </View>
                </KeyboardAvoidingView>
            </Container>
        );
    }
}
MessageBoxScreen.navigationOptions = {
    header: null
}

const styles = StyleSheet.create({
    item_border: {
        display: 'flex' ,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center',
        borderRadius : normalize(12),
        paddingRight: normalize(0),
        paddingLeft: normalize(24),
        paddingTop: normalize(12),
        paddingBottom: normalize(12),
        height: normalize(52),
        backgroundColor: 'white',
        borderBottomWidth: 1,
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderColor: '#F4F4F6'
    },
    input: {
        fontSize: normalize(14),
        lineHeight: normalize(14),
        textAlignVertical: 'center',
        color: '#78849E',
        borderRightWidth: 1,
        borderColor: '#F4F4F6',
        height: 28
    }
});
