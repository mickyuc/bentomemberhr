import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, TouchableHighlight} from 'react-native';
import MainHeader from '../components/MainHeader';
import { shared, fonts, margin } from '../assets/styles';
import { Container, Content } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import { getState } from './../api';
import { showToast } from './../shared/global';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
let pageTitle = '企業検索｜都道府県'
TouchableOpacity.defaultProps = { activeOpacity: 0.6 };
export default class BusinessLocationScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      data: [],
      loaded: false
    }
  }
  componentDidMount(){
    //comment
    this.fetchState()
  }
  fetchState() {
    let self = this
    this.setState({loaded: false});
    getState()
    .then((response) => {
      this.setState({loaded: true});
      if(response.status) {
        this.setState({
          data: response.data
        })
      }
      else
        showToast();
    })
    .catch((error) => {
      this.setState({loaded: true});
      showToast();
    })
  }
  chooseState(id, name) {
    Actions.push('business_location_d_screen', {category_id: this.props.category_id, category_name: this.props.category_name, state_id: id, state_name: name});
  }
  render() {
    return (
      <Container  style={shared.container}>
        <MainHeader title={pageTitle}></MainHeader>
        <Content contentContainerStyle={{ flex:1 , display: 'flex' , paddingTop: 10}}>
          <ScrollView style={{padding: 0 , margin: 0}}>
            <View style={margin.mt4}>
                {
                  Object.values(this.state.data).map((item, key) => {
                    if(key != this.state.data.length - 1)
                      return (
                        <TouchableHighlight key={key}
                        style={[margin.py5, margin.pl3, margin.mr12, styles.location_info, styles.location_bottom, {backgroundColor: 'white'}]}
                        onPress={() => this.chooseState(item.id, item.value)}
                        underlayColor="#DDDDDD"
                        >
                          <Text style={[fonts.size15, {color: '#2a2e43'}]}> { item.value } </Text>
                        </TouchableHighlight>
                      )
                    else
                      return (
                        <TouchableHighlight key={key}
                        style={[margin.py5, margin.pl3, margin.mr12, styles.location_info, {backgroundColor: 'white'}]}
                        onPress={() => this.chooseState(item.id)}
                        underlayColor="#DDDDDD"
                        >
                          <Text style={[fonts.size15, {color: '#2a2e43'}]}> { item.value } </Text>
                        </TouchableHighlight>
                      )
                  })
                }

            </View>
          </ScrollView>
        </Content>
        <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
      </Container>
    );
  }
}

BusinessLocationScreen.navigationOptions = {
  header: null
}

const styles = StyleSheet.create({
  location_info: {
    marginLeft: 50
  },
  location_bottom: {
    borderBottomWidth: 1,
    borderColor: '#C3C9D6'
  }
});