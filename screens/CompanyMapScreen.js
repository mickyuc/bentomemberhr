import React from 'react';
import { StyleSheet, Text, View,  Animated, Image, Dimensions, TouchableOpacity} from 'react-native';
import MainHeader from '../components/MainHeader';
import { shared, margin, normalize } from '../assets/styles';
import { Container, Content } from 'native-base';
import { ClusterMap } from 'react-native-cluster-map';
import { Marker } from "react-native-maps";

import { getCompanyListBySearch } from '../api';


import Spinner_bar from 'react-native-loading-spinner-overlay';
import { MyCluster } from "./../components/MyCluster";

import { showToast } from '../shared/global';
import {Actions} from 'react-native-router-flux';
import { ASSET_URL } from '../constants/Global';
import PlusButton from '../components/PlusButton';
import { Ionicons } from '@expo/vector-icons';
import { _e } from '../lang';
import store from '../store/configuteStore';

TouchableOpacity.defaultProps = { activeOpacity: 0.6 };
let pageTitle = '地図を表示'
let asset_url = ASSET_URL;
const Images = [
    { uri: "https://i.imgur.com/sNam9iJ.jpg" },
    { uri: "https://i.imgur.com/N7rlQYt.jpg" },
    { uri: "https://i.imgur.com/UDrH0wm.jpg" },
    { uri: "https://i.imgur.com/Ka8kNST.jpg" }
]
const { width, height } = Dimensions.get("window");
const CARD_HEIGHT = height / 5;
const CARD_WIDTH = width - 20;

var _self = null;

const INITIAL_REGION = {
    latitude: 52.5,
    longitude: 19.2,
    latitudeDelta: 8.5,
    longitudeDelta: 8.5,
};

export default class CompanyMapScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loaded: true,
            markers: [],
            region: {
                latitude: 34.6923552,
                longitude: 134.8607082,
                latitudeDelta: 0.3,
                longitudeDelta: 0.1,
            },
            company: null
        };
    }
    componentDidMount(){
        _self = this;
        _self.setState({loaded: false});
        getCompanyListBySearch(store.getState().user.token, _self.props.category_id,
        _self.props.state_id, _self.props.cityIds, _self.props.businessTypeIds, _self.props.predictTypeId)
        .then((response) => {
            _self.setState({loaded: true});
            if(response.status) {
                let watch_list = [];
                for( let i = 0; i < response.data.length; i ++ ) {
                    watch_list.push( {
                        coordinate: {
                            latitude: parseFloat(response.data[i]['mainLat']),
                            longitude: parseFloat(response.data[i]['mainLon']),
                        },
                        title: response.data[i]['companyTitle'],
                        description: response.data[i]['receiveAddress'],
                        phone: response.data[i]['phone'],
                        image: response.data[i]['images'] == null ? Images[0] : { 'uri' : asset_url + JSON.parse(response.data[i]['images'])[0] } ,
                        companyId: response.data[i]['companyId']
                    } )
                }
                if(response.data.length > 0) {
                    _self.setState({
                        markers: watch_list,
                        region: {
                            latitude: watch_list[0]['coordinate']['latitude'],
                            longitude: watch_list[0]['coordinate']['longitude'],
                            latitudeDelta: 0.3,
                            longitudeDelta: 0.1,
                        }
                    })
                }
            }
        })
    }
    renderCustomClusterMarker = (count) => <MyCluster count={count} />
    markerClick(item) {
        this.setState({
            company: item
        })
    }
    businessScreen(companyId) {
        Actions.push('businessscreen', {
            'companyId': companyId
        })
    }
    closeCard() {
        this.setState({
            company: null
        })
    }
    render() {
        return (
            <Container style={[shared.container , {backgroundColor: '#f2f2f2'}]}>
                <MainHeader title={pageTitle + ':' + this.props.state_name}></MainHeader>
                {
                    this.state.markers.length > 0 ?
                    <ClusterMap
                    renderClusterMarker={this.renderCustomClusterMarker}
                    region={this.state.region} style={{flex: 1}} clusterColor='white' clusterTextColor='black'>
                    {
                        this.state.markers.map((item, key) => (
                            <Marker key={key} coordinate={item.coordinate}
                            onPress={() => this.markerClick(item)}>
                                    <View style={styles.marker}>
                                    </View>
                            </Marker>
                        ))
                    }
                    </ClusterMap>
                    :
                    <View style={margin.mt4}>
                        <Text style={{ textAlign: 'center' }}>{ _e.noCompanyList }</Text>
                    </View>
                }
                {
                    this.state.company != null ?
                    <View style={{position: 'absolute', bottom: 30}}>
                        <View style={{position: 'relative'}}>
                                <TouchableOpacity style={{zIndex: 10001}}
                                onPress={() => this.closeCard()}>
                                    <Ionicons
                                                    name="md-close"
                                                    size={28}
                                                    color='#2A2E43'
                                                    style={{position: 'absolute' , right: 20 , top: 5}}
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity style={styles.card}
                                        onPress={() => this.businessScreen(this.state.company.companyId)}>
                                            <View style={{width: '40%', paddingHorizontal: 10, justifyContent: 'flex-start', alignItems: 'center'}}>
                                                <Image
                                                    source={this.state.company.image}
                                                    style={styles.cardImage}
                                                    resizeMode="contain"
                                                />
                                                <Text>訪問</Text>
                                            </View>
                                            <View style={styles.textContent}>
                                                <View style={{flex: 1, borderBottomColor: '#707070', borderBottomWidth: 1}}>
                                                    <Text numberOfLines={1} style={styles.cardtitle}>{ this.state.company.title }</Text>
                                                    <Text style={[styles.cardDescription, margin.mt2]}>
                                                        { this.state.company.description }
                                                    </Text>
                                                </View>
                                                <View style={{display: 'flex', alignItems: 'center', flexDirection: 'row', flex: 1}}>
                                                    <PlusButton
                                                        color="#3497FD"
                                                        width={34}
                                                        height={34}
                                                        size={20}
                                                        type="phone"
                                                        borderRadius={8}
                                                    />
                                                    <Text style={margin.ml2}>{ this.state.company.phone }</Text>
                                                </View>
                                            </View>
                                </TouchableOpacity>
                        </View>
                    </View>
                    :
                    <View></View>
                }
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    marker: {
        width: 30,
        height: 30,
        borderRadius: 15,
        backgroundColor: "#FF9057",
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: normalize(2),
        borderColor: '#707070'
    },
    card: {
        padding: 10,
        elevation: 2,
        backgroundColor: "#FFF",
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: "hidden",
        flexDirection: 'row',
        paddingRight: 20
    },
    cardImage: {
        flex: 3,
        width: "100%",
        height: "100%",
        alignSelf: "center",
        marginBottom: 10
    },
    textContent: {
        flex: 1,
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    cardtitle: {
        fontSize: 12,
        marginTop: 5,
        fontWeight: "bold",
    },
    cardDescription: {
        fontSize: 12,
        color: "#444",
    },
})