import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import MainHeader from '../components/MainHeader';
import CategoryItem from '../components/CategoryItem';
import { shared, fonts, margin, normalize } from '../assets/styles';
import { Container, Content } from 'native-base';
import { Actions } from 'react-native-router-flux';

TouchableOpacity.defaultProps = { activeOpacity: 0.6 };
let pageTitle = '企業検索｜企業区分'
let _self = null;
export default class BusinessCateScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      category: [
        { title: '企業区分A', active: false, id: 1 },
        { title: '企業区分B', active: false, id: 2 },
        { title: '企業区分C', active: false, id: 3 },
        { title: '企業区分D', active: false, id: 4 }
      ],
      activeId: 1,
      choose: false
    }
  }
  componentDidMount(){
    _self = this;
  }
  gotoLocation() {
    let category_id = [], category_name = []
    for(let i = 0; i < this.state.category.length; i ++)
      if(this.state.category[i]['active']) {
          category_id.push(this.state.category[i]['id'])
          category_name.push(this.state.category[i]['title'])
      }
    Actions.push('business_location_screen', {category_id: category_id, category_name: category_name});
  }
  changeCategoryState(status, id) {
    let categories = this.state.category , isChecked = false;
    for(let i = 0; i < categories.length; i ++)
      if(categories[i]['id'] == id)  {
        categories[i]['active'] = status
        break;
      }
    _self.setState({
      category: categories
    })
    for(let i = 0; i < categories.length; i ++)
      if(categories[i]['active']) {
        _self.setState({choose: true})
        isChecked  = true
        break;
      }
    if(!isChecked)
      _self.setState({choose: false})
  }
  render() {
    return (
      <Container  style={shared.container}>
        <MainHeader title={pageTitle}></MainHeader>
        <Content contentContainerStyle={{ flex:1 , display: 'flex', justifyContent: 'center' }}>
          <View style={[{ display: 'flex', flexDirection: 'row', flexWrap: 'wrap', alignItems: 'flex-end'}, margin.px4]}>
            {
              Object.values(this.state.category).map((item, key) => {
                return (
                  <CategoryItem key={key} title={item.title} category={item.id} active={item.active}
                  customClickEvent={this.changeCategoryState.bind(this)} />
                )
              })
            }
          </View>
        </Content>
        {
            this.state.choose ?
            <View>
            <TouchableOpacity style={{borderRadius: 0, width: '100%',backgroundColor:'#3497FD' }}
            onPress={() => this.gotoLocation()} >
                          <Text style={[styles.btnText , fonts.size15]}>都道府県を選択する </Text>
            </TouchableOpacity>
          </View>
          :
          <View style={{ height: normalize(55) }}></View>
        }
      </Container>
    );
  }
}
BusinessCateScreen.navigationOptions = {
  header: null
}
const styles = StyleSheet.create({
  mainPanelSection: {
    display: 'flex',
    alignItems: 'center',
    width: '50%',
    paddingVertical: 30,
    position: 'relative'
  },
  activeTextColor: {
    color: '#3497fd'
  },
  defautTextColor: {
    color: 'black'
  },
  btnText: {
    padding: normalize(18),
    width: "100%",
    color: '#fff',
    textAlign: 'center',
  },
  activeCheck: {
    position: 'absolute',
    left: 10,
    top: 30
  }
});