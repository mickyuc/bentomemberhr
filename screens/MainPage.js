import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { Platform, StatusBar, StyleSheet, View, Text, SafeAreaView } from 'react-native';
// import {Root} from 'native-base';
// import store from './store/configuteStore';
import HomeScreen from './HomeScreen';
//import BusinessCateScreen from './screens/BusinessCateScreen';
import MessageScreen from './MessageScreen';
import MessageBoxScreen from './MessageBoxScreen';

//import TabBarIcon from './components/TabBarIcon';

//import { Ionicons, SimpleLineIcons, Feather, MaterialIcons, AntDesign } from '@expo/vector-icons';



const Stack = createStackNavigator();


export default function MainPage(props) {
    return (
          <View style={styles.container}>
            {Platform.OS === 'ios' && <StatusBar barStyle="light-content" backgroundColor="white" />}
                <Stack.Navigator initialRouteName="home">
                  <Stack.Screen name="home" component={HomeScreen} options={{headerShown: false}} />
                  <Stack.Screen name="message" component={MessageScreen} options={{headerShown: false}} />
                  <Stack.Screen name="message_box" component={MessageBoxScreen} options={{headerShown: false}} />
                </Stack.Navigator>
          </View>
    );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
