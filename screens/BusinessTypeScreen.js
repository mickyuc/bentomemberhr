import React from 'react';
import { View} from 'react-native';
import MainHeader from '../components/MainHeader';
import CheckBoxLine from '../components/CheckBoxLine';
import ActionButton from '../components/ActionButton';
import { shared, margin } from '../assets/styles';
import { Container, Content } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import { getBusinessType, getCompanyListBySearch } from './../api';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { _e } from './../lang';
import { showToast } from '../shared/global';
import { Actions } from 'react-native-router-flux';
import store from './../store/configuteStore';
import { connect } from "react-redux";
import { setUser } from './../actions';

let pageTitle = '企業検索｜業種'
var _self = null;
class BusinessTypeScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      type: true,
      data: [],
      loaded: false,
      choose: false
    };
  }
  componentDidMount(){
    _self = this;
    this.fetchBusinessType();
  }
  fetchBusinessType() {
    this.setState({loaded: false});
    getBusinessType()
    .then((response) => {
      _self.setState({loaded: true});
      if(response.status) {
        _self.setState({
          data: response.data
        })
      }
      else
        showToast();
    })
    .catch((error) => {
      _self.setState({loaded: true});
      showToast();
    })
  }
  bindCheckChangeTrigger(id, value) {
    let types = _self.state.data, isChecked = false
    for (let i = 0; i < types.length; i ++)
      if(types[i]['id'] == id) {
        types[i]['checked'] = value
        break;
      }
    _self.setState({
      data: types
    })

    for(let i = 0; i < types.length; i ++)
      if(types[i]['checked']) {
        _self.setState({choose: true})
        isChecked  = true
        break;
      }
    if(!isChecked)
      _self.setState({choose: false})
  }
  getSelectedTypeIds() {
    let types = [] , typenames = []
    let _types = _self.state.data
    for (let i = 0; i < _types.length; i ++)
      if(_types[i].hasOwnProperty('checked') && _types[i]['checked']) {
        types.push(_types[i]['id'])
        typenames.push(_types[i]['value'])
      }
    if(types.length < 1) {
      showToast(_e.btypeNoSelect)
      return false
    }
    return { types: types , typenames: typenames }
  }
  showList() {
    let ret = this.getSelectedTypeIds()
    if(!ret)
      return

    let user = store.getState().user;
    let business_list = user.business_list;
    if(business_list == null)
      business_list = []
    business_list.push({
      'category_name': this.props.category_name.join(),
      'citynames': this.props.citynames.join(),
      'businesstypenames': ret.typenames.join(),
      'predictnames': '',
      'main_value': {
        'category_id': this.props.category_id,
        'state_id': this.props.state_id,
        'state_name': this.props.state_name,
        'cityIds': this.props.cityids,
        'businessTypeIds': ret.types,
        'predictTypeId': [],
        'type': 'company_list_screen'
      }
    })

    if(business_list.length > 10)
      business_list.splice(0 , 1)

    this.props.setUser({
      username: user.username,
      token: user.token,
      route_list: user.route_list,
      business_list: business_list
    });

    Actions.push('company_list_screen', {
      'category_id': this.props.category_id,
      'category_name': this.props.category_name,
      'state_id': this.props.state_id,
      'state_name': this.props.state_name,
      'cityIds': this.props.cityids,
      'citynames': this.props.citynames,
      'businessTypeIds': ret.types,
      'predictTypeId': [],
      'predictnames': []
    })
  }
  showOnMap() {
    let ret = _self.getSelectedTypeIds()
    if(!ret)
      return
    let user = store.getState().user;
    let business_list = user.business_list;
    if(business_list == null)
      business_list = []
    business_list.push({
      'category_name': this.props.category_name.join(),
      'citynames': this.props.citynames.join(),
      'businesstypenames': ret.typenames.join(),
      'predictnames': '',
      'main_value': {
        'category_id': this.props.category_id,
        'state_id': this.props.state_id,
        'cityIds': this.props.cityids,
        'businessTypeIds': ret.types,
        'predictTypeId': [],
        'state_name': this.props.state_name,
        'type': 'company_map_screen'
      }
    })

    if(business_list.length > 10)
      business_list.splice(0 , 1)

    this.props.setUser({
      username: user.username,
      token: user.token,
      route_list: user.route_list,
      business_list: business_list
    });
    Actions.push('company_map_screen', {
      'category_id': this.props.category_id,
      'state_id': this.props.state_id,
      'cityIds': this.props.cityids,
      'businessTypeIds': ret.types,
      'predictTypeId': [],
      'state_name': this.props.state_name
    });
  }
  choosePredictLevel() {
    let ret = this.getSelectedTypeIds()
    if(!ret)
      return
    Actions.push('business_level_screen', {
      'category_id': this.props.category_id,
      'category_name': this.props.category_name,
      'state_id': this.props.state_id,
      'state_name': this.props.state_name,
      'cityIds': this.props.cityids,
      'citynames': this.props.citynames,
      'typeids': ret.types,
      'typenames': ret.typenames
    })
  }
  render() {
    return (
      <Container  style={shared.container}>
        <MainHeader title={pageTitle}></MainHeader>
        <Content contentContainerStyle={{ flex:1 , display: 'flex'}}>
          <ScrollView style={{padding: 0 , margin: 0}}>
            <View style={margin.mt4, margin.pb5}>
              {
                Object.values(this.state.data).map((item, key) => {
                  return (
                        <CheckBoxLine key={key} title={ item.value } id={ item.id } 
                                      parentMethod={this.bindCheckChangeTrigger}></CheckBoxLine>
                  );
                })
              }
            </View>
          </ScrollView>
        </Content>
        {
            this.state.choose ? (
              this.props.category_id.includes(4) ?
              <View>
                <ActionButton title="見込みレベルの選択" color="#FF9057"
                customClickEvent={this.choosePredictLevel.bind(this)}></ActionButton>
              </View>
              :
              <View style={[{display: 'flex', flexDirection: 'row', justifyContent: 'space-between'}, margin.p5]}>
                <View style={{width: '48%'}}>
                  <ActionButton title="リストで表示" borderRadius={12} iconType="Image"
                  customClickEvent={this.showList.bind(this)}></ActionButton>
                </View>
                <View style={{width: '48%'}}>
                  <ActionButton color="#3ACCE1" title="地図で表示" borderRadius={12} iconType="SimpleLineIcons" name="location-pin"
                  customClickEvent={this.showOnMap.bind(this)}></ActionButton>
                </View>
              </View>
            ) :
            <View></View>
          }
          <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
      </Container>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
      setUser : user => { dispatch(setUser(user)) }
  }
}

export default connect(null,mapDispatchToProps)(BusinessTypeScreen)

BusinessTypeScreen.navigationOptions = {
  header: null
}