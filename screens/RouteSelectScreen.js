import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import MainHeader from '../components/MainHeader';
import ActionButton from '../components/ActionButton';
import PlusButton from '../components/PlusButton';
import { shared, fonts, margin, normalize } from '../assets/styles';
import { Container, Content } from 'native-base';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import RNPickerSelect, { defaultStyles } from 'react-native-picker-select';
import { getBusinessOffice, getRouteList } from './../api';
import { showToast } from './../shared/global';
import { FontAwesome } from '@expo/vector-icons';
import Spinner_bar from 'react-native-loading-spinner-overlay';

import store from './../store/configuteStore';
import { connect } from "react-redux";
import { setUser } from './../actions';

import { Actions } from 'react-native-router-flux';
let pageTitle = 'ルート検索';

class RouteSelectScreen extends React.Component {
    constructor(props){
        super(props);
        this.inputRefs = {
            firstTextInput: null,
            favSport0: null,
            favSport1: null,
            lastTextInput: null
        };
        this.state = {
            favSport0: undefined,
            favSport1: undefined,
            business: '',
            route: '',
            businessOffice: [],
            routes: [],
            loaded: true,
            count: ['0']
        };
    }
    componentDidMount(){
        let token = store.getState().user.token;
        this.setState({loaded: false});
        getBusinessOffice(token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                let temp = [];
                response.data.map((item) => {
                    temp.push({label: item.value, value: item.value})
                })
                this.setState({businessOffice: temp})
            }
            else {
                showToast(response.msg);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        })
    }

    getRoutes(value, label) {
        this.setState({ business: label });
        let token = store.getState().user.token;
        this.setState({loaded: false});
        getRouteList(value, token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                let temp = [];
                response.data.map((item) => {
                    temp.push({label: item.routeCode, value: item.routeId})
                })
                this.setState({routes: temp})
            }
            else {
                showToast(response.msg);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        });
    }

    addBox = () => {
        let { count } = this.state;
        count.push('0');
        this.setState({
            count
        });
    }

    selectRoute(label, value, ind) {
        let { count } = this.state;
        for(let i = 0; i < count.length; i++) {
            if(i == ind) {
                count[i] = label;
                break;
            }
        }
        this.setState({count});
    }

    removeRoute = (ind) => {
        let { count } = this.state;
        let newCount = [];
        for(let i = 0; i < count.length; i++) {
            if(i != ind) {
                newCount.push(count[i]);
            }
        }
        this.setState({count: newCount});
    }

    goMapPage = () => {
        const { count } = this.state;
        const { navigation } = this.props;

        let isValid = true;
        for(let i = 0; i < count.length; i++) {
            if(count[i] == '0') {
                isValid = false;
                break;
            }
        }
        let routes_names = []
        for(let i = 0; i < this.state.routes.length; i ++)
        {
            for(let j = 0; j < this.state.count.length; j ++) {
                if(this.state.count[j] == this.state.routes[i]['value']) {
                    routes_names.push(this.state.routes[i]['label'])
                    break;
                }
            }
        }

        if(count.length > 1) {
            let user = store.getState().user;
            let route_list = user.route_list;
            if(route_list == null)
                route_list = []

            route_list.push({
                'bname': this.state.business,
                'routes': routes_names.join(','),
                'main_value': {
                    'count': count
                }
            })

            if(route_list.length > 10)
                route_list.splice(0 , 1)

            this.props.setUser({
                username: user.username,
                token: user.token,
                route_list: route_list,
                business_list: user.business_list
            });
        }

        if(isValid) {
            if(count.length == 1) {
                //navigation.navigate('mapdetail', {id: count});
                Actions.push('mapdetail', {id: count, routes_names: routes_names.join(',')})
            } else if(count.length > 1) {
                //navigation.navigate('mapshow', {ids: count});
                Actions.push('mapshow', {ids: count, routes_names: routes_names.join(',')})
            }
        }
    }

    render() {
        const { count } = this.state;

        const placeholder = {
            label: '選択してください',
            value: null,
            color: '#9EA0A4',
        };

        return (
            <Container  style={shared.container}>
                <MainHeader title={pageTitle}></MainHeader>
                <Content contentContainerStyle={[{ flex:1 , display: 'flex'} , styles.contentBorder]}>
                    <ScrollView style={{margin: 0}}>
                        <View style={[margin.px6]}>
                            <View style={[margin.mt10]}>
                                <Text style={[fonts.size15, margin.mb3]}>
                                    営業所名
                                </Text>
                                <RNPickerSelect
                                    placeholder={placeholder}
                                    items={this.state.businessOffice}
                                    onValueChange={(label, value) => this.getRoutes(value, label) }
                                    style={ pickerSelectStyles }
                                    value={this.state.business}
                                    ref={el => { this.inputRefs.favSport0 = el; }}
                                    useNativeAndroidPickerStyle={false}
                                />
                            </View>
                            <View style={[margin.mt4]}>
                                <Text style={[fonts.size15, margin.mb3]}>
                                    ルート名
                                </Text>
                                {
                                    count.map((val, ind) =>
                                        <View style={margin.mt3} key={ind}>
                                            <RNPickerSelect
                                                placeholder={placeholder}
                                                items={this.state.routes}
                                                onValueChange={(label, value) => this.selectRoute(label, value, ind) }
                                                style={ pickerSelectStyles }
                                                value={val}
                                                ref={el => { this.inputRefs.favSport1 = el; }}
                                                useNativeAndroidPickerStyle={false}
                                            />
                                            {
                                                ind == 0 ? null :
                                                <TouchableOpacity onPress={() => this.removeRoute(ind)}>
                                                    <FontAwesome
                                                        name='times'
                                                        size={30}
                                                        color='black'
                                                    />
                                                </TouchableOpacity>
                                            }
                                        </View>
                                    )
                                }
                            </View>
                            <View style={[{ display: 'flex' , alignItems: 'center' }, margin.mt10]}>
                                <TouchableOpacity onPress={() => this.addBox()}>
                                    <PlusButton width={58} height={58} />
                                </TouchableOpacity>
                            </View>
                            <View style={[margin.mt16,margin.mb6]}>
                                <ActionButton customClickEvent={this.goMapPage.bind(this)} color="#41cce1" title="ルートを確認" borderRadius={12} iconType="SimpleLineIcons" name="location-pin"></ActionButton>
                            </View>
                        </View>
                    </ScrollView>
                </Content>
                <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
            </Container>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        setUser : user => { dispatch(setUser(user)) }
    }
}

export default connect(null,mapDispatchToProps)(RouteSelectScreen)

RouteSelectScreen.navigationOptions = {
    header: null
}
const styles = StyleSheet.create({
    contentBorder: {
        borderTopWidth: 1,
        borderColor: '#EAEAEA'
    }
});
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
      fontSize: normalize(15),
      paddingVertical: normalize(15),
      paddingHorizontal: normalize(24),
      borderWidth: 1,
      borderColor: '#c3c3c3',
      borderRadius: 8,
      color: '#78849E',
      paddingRight: normalize(30), // to ensure the text is never behind the icon
    },
    inputAndroid: {
      fontSize: normalize(15),
      paddingHorizontal: normalize(24),
      paddingVertical: normalize(12),
      borderWidth: 1,
      borderColor: '#c3c3c3',
      borderRadius: 8,
      color: '#78849E',
      paddingRight: 30, // to ensure the text is never behind the icon
    },
});