import React from 'react';
import { View, Text, StyleSheet, FlatList, Linking, Alert,TouchableOpacity} from 'react-native';
import MainHeader from '../components/MainHeader';
import { shared, fonts, margin, normalize, getScreenWidth } from '../assets/styles';
import { Container, Content } from 'native-base';
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import { Feather, Ionicons, EvilIcons, SimpleLineIcons } from '@expo/vector-icons';
import moment from 'moment';
import { getListByDate, getBusinessType, checkedAfter, updateAfter } from './../api';
import { showToast } from './../shared/global';
import store from './../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
import { _e } from './../lang';
import Modal from 'react-native-modal';
import RNPickerSelect from 'react-native-picker-select';
import Layout from '../constants/Layout'
let pageTitle = 'アフターケア'
const dayNamesShort =  ['日', '月', '火', '水', '木', '金', '土'];

export default class AfterCareDetail extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        loaded: true,
        orderList: [],
        bussinesType: [],
        method: '',
        modalVisible: false,
        business: '',
        detail: '',
        afterId: ''
    };
  }
  componentDidMount(){
    this.refresh();
  }

  refresh(){
    let token = store.getState().user.token;
    this.setState({loaded: false});
    
    getBusinessType()
    .then((response) => {
        if(response.status) {
            let temp = []
            for(var i = 0;i<response.data.length;i++){
                temp.push({label: response.data[i]['value'], value: response.data[i]['id']})
            }
            this.setState({bussinesType: temp})
        }
        else
            showToast(response.msg);
    })

    getListByDate(this.props.day, token)
    .then((response) => {
        this.setState({loaded: true});
        if(response.status) {
            this.setState({orderList: response.data})
        }
        else {
            showToast(response.msg);
        }
    })
    .catch((error) => {
        this.setState({loaded: true});
        showToast();
    })
  }

  phoneCall(tel,item){
      if(item.status == 0){
        let token = store.getState().user.token;
        checkedAfter(item.id, token)
        .then((response) => {
        })
      }
    Linking.openURL(`tel:${tel}`)
    this.setState({method: 2})
    this.setState({detail: item.detail})
    this.setState({afterId: item.id})
    this.setState({business: item.businessTypeId})
    this.setState({modalVisible: true})
  }


  goDetail(item){
      //Actions.push("addafterdetail", {id: id, day: this.props.day, detail: detail, method: methodId, companyId: companyId, company: companyTitle})
      if(item.status == 0){
        let token = store.getState().user.token;
        checkedAfter(item.id, token)
        .then((response) => {
        })
      }
      this.setState({method: 1})
      this.setState({business: item.businessTypeId})
      this.setState({detail: item.detail})
      this.setState({afterId: item.id})
      this.setState({modalVisible: true})
  }

  showMap(){
      if(this.state.orderList.length > 0){
          let lat = 35.652832;
          let lon = 139.839478;
          for(var i = 0;i<this.state.orderList.length;i++){
            if(this.state.orderList[i].mainLat != null && this.state.orderList[i].mainLat != "" && this.state.orderList[i].mainLon != null && this.state.orderList[i].mainLon != ""){
                lat = parseFloat(this.state.orderList[i].mainLat)
                lon = parseFloat(this.state.orderList[i].mainLon)
                break;
            }
          }
          Actions.push("showallclient", {latitude: lat, longitude: lon, day : this.props.day});
          
      }else{
        showToast(_e.noClient)
      }
  }

  addCompany() {
    Actions.push("addafterstates", {day : this.props.day});
  }

  closeModal(){
        this.setState({modalVisible: false})
  }

  save(){
    if(this.state.afterId != ''){
        let token = store.getState().user.token;
        this.setState({loaded: false})
        updateAfter(this.state.afterId, this.props.day, this.state.method, this.state.detail, this.state.business, token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                this.setState({method: ''})
                this.setState({business: ''})
                this.setState({detail: ''})
                this.setState({afterId: ''})
                this.setState({modalVisible: false})
                this.refresh();
            }
            else {
                showToast(response.msg);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        })
      }
  }

  render() {
    const placeholder = {
        label: '選択',
        value: null,
        color: '#9EA0A4',
    };
    return (
      <Container style={[shared.container , {backgroundColor: '#f2f2f2'}]}>
        <MainHeader title={pageTitle}></MainHeader>
        <Modal
            isVisible={this.state.modalVisible}
            backdropOpacity={0.5}
            avoidKeyboard={true}
            onBackButtonPress={() => this.closeModal()}
            onBackdropPress={() => this.closeModal()}
        >
            
            <View style={{backgroundColor: 'white', flex: 1, margin: 20, padding: 20, }}>
                <ScrollView>
                    <View style={{borderBottomColor: '#707070', borderBottomWidth: 1, paddingBottom: 20}}>
                        <Text style={[fonts.size20, {color: '#454f63'}]}>株式会社あいうえお</Text>
                    </View>
                    <View style={{borderBottomColor: '#707070', borderBottomWidth: 1, paddingVertical: 20, flexDirection: 'row'}}>
                        {
                            /*<View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
                                <Text style={{marginBottom: 10}}>種別</Text>
                                <RNPickerSelect
                                    placeholder={placeholder}
                                    items={this.state.bussinesType}
                                    onValueChange={value => { this.setState({business: value}) }}
                                    style={ pickerSelectStyles }
                                    value={this.state.business}
                                    useNativeAndroidPickerStyle={false}
                                />
                            </View>*/
                        }
                        
                        <View style={{ justifyContent: 'center', flex: 1}}>
                            <Text style={{marginBottom: 10}}>方法</Text>
                            <RNPickerSelect
                                placeholder={placeholder}
                                items={Layout.method}
                                onValueChange={value => { this.setState({method: value}) }}
                                style={ pickerSelectStyles }
                                value={this.state.method}
                                useNativeAndroidPickerStyle={false}
                            />
                        </View>
                        
                        
                    </View>
                    <View style={{ paddingVertical: 20, flexDirection: 'row', borderBottomColor: '#707070', borderBottomWidth: 1, marginBottom: 15}}>
                        <TextInput 
                            multiline={true}
                            numberOfLines={5}
                            defaultValue={this.state.detail}
                            onChangeText={value => this.setState({detail: value})}
                            style={{height: 100, width: '100%', justifyContent: 'flex-start', textAlignVertical: 'top'}}
                        />
                    </View>
                    <TouchableOpacity onPress={() => this.save()} style={styles.save}>
                        <Text style={[{color: 'white' }, fonts.size20]}>保存</Text>
                    </TouchableOpacity>
                </ScrollView>
            </View>
            
        </Modal>

        <ScrollView>
            <View style={styles.dateSection}>
                <View style={styles.prevNextDate}>
                    <Text style={{color: 'white', fontSize: normalize(20)}}>{moment(this.props.day).subtract(1, 'days').format("DD")}</Text>
                </View>
                <View style={{justifyContent:'center', alignItems: 'center'}}>
                    <Text style={{color: 'white', fontSize: normalize(20)}}>{moment(this.props.day).format("MM/DD")}（{dayNamesShort[moment(this.props.day).day()]}）</Text>
                </View>
                <View style={styles.prevNextDate}>
                    <Text style={{color: 'white', fontSize: normalize(20)}}>{moment(this.props.day).add(1, 'days').format("DD")}</Text>
                </View>
            </View>
            <FlatList
                data={this.state.orderList}
                renderItem={({ item }) => {
                    return (
                        <View style={styles.section}>
                            <View style={{width: getScreenWidth() - 150}}>
                                <View style={{flexDirection: 'row'}}>
                                    {
                                        item.status == 0 ?
                                        <View style={styles.noDone}></View>
                                        :
                                        <View style={{width: 8}}></View>
                                    }
                                    <Text style={item.status == 0 ? [fonts.size20, {paddingBottom: normalize(6), color: 'black'}] : [fonts.size20, {paddingBottom: normalize(6), color: '#717989'}]}>{item.companyTitle}</Text>
                                </View>
                                <View style={{paddingLeft: 8}}>
                                    {
                                        item.method != undefined && item.method != null && item.method != "" && item.detail != null && item.detail != '' ?
                                    <Text style={item.status == 0 ? [fonts.size13, {color: 'black'}] : [fonts.size13, {color: '#959dad'}]}>{item.method} / {item.detail} / {item.status == 0 ? <Text>未対応</Text> : <Text>対応済</Text> }</Text>
                                        :
                                        item.method != undefined && item.method != null && item.method != "" && (item.detail == null || item.detail == '') ?
                                        <Text style={item.status == 0 ? [fonts.size13, {color: 'black'}] : [fonts.size13, {color: '#959dad'}]}>{item.method} / {item.status == 0 ? <Text>未対応</Text> : <Text>対応済</Text> }</Text>
                                        :
                                        (item.method == undefined || item.method == null || item.method != "") && item.detail != null && item.detail != '' ?
                                        <Text style={item.status == 0 ? [fonts.size13, {color: 'black'}] : [fonts.size13, {color: '#959dad'}]}>{item.detail} / {item.status == 0 ? <Text>未対応</Text> : <Text>対応済</Text> }</Text>
                                        :
                                        null
                                    }
                                </View>
                            </View>
                            <View style={{justifyContent: 'space-between', flexDirection: 'row', width: 80}}>
                                <View>
                                    <TouchableOpacity style={styles.iconSection} onPress={() => this.phoneCall(item.phone, item)}>
                                        <Feather name={"phone"} color={'white'} size={20} />
                                    </TouchableOpacity>
                                </View>
                                <View>
                                    <TouchableOpacity style={[styles.iconSection, {marginLeft: normalize(20)}]} onPress={() => this.goDetail(item)}>
                                        <SimpleLineIcons name={"pencil"} color={'white'} size={20} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    )
                }}
                keyExtractor={item => item.id}
                style={{marginHorizontal: normalize(16), marginTop: normalize(20)}}
            />
            {
                this.state.orderList.length > 0 ?
                <TouchableOpacity style={styles.mapSection} onPress={() => this.showMap()}>
                    <Feather name={"map-pin"} color={'white'} size={23}/>
                    <Text style={{color: 'white', marginLeft: 16}}>全てのクライアントを地図で表示</Text>
                </TouchableOpacity>
                :
                null
            }
            
        </ScrollView>
        <View style={styles.plus}>
            <TouchableOpacity onPress={() => this.addCompany()}>
                <Feather name={"plus"} color={"white"} size={40} />
            </TouchableOpacity>
        </View>
        <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
      </Container>
    );
  }
}

const styles = StyleSheet.create({
    dateSection: {
        marginTop: normalize(40),
        backgroundColor: '#3acce1',
        height: normalize(50),
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    plus: {
        width: normalize(60),
        height: normalize(60),
        position: 'absolute',
        backgroundColor: '#82a3ea',
        borderRadius: normalize(30),
        bottom: 10,
        right: normalize(32),
        bottom: normalize(32),
        justifyContent: 'center',
        alignItems: 'center'
    },
    prevNextDate: {
        backgroundColor: '#5773ff',
        width: normalize(50),
        height: normalize(50),
        justifyContent: 'center',
        alignItems:'center',
    },
    section: {
        borderBottomWidth: 1,
        borderBottomColor: 'rgba(120, 132, 158, 0.08)',
        paddingHorizontal: normalize(16),
        paddingVertical: normalize(14),
        flexDirection: 'row', 
        justifyContent: 'space-between'
    },
    iconSection: {
        backgroundColor: '#78849e',
        padding: normalize(8),
        borderRadius: normalize(8),
    },
    mapSection: {
        marginHorizontal: normalize(24), 
        backgroundColor: '#5773ff', 
        paddingVertical: normalize(16),
        borderRadius: normalize(12),
        justifyContent: 'center', 
        alignItems: 'center', 
        flexDirection: 'row',
        marginBottom: normalize(100),
        marginTop: normalize(20)
    },
    noDone: {
        width: 8,
        height: 8,
        borderRadius: 4,
        backgroundColor: '#3497FD'
    },
    save: {
        backgroundColor: '#41cce1',
        borderRadius: normalize(12),
        zIndex: 99999,
        width: '100%',
        paddingVertical: normalize(14),
        alignItems: 'center'
    }
});
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
      fontSize: normalize(15),
      paddingVertical: normalize(8),
      borderWidth: 1,
      borderColor: '#c3c3c3',
      borderRadius: normalize(10),
      color: '#78849E',
      paddingLeft: normalize(30), // to ensure the text is never behind the icon
    },
    inputAndroid: {
      fontSize: normalize(15),
      paddingVertical: normalize(8),
      borderWidth: 1,
      borderColor: '#c3c3c3',
      borderRadius: normalize(10),
      color: '#78849E',
      paddingLeft: 10, // to ensure the text is never behind the icon
      
    }
});