import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import MainHeader from '../components/MainHeader';
import { shared, fonts, margin } from '../assets/styles';
import { Container, Content } from 'native-base';
import { ScrollView,  } from 'react-native-gesture-handler';
import { getStates } from '../api';
import { showToast } from '../shared/global';
import store from '../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
import { _e } from '../lang';

let pageTitle = '企業検索｜都道府県'
export default class AddAfterStates extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        loaded: true,
        states: []
    };
  }
  componentDidMount(){
    let token = store.getState().user.token;
    this.setState({loaded: false});
    getStates(token)
    .then((response) => {
        this.setState({loaded: true});
        if(response.status) {
            this.setState({states: response.data})
        }
        else {
            showToast(response.msg);
        }
    })
    .catch((error) => {
        this.setState({loaded: true});
        showToast();
    })
  }
  renderState(){
      return this.state.states.map((state) => {
          return (
              <TouchableOpacity style={[margin.py5, margin.pl3, margin.mr12, styles.location_info, styles.location_bottom]} onPress={() => this.getCities(state.id, state.value)}>
                <Text style={[fonts.size15, {color: '#2a2e43'}]}>{state.value}</Text>
              </TouchableOpacity>
          )
      })
  }
  getCities(id, value){
      Actions.push("addaftercities", { id: id, day: this.props.day, value: value})
  }
  render() {
    return (
      <Container  style={shared.container}>
        <MainHeader title={pageTitle}></MainHeader>
        <Content contentContainerStyle={{ flex:1 , display: 'flex' , paddingTop: 10}}>
          <ScrollView style={{padding: 0 , margin: 0}}>
            <View style={margin.mt4}>
                
              {
                this.state.states.length > 0 ?
                  this.renderState()
                  :
                  <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Text>{_e.noState}</Text>
                  </View>
              }
              
            </View>
            
          </ScrollView>
          <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
        </Content>
      </Container>
    );
  }
}

AddAfterStates.navigationOptions = {
  header: null
}

const styles = StyleSheet.create({
  location_info: {
    marginLeft: 50
  },
  location_bottom: {
    borderBottomWidth: 1,
    borderColor: '#C3C9D6'
  }
});