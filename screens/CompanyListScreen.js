import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import MainHeader from '../components/MainHeader';
import CompanyItem from '../components/CompanyItem';
import { Container, Content } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import { shared, fonts, margin, normalize } from '../assets/styles';
import Images from "../assets/Images";
import { getCompanyListBySearch } from '../api';
import store from '../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { showToast } from '../shared/global';
import { _e } from '../lang';

let pageTitle = '営業リスト'
var _self = null;
export default class CompanyListScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loaded: false,
            data: []
        }
    }
    componentDidMount(){
        _self = this;
        getCompanyListBySearch(store.getState().user.token, _self.props.category_id,
                          _self.props.state_id, _self.props.cityIds, _self.props.businessTypeIds, _self.props.predictTypeId)
        .then((response) => {
            console.log('response---', response)
            _self.setState({loaded: true});
            if(response.status) {
                _self.setState({
                    data: response.data
                })
            }
            else
                showToast();
        })
        .catch((error) => {
            console.log('eerror', error)
            _self.setState({loaded: true});
            showToast();
        })
    }
    render() {
        return(
            <Container  style={shared.container}>
                <MainHeader title={pageTitle}></MainHeader>
                <Content contentContainerStyle={{ flex:1 , display: 'flex'}}>
                    <ScrollView style={{margin: 0}}>
                        <View style={[margin.px5, margin.pb5, margin.mt2, {display: 'flex'}]}>
                            {
                                this.state.data.length < 1 && this.state.loaded ?
                                    (
                                        <View>
                                            <Text style={{ textAlign: 'center' }}>{ _e.noCompanyList }</Text>
                                        </View>
                                    ) :
                                    Object.values(this.state.data).map((item, key) => {
                                        return (
                                            <View key={key} style={[margin.mt2]}>
                                                <CompanyItem companyId={item.companyId} name={item.companyTitle} address={item.receiveAddress} phone={item.phone} image={item.images != null ? JSON.parse(item.images)[0] : null} />
                                            </View>
                                        );
                                    })
                            }
                        </View>
                    </ScrollView>
                </Content>
                <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
            </Container>
        )
    }
}