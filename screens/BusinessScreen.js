import React from 'react';
import { StyleSheet, View, Platform, Text, Image, TouchableOpacity,TextInput} from 'react-native';
import BadgeComponent from '../components/BadgeComponent';
import BusinessInfoItem from '../components/BusinessInfoItem';
import PlusButton from '../components/PlusButton';
import { shared, fonts, margin, normalize, getScreenWidth,  } from '../assets/styles';
import Images from "../assets/Images";
import { ScrollView } from 'react-native-gesture-handler';
import  MapView, { Marker } from 'react-native-maps';
import { getCompanyDetail, getLastActionDate, getMemberLog, getBusinessType, createAfter } from '../api';
import { showToast } from '../shared/global';
import store from '../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
import { _e } from '../lang';
import moment from 'moment';
import { EvilIcons, SimpleLineIcons } from '@expo/vector-icons';
import Constants from "expo-constants";
import { Feather } from '@expo/vector-icons';
import Modal from 'react-native-modal';
import RNPickerSelect from 'react-native-picker-select';
import Layout from '../constants/Layout'
import SettingIcon from './../assets/images/path.svg';
let pageTitle = ''
const predict = [
  '', '高', '中', '低'
]
export default class BusinessScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
          region: {
              latitude: 34.732126,
              longitude: -134.8244116,
              latitudeDelta: 0.05864195044303443,
              longitudeDelta: 0.050142817690068,
          },
          loaded: true,
          companyInfo: null,
          lastDate: '',
          logs: [],
          bussinesType: [],
          modalVisible: false, 
          method: 0,
          business: '',
          detail: '',
          homeImage: ''
      };
    }
    componentDidMount(){
      this.refresh();
    }

    UNSAFE_componentWillReceiveProps(){
      this.refresh();
    }

    refresh(){
      let token = store.getState().user.token;
      this.setState({loaded: false});
      getLastActionDate(this.props.companyId, token)
      .then((response) => {
          this.setState({loaded: true});
          if(response.status && response.data != undefined) {
            this.setState({lastDate: response.data.lastDate})
          }
      })

      getBusinessType()
      .then((response) => {
          if(response.status) {
              let temp = []
              for(var i = 0;i<response.data.length;i++){
                  temp.push({label: response.data[i]['value'], value: response.data[i]['id']})
              }
              this.setState({bussinesType: temp})
          }
          else
              showToast(response.msg);
      })

      getMemberLog(this.props.companyId, token)
      .then((response) => {
          this.setState({loaded: true});
          if(response.status) {
            this.setState({logs: response.data})
          }
      })

      getCompanyDetail(this.props.companyId, token)
      .then((response) => {
          this.setState({loaded: true});
          if(response.status) {
            let temp = {
              latitude: parseFloat(response.data[0].mainLat),
              longitude: parseFloat(response.data[0].mainLon),
              latitudeDelta: 0.05864195044303443,
              longitudeDelta: 0.050142817690068,
            }
            if(response.data[0]['homeImage']){
              this.setState({homeImage: response.data[0]['homeImage']})
            }
            else if(response.data[0]['images']){
              let images = JSON.parse(response.data[0]['images'])
              this.setState({homeImage: images[0]})
            }
            this.setState({region: temp})
            this.setState({companyInfo: response.data[0]})
          }
          else {
              showToast(response.msg);
          }
      })
      .catch((error) => {
          this.setState({loaded: true});
          showToast();
      })
    }

    headerBKColor() {
      if(this.props.hasOwnProperty('transparent') && this.props.transparent)
          return {
              backgroundColor: 'transparent',
              zIndex: 1
          }
      else
          return {
              backgroundColor: 'white'
          }
    }

    renderLogs(){
      return this.state.logs.map((log) => {
        return <BusinessInfoItem
          item={moment(log.regDate).format("YYYY/MM/DD HH:mm")}
          type='business'
          bkColor={log.action == 'アフター' ? '#2a2e43' : '#FF9057'}
          text={log.action}
        />
      })
    }
    
    showModal(){
      this.setState({modalVisible: true})
    }

    closeModal(){
      this.setState({modalVisible: false})
    }

    save(){
      let token = store.getState().user.token;
        this.setState({loaded: false})
        createAfter(moment().format("YYYY-MM-DD"), this.props.companyId, this.state.method, this.state.detail, 1, token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                this.setState({method: ''})
                this.setState({business: ''})
                this.setState({detail: ''})
                this.setState({modalVisible: false})
                this.refresh();
            }
            else {
              this.setState({modalVisible: false})
              showToast(response.msg);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        })
    }

    render(){
      const placeholder = {
        label: '選択',
        value: null,
        color: '#9EA0A4',
      };
      
        return (
          <View style={shared.container}>
            {
                  this.state.companyInfo != null ?
              <ScrollView style={{padding: 0 , margin: 0}}>
                
                <View style={Platform.OS == 'android' ? [styles.backBtn] : [styles.backBtn, {top: 35}]}>
                    <TouchableOpacity onPress={() => Actions.pop()} style={{ alignItems: 'flex-start' }} >
                        <EvilIcons
                            name='chevron-left'
                            size={42}
                            color='white'
                            />
                    </TouchableOpacity>
                </View>
                
                
                  <View>
                    <View style={[styles.bannerContainer , {position: 'relative'}]}>
                      {
                        this.state.homeImage != '' && this.state.homeImage != null ?
                        <Image source={{uri: Layout.serverLink + this.state.homeImage}} style={styles.businessImage} resizeMethod={"resize"} resizeMode="stretch" />
                        :
                        <Image source={Images.business_img} style={styles.businessImage} />
                      }
                        
                      
                      
                      <View style={[styles.businessInfo, margin.px6]}>
                        <View style={{width: getScreenWidth() - 100}}>
                          <Text style={styles.businessName}>
                            {this.state.companyInfo.companyTitle}
                          </Text>
                          <Text style={[styles.businessTime, margin.mt2]}>
                            直近アクション：{this.state.lastDate != '' ? moment(this.state.lastDate).format("YYYY/MM/DD") : ''}
                          </Text>
                        </View>
                        {
                          this.state.companyInfo.predictTypeId == 1 ?
                          <BadgeComponent
                            width={53}
                            height={53}
                            bkColor='#62BC56'
                            color='white'
                            text={predict[1]}
                          />
                          :
                          this.state.companyInfo.predictTypeId == 2 ?
                          <BadgeComponent
                            width={53}
                            height={53}
                            bkColor='#FF9057'
                            color='white'
                            text={predict[2]}
                          />
                          :
                          this.state.companyInfo.predictTypeId == 3 ?
                          <BadgeComponent
                            width={53}
                            height={53}
                            bkColor='#41CCE1'
                            color='white'
                            text={predict[3]}
                          />
                          :
                          null
                        }
                        
                      </View>
                    </View>
                    <View style={[{flex: 1, position: 'relative', borderTopLeftRadius: 10, borderTopRightRadius: 10, backgroundColor: '#F8F8F8'}, margin.pt5, margin.pb10, margin.px6, styles.mainPanelPosition]}>
                      <View style={[{display: 'flex', flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'space-between',
                                    borderBottomWidth: 1, borderColor: '#EAEAEA'},
                                    margin.pb3]}>
                        <View style={margin.mt5}>
                          <Text style={[fonts.size16, {color: '#454f63'}]}>
                            {this.state.companyInfo.business}
                          </Text>
                          <Text style={[fonts.size14, margin.mt2, {color: '#454f63'}]}>
                            {this.state.companyInfo.receiveAddress}
                          </Text>
                        </View>
                        <View style={{display: 'flex', alignItems: 'center'}}>
                          <TouchableOpacity onPress={() => Actions.push("businesseditscreen", { companyId: this.props.companyId})}>
                            <SettingIcon width={25} height={25} />
                          </TouchableOpacity>
                          <Text style={[fonts.size13, margin.mt3, {color: '#78849e'}]}>
                            設定
                          </Text>
                        </View>
                      </View>
                      <View style={[{display: 'flex', flexDirection: 'row', alignItems: 'center'}, 
                                  margin.mt3]}>
                            <PlusButton
                                color="#2a2e43"
                                width={34}
                                height={34}
                                size={20}
                                type="phone"
                                borderRadius={8}
                            >
                            </PlusButton>
                            <Text style={[fonts.size20, margin.ml3]}>
                              {this.state.companyInfo.phone}
                            </Text>
                      </View>
                      <View style={[{height: normalize(200)}, margin.mt6]}>
                        <MapView style={{width: '100%', height: '100%'}}
                            region={this.state.region}>
                              {
                                this.state.companyInfo.mainLat != null && this.state.companyInfo.mainLat != '' && this.state.companyInfo.mainLon != null && this.state.companyInfo.mainLon != '' ?
                                <MapView.Marker coordinate={{latitude: parseFloat(this.state.companyInfo.mainLat), longitude: parseFloat(this.state.companyInfo.mainLon)}} centerOffset={Platform.OS == 'android' ? {x: 20, y: -20} : {x: 0, y: 0}} anchor={Platform.OS == 'android' ? {x: 0.5, y: 0.5} : {x: 0, y: 0}}>
                                    <Image source={require('../assets/images/marker.png')} style={{height: 40, width: 40}} resizeMethod="resize" resizeMode="contain" />
                                </MapView.Marker>
                                :
                                null
                              }
                            
                        </MapView>
                      </View>
                      <View style={[{ backgroundColor: 'white',
                                    borderRadius: 12},
                                    margin.mt6,
                                    margin.px4,
                                    margin.py4,
                                    styles.panelShadow]}>
                          
                          <View style={{ display: 'flex', alignItems: 'center'}}>
                            <TouchableOpacity style={{backgroundColor: '#78849E', width: 44, height: 44, borderRadius: 22, justifyContent: 'center', alignItems: 'center'}} onPress={() => this.showModal()}>
                              <Feather
                                name='plus'
                                size={20}
                                color='white'
                              />
                            </TouchableOpacity>
                          </View>
                          
                          <View>
                            {
                              this.state.logs.length > 0 ?
                              this.renderLogs()
                              :
                              <View style={{alignItems: 'center', justifyContent: 'center', marginTop: 10}}>
                                <Text>履歴がありません。</Text>
                              </View>
                            }
                            
                          </View>
                      </View>
                    </View>
                  </View>
                  
                
              </ScrollView>
                :
                null
              }
              <Modal
                isVisible={this.state.modalVisible}
                backdropOpacity={0.5}
                avoidKeyboard={true}
                onBackButtonPress={() => this.closeModal()}
                onBackdropPress={() => this.closeModal()}
            >
                
                <View style={{backgroundColor: 'white', flex: 1, margin: 20, padding: 20, }}>
                    <ScrollView>
                        <View style={{borderBottomColor: '#707070', borderBottomWidth: 1, paddingBottom: 20}}>
                            <Text style={[fonts.size20, {color: '#454f63'}]}>株式会社あいうえお</Text>
                        </View>
                        <View style={{borderBottomColor: '#707070', borderBottomWidth: 1, paddingVertical: 20, flexDirection: 'row'}}>
                            {
                              /*<View style={{alignItems: 'center', justifyContent: 'center', flex: 1}}>
                                <Text style={{marginBottom: 10}}>種別</Text>
                                <RNPickerSelect
                                    placeholder={placeholder}
                                    items={this.state.bussinesType}
                                    onValueChange={value => { this.setState({business: value}) }}
                                    style={ pickerSelectStyles }
                                    value={this.state.business}
                                    useNativeAndroidPickerStyle={false}
                                />
                              </View>*/
                            }
                            
                            <View style={{justifyContent: 'center', flex: 1}}>
                                <Text style={{marginBottom: 10}}>方法</Text>
                                <RNPickerSelect
                                    placeholder={placeholder}
                                    items={Layout.method}
                                    onValueChange={value => { this.setState({method: value}) }}
                                    style={ pickerSelectStyles }
                                    value={this.state.method}
                                    useNativeAndroidPickerStyle={false}
                                />
                            </View>
                            
                            
                        </View>
                        <View style={{ paddingVertical: 20, flexDirection: 'row', borderBottomColor: '#707070', borderBottomWidth: 1, marginBottom: 15}}>
                            <TextInput 
                                multiline={true}
                                numberOfLines={5}
                                defaultValue={this.state.detail}
                                onChangeText={value => this.setState({detail: value})}
                                style={{height: 100, width: '100%', justifyContent: 'flex-start', textAlignVertical: 'top'}}
                            />
                        </View>
                        <TouchableOpacity onPress={() => this.save()} style={styles.save}>
                          <Text style={[{color: 'white' }, fonts.size20]}>保存</Text>
                        </TouchableOpacity>
                    </ScrollView>
                </View>
                
            </Modal>
              <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
          </View>
        );
    }
}
BusinessScreen.navigationOptions = {
  header: null
}
const styles = StyleSheet.create({
    businessImage: {
      width: '100%',
      height: Layout.window.width * 0.9
    },
    settingImage: {
      width: 20,
      height: 20
    },
    bannerContainer: {
      //marginTop: Platform.OS === 'ios' ? -110 : -70
    },
    mainPanelPosition: {
      marginTop: Platform.OS === 'ios' ? -50 : -60
    },
    businessInfo: {
        position: 'absolute',
        left: 0,
        bottom: Platform.OS === 'ios' ? 78 : 88,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%'
    },
    businessName: {
        color: 'white',
        fontSize: normalize(22)
    },
    businessTime: {
      color: 'white',
      fontSize: normalize(12)
    },
    mapSize: {
      height: normalize(200)
    },
    header: {
      height:60,
      zIndex:10001,
      position: 'relative',
      //marginTop:  Platform.OS === 'android' ? -Constants.statusBarHeight : 0
    },
    panelShadow: Platform.OS === 'ios' ? {
      shadowColor: "rgba(69, 91, 99, 0.3)",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.82,
      shadowRadius: 0.41,
      elevation: 0.5,
    } : {
      shadowColor: "rgba(69, 91, 99, 0.3)",
      shadowOffset: {
        width: 0,
        height: 1,
      },
      shadowOpacity: 0.20,
      shadowRadius: 0.11,
      elevation: 0.5,
    },
    save: {
        backgroundColor: '#41cce1',
        borderRadius: normalize(12),
        zIndex: 99999,
        width: '100%',
        paddingVertical: normalize(14),
        alignItems: 'center'
    },
    backBtn: { 
      position: 'absolute' , 
      left: 10, 
      zIndex: 99999, 
      top: 10
    }

});
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
    fontSize: normalize(15),
    paddingVertical: normalize(8),
    borderWidth: 1,
    borderColor: '#c3c3c3',
    borderRadius: normalize(10),
    color: '#78849E',
    paddingLeft: normalize(30), // to ensure the text is never behind the icon
  },
  inputAndroid: {
    fontSize: normalize(15),
    paddingVertical: normalize(8),
    borderWidth: 1,
    borderColor: '#c3c3c3',
    borderRadius: normalize(10),
    color: '#78849E',
    paddingLeft: 10, // to ensure the text is never behind the icon
  },

});