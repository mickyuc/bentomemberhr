import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import MainHeader from '../components/MainHeader';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import MessageTimeHeader from '../components/MessageTimeHeader';
import ActionButton from '../components/ActionButton';
import { Container, Content,  } from 'native-base';
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import { shared, margin, fonts } from '../assets/styles';
import { setMessageCheck, getMessageDetail, deleteMessage } from '../api';
import { showToast } from './../shared/global';
import store from './../store/configuteStore';
import { _e } from './../lang';

const pageTitle = 'メッセージ'

export default class MessageScreen extends React.Component {
    constructor(props){
        super(props);

        this.state = {
            title: '',
            content: '',
            date: '',
            loaded: true
        }
    }

    componentDidMount() {
        const userToken = store.getState().user.token;
        getMessageDetail(this.props.messageid, userToken)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                const msgData = response.data;
                const date1 = new Date();
                const date2 = new Date(msgData.date);
                const diffTime = Math.abs(date2 - date1);
                const diffDays = Math.ceil(diffTime / (1000 * 60 * 60 * 24)); 
                const timeVal = parseInt(msgData.date.substr(11, 2));
                const timeStr = timeVal > 12 ? ("0" + (timeVal - 12)).substr(-2) + msgData.date.substr(13, 3) + " PM" : msgData.date.substr(11, 5) + " AM";
                const dateStr = diffDays == 1 ? "昨日, " + timeStr: msgData.date.substr(0, 10) + " " + timeStr;
                this.setState({
                    title: msgData.title,
                    content: msgData.content,
                    date: dateStr
                });

                if(msgData.status == 1) {
                    setMessageCheck(msgData.id, userToken);
                }
            } else {
                showToast(_e.InvalidToken);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast(error);
        });
    }

    removeMessage = () => {
        this.setState({loaded: false});
        const { navigation } = this.props;
        const userToken = store.getState().user.token;
        deleteMessage(this.props.messageid, userToken)
        .then((resp) => {
            this.setState({loaded: true});
            if(resp.status) {
                navigation.navigate('message_screen');
            } else {
                showToast(_e.InvalidToken);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast(error);
        });
    }

    render() {
        const { title, content, loaded, date } = this.state;
        return(
            <Container  style={shared.container}>
                <MainHeader title={pageTitle}></MainHeader>
                <Content contentContainerStyle={{ flex:1 , display: 'flex'}}>
                    <ScrollView style={{margin: 0}}>
                        <View style={[margin.px5, margin.pb5, {width: '100%'}]}>
                            <View style={margin.mt3}>
                                <MessageTimeHeader
                                    time={date}
                                />
                            </View>
                            {
                                title.length > 0 ?
                                <View style={[
                                    margin.pt4,
                                    margin.pb4,
                                    margin.pl6,
                                    margin.pr4,
                                    { 
                                        backgroundColor: '#78849e',
                                        borderRadius: 10
                                    }, 
                                    margin.mt3
                                ]}>
                                    <Text style={[{ color: 'white' }, fonts.size14]}>
                                        { title }
                                    </Text>
                                </View> : null
                            }
                            {
                                content.length > 0 ? 
                                <View style={[
                                    margin.pt4,
                                    margin.pb4,
                                    margin.pl6,
                                    margin.pr4,
                                    { 
                                        backgroundColor: '#5773ff',
                                        borderRadius: 10
                                    }, 
                                    margin.mt3
                                ]}>
                                    <Text style={[{ color: 'white' }, fonts.size14]}>
                                        { content }
                                    </Text>
                                </View> : null
                            }
                            {
                                title.length > 0 && content.length > 0 ?
                                <View style={margin.mt5}>
                                    <ActionButton customClickEvent={this.removeMessage.bind(this)} color="#41cce1" title={_e.DeleteMsg} borderRadius={12} iconType="FontAwesome" name="trash-o"></ActionButton>
                                </View>
                                : null
                            }
                        </View>
                    </ScrollView>
                </Content>
                <Spinner_bar color={'#27cccd'} visible={!loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    trashBtn: {
        borderRadius: 50,
        backgroundColor: '#82a3ea',
        marginTop: 10,
        width: 50,
        padding: 10,
        justifyContent: 'center',
        textAlign: 'center'
    }
});