import React from 'react';
import { StyleSheet, View, Text} from 'react-native';
import MainHeader from '../components/MainHeader';
import CheckBoxLine from '../components/CheckBoxLine';
import { shared, fonts, margin } from '../assets/styles';
import { Container, Content } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import { getCities } from './../api';
import { showToast } from '../shared/global';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
import store from './../store/configuteStore';
import ActionButton from '../components/ActionButton';
import { _e } from './../lang';

let pageTitle = '企業検索｜市区町村'
var _self = null;
export default class BusinessLocationDScreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      data: [],
      loaded: false,
      choose: false
    };
  }
  componentDidMount() {
    _self = this;
    this.fetchCity()
  }
  fetchCity() {
    let self = this
    this.setState({loaded: false});
    getCities(this.props.state_id)
    .then((response) => {
      self.setState({loaded: true});
      if(response.status) {
        self.setState({
          data: response.data
        })
      }
      else
        showToast();
    })
    .catch((error) => {
      self.setState({loaded: true});
      showToast();
    })
  }
  bindCheckChangeTrigger(id, value) {
    let cities = _self.state.data, isChecked = false
    for (let i = 0; i < cities.length; i ++)
      if(cities[i]['id'] == id) {
        cities[i]['checked'] = value
        break;
      }
    _self.setState({
      data: cities
    })
    for(let i = 0; i < cities.length; i ++)
      if(cities[i]['checked']) {
        _self.setState({choose: true})
        isChecked  = true
        break;
      }
    if(!isChecked)
      _self.setState({choose: false})
  }
  chooseBusinessType() {
    let cities = _self.state.data
    let cityids = [], citynames = []
    for (let i = 0; i < cities.length; i ++)
      if(cities[i].hasOwnProperty('checked') && cities[i]['checked']) {
        cityids.push(cities[i]['id'])
        citynames.push(this.props.state_name + cities[i]['value'])
      }
    // console.log(cityids)
    if(cityids.length < 1) {
      showToast(_e.cityNoSelect)
      return;
    }
    Actions.push('business_type_screen', {state_id: this.props.state_id,
                                          state_name: this.props.state_name,
                                          category_id: this.props.category_id,
                                          category_name: this.props.category_name,
                                          cityids: cityids,
                                          citynames: citynames})
  }
  render() {
    return (
      <Container  style={shared.container}>
        <MainHeader title={pageTitle}></MainHeader>
        <Content contentContainerStyle={{ flex:1 , display: 'flex' , paddingTop: 10}}>
          <ScrollView style={{padding: 0 , margin: 0}}>
            <View style={margin.mt4, margin.pb5}>
              <View style={[{backgroundColor: '#78849E', paddingVertical: 12, width: '50%'}, margin.pl15]}>
                <Text style={[fonts.size18, {color: 'white'}]}> { this.props.state_name } </Text>
              </View>
              {
                Object.values(this.state.data).map((item, key) => {
                  return (
                        <CheckBoxLine key={key} title={ item.value } id={ item.id } 
                                      parentMethod={this.bindCheckChangeTrigger}></CheckBoxLine>
                  );
                })
              }
            </View>
          </ScrollView>
        </Content>
        {
          this.state.choose ?
          <View>
              <ActionButton title="業種を選択する"
              customClickEvent={this.chooseBusinessType.bind(this)}></ActionButton>
          </View> :
          <View></View>
        }
        <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
      </Container>
    );
  }
}

BusinessLocationDScreen.navigationOptions = {
  header: null
}