import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import MainHeader from '../components/MainHeader';
import ActionButton from '../components/ActionButton';
import { CheckBox } from 'react-native-elements';
import { shared, fonts, margin } from '../assets/styles';
import { Container, Content } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import { getCites } from '../api';
import { showToast } from '../shared/global';
import store from '../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
import { _e } from '../lang';

let pageTitle = '企業検索｜市区町村'
export default class AddAfterCities extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isSelected: false,
      loaded: true,
      cities: [],
      checked: []
    };
  }
  componentDidMount(){
    let token = store.getState().user.token;
    this.setState({loaded: false});
    getCites(this.props.id, token)
    .then((response) => {
        this.setState({loaded: true});
        if(response.status) {
            this.setState({cities: response.data})
            let temp = []
            for(var i = 0;i<response.data.length;i++){
                temp.push(false)
            }
            this.setState({checked: temp})
        }
        else {
            showToast(response.msg);
        }
    })
    .catch((error) => {
        this.setState({loaded: true});
        showToast();
    })
  }
  setSelection(index) {
    this.state.checked[index] = !this.state.checked[index]
    this.setState({checked: this.state.checked})
  }
  renderCities(){
      return this.state.cities.map((city,index) => {
          return <TouchableOpacity style={[styles.location_d, margin.mr12, styles.location_info, styles.location_bottom, styles.checkboxContainer]} onPress={() => this.searchCompany(city.id, city.value)}>
              {/*
                <CheckBox
                    title={city.value}
                    onPress={() => {this.setSelection(index)}}
                    checked={this.state.checked[index]}
                    containerStyle={{ backgroundColor: 'white' , borderWidth: 0 , marginLeft: -8}}
                    textStyle={[{ color: '#2a2e43', fontWeight: '400' }, fonts.size15]}
                    uncheckedColor='#707070'
                    size={22}
                />*/
              }
                <Text style={{paddingVertical: 10}}>{city.value}</Text>
            </TouchableOpacity>
      })
  }
  searchCompany(id, value){
    Actions.push("aftercompanylist", {state: this.props.value, city: value, cityId: id, stateId: this.props.id, day : this.props.day})
  }
  render() {
    return (
      <Container  style={shared.container}>
        <MainHeader title={pageTitle}></MainHeader>
        <Content contentContainerStyle={{ flex:1 , display: 'flex' , paddingTop: 10}}>
          <ScrollView style={{padding: 0 , margin: 0}}>
            <View style={margin.mt4, margin.pb5}>
              <View style={[{backgroundColor: '#78849E', paddingVertical: 12, width: '50%'}, margin.pl15]}>
                <Text style={[fonts.size18, {color: 'white'}]}>{this.props.value}</Text>
              </View>
              {
                  this.state.cities.length > 0 ?
                  this.renderCities()
                  :
                  <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Text>{_e.noCity}</Text>
                  </View>
              }
            </View>
          </ScrollView>
          {/*
            <View style={{ width: '100%', backgroundColor: '#3497FD'}}>
                <TouchableOpacity onPress={() => this.searchCompany()} style={{justifyContent: 'center', alignItems: 'center', width: '100%', paddingVertical: 20}}>
                    <Text style={[fonts.size15, {color: 'white'}]}>企業検索</Text>
                </TouchableOpacity>
            </View>*/
          }
          
        </Content>
        
        <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
      </Container>
    );
  }
}

AddAfterCities.navigationOptions = {
  header: null
}

const styles = StyleSheet.create({
    location_info: {
      marginLeft: 50
    },
    location_bottom: {
      borderBottomWidth: 1,
      borderColor: '#C3C9D6'
    },
    location_d: {
      paddingVertical: 6,
    }
});