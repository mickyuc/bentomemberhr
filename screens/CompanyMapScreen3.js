import React from 'react';
import { StyleSheet, Text, View,  Animated, Image, Dimensions, TouchableOpacity, Platform} from 'react-native';
import MainHeader from '../components/MainHeader';
import { shared, margin, normalize } from '../assets/styles';
import { Container, Content } from 'native-base';
import MapView, { Polyline } from 'react-native-maps';
import { getCompanyListBySearch } from '../api';
import store from '../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { showToast } from '../shared/global';
import PlusButton from '../components/PlusButton';
import {Actions} from 'react-native-router-flux';
import { ASSET_URL } from '../constants/Global';
//import { TouchableHighlight } from 'react-native-gesture-handler';
TouchableOpacity.defaultProps = { activeOpacity: 0.6 };
let pageTitle = '地図を表示'
let asset_url = ASSET_URL;
const Images = [
    { uri: "https://i.imgur.com/sNam9iJ.jpg" },
    { uri: "https://i.imgur.com/N7rlQYt.jpg" },
    { uri: "https://i.imgur.com/UDrH0wm.jpg" },
    { uri: "https://i.imgur.com/Ka8kNST.jpg" }
]
const { width, height } = Dimensions.get("window");
const CARD_HEIGHT = height / 5;
const CARD_WIDTH = width - 80;
var _self = null;
export default class CompanyMapScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loaded: true,
            index: 0,
            markers: [],
            region: {
                latitude: 34.6923552,
                longitude: 134.8607082,
                latitudeDelta: 0.3,
                longitudeDelta: 0.1,
            },
            coordinate: []
        };
    }
    UNSAFE_componentWillMount(){
        //this.index = 0;
        this.setState({index: 0})
        this.animation = new Animated.Value(0);
    }
    componentDidMount(){
        _self = this;
        _self.setState({loaded: false});
        getCompanyListBySearch(store.getState().user.token, _self.props.category_id,
                          _self.props.state_id, _self.props.cityIds, _self.props.businessTypeIds, _self.props.predictTypeId)
        .then((response) => {
            _self.setState({loaded: true});
            if(response.status) {
                let watch_list = [];
                for( let i = 0; i < response.data.length; i ++ ) {
                    watch_list.push( {
                        coordinate: {
                            latitude: parseFloat(response.data[i]['mainLat']),
                            longitude: parseFloat(response.data[i]['mainLon']),
                        },
                        title: response.data[i]['companyTitle'],
                        description: response.data[i]['receiveAddress'],
                        phone: response.data[i]['phone'],
                        //image: Images[0],
                        image: response.data[i]['images'] == null ? Images[0] : { 'uri' : asset_url + JSON.parse(response.data[i]['images'])[0] } ,
                        companyId: response.data[i]['companyId']
                    } )
                }
                if(response.data.length > 0) {
                    _self.setState({
                        markers: watch_list
                    })
                }
                let temp = [];
                _self.state.markers.map((marker) => {
                    temp.push(marker.coordinate)
                })
                _self.setState({coordinate: temp})
                _self.animation.addListener(({ value }) => {
                    let index = Math.floor(value / CARD_WIDTH + 0.3); // animate 30% away from landing on the next item
                    if (index >= _self.state.markers.length) {
                        index = _self.state.markers.length - 1;
                    }
                    if (index <= 0) {
                        index = 0;
                    }
                    clearTimeout(_self.regionTimeout);
                    _self.regionTimeout = setTimeout(() => {
                    if (_self.state.index !== index) {
                        //this.index = index;
                        _self.setState({index: index})
                        _self.setState({markers: _self.state.markers})
                        const { coordinate } = _self.state.markers[index];
                        _self.map.animateToRegion(
                        {
                            ...coordinate,
                            latitudeDelta: _self.state.region.latitudeDelta,
                            longitudeDelta: _self.state.region.longitudeDelta,
                        },
                        350
                        );
                    }
                    }, 10);
                });
            }
            else
                showToast();
        })
        .catch((error) => {
            _self.setState({loaded: true});
            showToast();
        })
    }
    businessScreen(companyId) {
        Actions.push('businessscreen', {
            'companyId': companyId
        })
    }
    render() {
        
        const interpolations = this.state.markers.map((marker, index) => {
            const inputRange = [
              (index - 1) * CARD_WIDTH,
              index * CARD_WIDTH,
              ((index + 1) * CARD_WIDTH),
            ];
            const scale = this.animation.interpolate({
              inputRange,
              outputRange: [1, 5, 1],
              extrapolate: "clamp",
            });
            const opacity = this.animation.interpolate({
              inputRange,
              outputRange: [1, 1, 1],
              extrapolate: "clamp",
            });
            return { scale, opacity };
        });
        return (
        <Container style={[shared.container , {backgroundColor: '#f2f2f2'}]}>
            <MainHeader title={pageTitle + ':' + this.props.state_name}></MainHeader>
            {
                <MapView style={{width: '100%', height: '100%'}} 
                    initialRegion={this.state.region}
                    ref={map => this.map = map}
                >
                    {this.state.markers.map((marker, index) => {
                        const scaleStyle = {
                            transform: [
                            {
                                scale: interpolations[index].scale,
                            },
                            ],
                        };
                        const opacityStyle = {
                            opacity: interpolations[index].opacity,
                        };
                        return (
                            this.state.index == index ?
                            <MapView.Marker key={index} coordinate={marker.coordinate} centerOffset={Platform.OS == 'android' ? {x: 20, y: -20} : {x: 0, y: 0}} anchor={Platform.OS == 'android' ? {x: 0.5, y: 0.5} : {x: 0, y: 0}}>
                                <Image source={require('../assets/images/marker1.png')} style={{height: 40, width: 40, zIndex: 999}} resizeMethod="resize" resizeMode="contain" />
                            </MapView.Marker>
                            :
                            <MapView.Marker key={index} coordinate={marker.coordinate} anchor={{x: 0.5, y: 0.5}}>
                                <Animated.View style={[styles.markerWrap, opacityStyle]}>
                                    <Animated.View style={[ scaleStyle]} />
                                    <View style={styles.marker} >
                                    </View>
                                </Animated.View>
                            </MapView.Marker>
                        );
                    })}
                </MapView>
            }
            {
                this.state.markers.length > 0 ?
                <MapView style={{width: '100%', height: '100%'}} 
                    region={this.state.region}
                    ref={map => this.map = map}
                >
                    {this.state.markers.map((marker, index) => {
                        const scaleStyle = {
                                transform: [
                                {
                                    scale: interpolations[index].scale,
                                },
                            ],
                        };
                        const opacityStyle = {
                            opacity: interpolations[index].opacity,
                        };
                        return (
                            this.state.index == index ?
                            <MapView.Marker key={index} coordinate={marker.coordinate} centerOffset={Platform.OS == 'android' ? {x: 20, y: -20} : {x: 0, y: 0}} anchor={Platform.OS == 'android' ? {x: 0.5, y: 0.5} : {x: 0, y: 0}}>
                                <Image source={require('../assets/images/marker1.png')} style={{height: 40, width: 40, zIndex: 1000}} resizeMethod="resize" resizeMode="contain" />
                            </MapView.Marker>
                            :
                            <MapView.Marker key={index} coordinate={marker.coordinate} anchor={{x: 0.5, y: 0.5}}>
                                <Animated.View style={[styles.markerWrap, opacityStyle]}>
                                    <Animated.View style={[ scaleStyle]} />
                                    <View style={styles.marker} >
                                    </View>
                                </Animated.View>
                            </MapView.Marker>
                        );
                    })}
                    </MapView>
                :
                <View></View>
            }
            {
                this.state.markers.length > 0 ?
                <Animated.ScrollView
                horizontal
                scrollEventThrottle={1}
                showsHorizontalScrollIndicator={false}
                snapToInterval={CARD_WIDTH}
                onScroll={Animated.event(
                    [
                        {
                            nativeEvent: {
                            contentOffset: {
                                x: this.animation,
                            },
                            },
                        },
                    ],
                    { useNativeDriver: true }
                )}
                style={styles.scrollView}
                contentContainerStyle={styles.endPadding}
                >
                {this.state.markers.map((marker, index) => (
                        <TouchableOpacity style={styles.card} key={index}
                        onPress={() => this.businessScreen(marker.companyId)}>
                            <View style={{width: '40%', paddingHorizontal: 10, justifyContent: 'flex-start', alignItems: 'center'}}>
                                <Image
                                    source={marker.image}
                                    style={styles.cardImage}
                                    resizeMode="contain"
                                />
                                <Text>訪問</Text>
                            </View>
                            <View style={styles.textContent}>
                                <View style={{flex: 1, borderBottomColor: '#707070', borderBottomWidth: 1}}>
                                    <Text numberOfLines={1} style={styles.cardtitle}>{marker.title}</Text>
                                    <Text style={styles.cardDescription}>
                                        {marker.description}
                                    </Text>
                                </View>
                                <View style={{display: 'flex', alignItems: 'center', flexDirection: 'row', flex: 1}}>
                                    <PlusButton
                                        color="#3497FD"
                                        width={34}
                                        height={34}
                                        size={20}
                                        type="phone"
                                        borderRadius={8}
                                    />
                                    <Text style={margin.ml2}>{ marker.phone }</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                ))}
            </Animated.ScrollView>:
                <View></View>
            }
            <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
        </Container>
        );
    }
}

const styles = StyleSheet.create({
    scrollView: {
        position: "absolute",
        bottom: 20,
        left: 0,
        right: 0,
        paddingVertical: 10,
        backgroundColor: 'rgba(112,112, 112, 0.3)',
    },
    endPadding: {
        paddingRight: width - CARD_WIDTH,
    },
    card: {
        padding: 10,
        elevation: 2,
        backgroundColor: "#FFF",
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: "hidden",
        flexDirection: 'row'
    },
    cardImage: {
        flex: 3,
        width: "100%",
        height: "100%",
        alignSelf: "center",
        marginBottom: 10
    },
    textContent: {
        flex: 1,
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    cardtitle: {
        fontSize: 12,
        marginTop: 5,
        fontWeight: "bold",
    },
    cardDescription: {
        fontSize: 12,
        color: "#444",
    },
    markerWrap: {
        alignItems: "center",
        justifyContent: "center",
    },
    marker: {
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: "#FF9057",
        justifyContent: 'center',
        alignItems: 'center',
        borderWidth: normalize(2),
        borderColor: '#707070'
    },
    ring: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: "rgba(130,4,150, 0.3)",
        position: "absolute",
        borderWidth: 1,
        borderColor: "rgba(130,4,150, 0.5)",
    },
});
