import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import MainHeader from '../components/MainHeader';
import ActionButton from '../components/ActionButton';
import { CheckBox } from 'react-native-elements';
import { shared, fonts, margin } from '../assets/styles';
import { Container, Content } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import { getCompanyByLocation } from '../api';
import { showToast } from '../shared/global';
import store from '../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
import { _e } from '../lang';

let pageTitle = '企業検索'
export default class AfterCompanyList extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isSelected: false,
      loaded: true,
      companies: [],
    };
  }
  componentDidMount(){
    let token = store.getState().user.token;
    this.setState({loaded: false});
    getCompanyByLocation(this.props.stateId, this.props.cityId, token)
    .then((response) => {
        this.setState({loaded: true});
        if(response.status) {
            this.setState({companies: response.data})
        }
        else {
            showToast(response.msg);
        }
    })
    .catch((error) => {
        this.setState({loaded: true});
        showToast();
    })
  }
  
  renderCompany(){
      return this.state.companies.map((company,index) => {
          return <TouchableOpacity style={[styles.location_d, margin.mr12, styles.location_info, styles.location_bottom, styles.checkboxContainer]} onPress={() => this.gotoAddAfter(company.companyId, company.companyTitle)}>
                <Text style={{paddingVertical: 10}}>{company.companyTitle}</Text>
            </TouchableOpacity>
      })
  }
  gotoAddAfter(id, value){
    Actions.push("addafterdetail", {state: this.props.state, city: this.props.city, companyId: id, company: value, day : this.props.day})
  }
  render() {
    return (
      <Container  style={shared.container}>
        <MainHeader title={pageTitle}></MainHeader>
        <Content contentContainerStyle={{ flex:1 , display: 'flex' , paddingTop: 10}}>
          <ScrollView style={{padding: 0 , margin: 0}}>
            <View style={margin.mt4, margin.pb5}>
              <View style={[{backgroundColor: '#78849E', paddingVertical: 12, width: '70%'}, margin.pl15]}>
                <Text style={[fonts.size18, {color: 'white'}]}>{this.props.state} / {this.props.city}</Text>
              </View>
              {
                  this.state.companies.length > 0 ?
                  this.renderCompany()
                  :
                  <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Text>{_e.noCompany}</Text>
                  </View>
              }
            </View>
          </ScrollView>
        </Content>
        
        <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
      </Container>
    );
  }
}

AfterCompanyList.navigationOptions = {
  header: null
}

const styles = StyleSheet.create({
    location_info: {
      marginLeft: 50
    },
    location_bottom: {
      borderBottomWidth: 1,
      borderColor: '#C3C9D6'
    },
    location_d: {
      paddingVertical: 6,
    }
});