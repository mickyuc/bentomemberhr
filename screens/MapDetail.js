import React from 'react';
import { StyleSheet, Text, View,  Animated, Image, Dimensions, ScrollView, TouchableOpacity, Platform} from 'react-native';
import MainHeader from '../components/MainHeader';
import { shared } from '../assets/styles';
import { Container, Content } from 'native-base';
import MapView, { Polyline } from 'react-native-maps';
import { GetRoutes, changeOrderStatus, changeRouteStatus } from './../api';
import { showToast } from './../shared/global';
import store from './../store/configuteStore';
import { _e } from './../lang';
import PlusButton from '../components/PlusButton';
import SwipeCards from 'react-native-swipe-cards';
import Layout from '../constants/Layout';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Entypo, SimpleLineIcons, AntDesign } from '@expo/vector-icons';
import { Actions } from 'react-native-router-flux';
import Constants from "expo-constants";
let pageTitle = '上野松が谷ルート'
const { width, height } = Dimensions.get("window");
const CARD_HEIGHT = height / 5;
const CARD_WIDTH = width - 80;

export default class MapDetail extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            index: 0,
            markers: [
            ],
            orders: [],
            region: {
                latitude: 34.7155157,
                longitude: 134.8533269,
                latitudeDelta: 0.04864195044303443,
                longitudeDelta: 0.040142817690068,
            },
            coordinate: [],
            curRoute: 0,
            loaded: true,
            curCompanyId: 0,
            curIndex: 0
        };
    }
    UNSAFE_componentWillMount(){
        //this.index = 0;
        this.setState({index: 0})
    }
    componentDidMount(){

        this.setState({loaded: false});
        const userToken = store.getState().user.token;

        const mapId = this.props.id;
        let postBody = [];
        var encodedKey = encodeURIComponent('routeIds[]');
        var encodedValue = encodeURIComponent(mapId);
        postBody.push(encodedKey + "=" + encodedValue);
        postBody = postBody.join("&");

        GetRoutes(postBody, userToken)
        .then((response) => {
            this.setState({loaded: true});
            
            if(response.status && response.data.length > 0) {
                this.setState({orders: response.data})

                this.setState({curCompanyId: response.data[0]['companyId']})
                let temp = [];
                response.data.map((marker) => {
                    if(marker.companyMainLat && marker.companyMainLon)
                        temp.push({latitude: parseFloat(marker.companyMainLat), longitude: parseFloat(marker.companyMainLon)})
                })
                
                this.setState({coordinate: temp})

                let markersList = [];
                let indArr = [];
                let maxLatitude = 0, maxLongitude = 0, minLatitude = 10000, minLongitude = 10000;
                const dataList = response.data;
                let latlngList = [];
                for(let i = 0; i < dataList.length; i++) {
                    if(indArr.indexOf(dataList[i].companyId) < 0) {
                        indArr.push(dataList[i].companyId);
                        let newItem = {};
                        let image_link = '';
                        if(dataList[i].companyImages){
                            let temp = JSON.parse(dataList[i].companyImages)
                            if(temp.length > 0)
                                image_link = temp[0];
                        }
                        newItem = (dataList[i].companyMainLat && dataList[i].companyMainLon) ? 
                            {
                                latitude: parseFloat(dataList[i].companyMainLat),
                                longitude: parseFloat(dataList[i].companyMainLon),
                                title: dataList[i].companyTitle,
                                description: dataList[i].receiveAddress,
                                phone: dataList[i].phone,
                                image: image_link,
                                companyId: dataList[i].companyId, 
                                orderId: dataList[i].orderId,
                                status: dataList[i].orderStatus
                            }
                         : null;
                        
                        latlngList = [];
                        if(dataList[i].companyMainLat && dataList[i].companyMainLon) {
                            latlngList.push({
                                lat: dataList[i].companyMainLat,
                                lng: dataList[i].companyMainLon,
                            });
                        }
                        if(newItem != null){
                            markersList.push(newItem);
                        }
                        maxLatitude = dataList[i].companyMainLat && parseFloat(dataList[i].companyMainLat) > maxLatitude ? parseFloat(dataList[i].companyMainLat) : maxLatitude
                        minLatitude = dataList[i].companyMainLat && parseFloat(dataList[i].companyMainLat) < minLatitude ? parseFloat(dataList[i].companyMainLat) : minLatitude
                        maxLongitude = dataList[i].companyMainLon && parseFloat(dataList[i].companyMainLon) > maxLongitude ? parseFloat(dataList[i].companyMainLon) : maxLongitude
                        minLongitude = dataList[i].companyMainLon && parseFloat(dataList[i].companyMainLon) < minLongitude ? parseFloat(dataList[i].companyMainLon) : minLongitude
                    } 
                }

                let latitude = (maxLatitude + minLatitude) / 2;
                let longitude = (maxLongitude + minLongitude) / 2;
                let latitudeDelta = maxLatitude - minLatitude + 0.01;
                let longitudeDelta = maxLongitude - minLongitude + 0.01;

                
                this.setState({
                    markers: markersList,
                    region: {
                        ...this.state.region,
                        latitude,
                        longitude,
                        latitudeDelta,
                        longitudeDelta
                    }
                });
            }
            else {
                showToast(_e.InvalidToken);
            }
        });
    }
    changeOrderStatus(id, status){
        let token = store.getState().user.token;
        this.setState({loaded: false});
        changeOrderStatus(id, status, token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
            }
            else {
                showToast(response.msg);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        });
    }
    handleYup (card) {
        if(this.state.curIndex == 0){
            this.setState({loaded: false});
            const userToken = store.getState().user.token;
            changeRouteStatus(this.props.id[0], 2, userToken)
            .then((response) => {
                this.setState({loaded: true});
            })
            .catch((error) => {
                this.setState({loaded: true});
                showToast();
            });
        }
        this.setState({curIndex: this.state.curIndex + 1})
        if(this.state.curIndex+1 < this.state.orders.length && this.state.orders[this.state.curIndex+1].companyId != this.state.curCompanyId){
            this.setState({curCompanyId: card.companyId})
            this.setState({curRoute: this.state.curRoute + 1})
        }
        if(this.state.curIndex + 1 == this.state.orders.length){
            this.setState({loaded: false});
            const userToken = store.getState().user.token;
            changeRouteStatus(this.props.id[0], 3, userToken)
            .then((response) => {
                this.setState({loaded: true});
            })
            .catch((error) => {
                this.setState({loaded: true});
                showToast();
            });
        }
        
        this.changeOrderStatus(card.orderId, 5)
    }
    handleNope (card) {
        if(this.state.curIndex == 0){
            this.setState({loaded: false});
            const userToken = store.getState().user.token;
            changeRouteStatus(this.props.id[0], 2, userToken)
            .then((response) => {
                this.setState({loaded: true});
            })
            .catch((error) => {
                this.setState({loaded: true});
                showToast();
            });
        }
        this.setState({curIndex: this.state.curIndex + 1})
        if(this.state.curIndex+1 < this.state.orders.length && this.state.orders[this.state.curIndex+1].companyId != this.state.curCompanyId){
            this.setState({curCompanyId: card.companyId})
            this.setState({curRoute: this.state.curRoute + 1})
        }
        if(this.state.curIndex + 1 == this.state.orders.length){
            this.setState({loaded: false});
            const userToken = store.getState().user.token;
            changeRouteStatus(this.props.id[0], 3, userToken)
            .then((response) => {
                this.setState({loaded: true});
            })
            .catch((error) => {
                this.setState({loaded: true});
                showToast();
            });
        }
        
        this.changeOrderStatus(card.orderId, 7)
    }
    
    gotoDetail(){
        Actions.push("orderstatusscreen", {id : this.props.id})
    }
    renderCard(data){
        let image_link = '';
        if(data.companyImages){
            let temp = JSON.parse(data.companyImages)
            if(temp.length > 0)
                image_link = temp[0];
        }
        return (
            <View style={styles.scrollView}>
                <View style={[styles.card]}>
                    <View style={{width: '40%', paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center'}}>
                        <Image
                            source={{uri: Layout.serverLink + image_link}}
                            style={styles.cardImage}
                            resizeMode="stretch"
                        />
                        <Text>訪問</Text>
                    </View>
                    
                    <View style={styles.textContent}>
                        <View style={{flex: 1, borderBottomColor: '#707070', borderBottomWidth: 1}}>
                            <Text numberOfLines={1} style={styles.cardtitle}>{data.companyTitle}</Text>
                            <Text style={styles.cardDescription}>
                                {data.receiveAddress}
                            </Text>
                        </View>
                        <View style={{justifyContent: 'center', flex: 1, flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'}}>
                            <PlusButton
                                color="#3497FD"
                                width={34}
                                height={34}
                                size={20}
                                type="phone"
                                borderRadius={8}
                            />
                            <Text style={{marginLeft: 5}}>{data.phone}</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
    render() {
        return (
        <Container style={[shared.container , {backgroundColor: '#f2f2f2'}]}>
            <MainHeader title={this.props.routes_names}></MainHeader>
            {
                <MapView style={{width: '100%', height: Platform.OS == 'android' ? Layout.window.height - 240 : Layout.window.height - 260 - Constants.statusBarHeight}} 
                    region={this.state.region}
                    ref={map => this.map = map}
                >
                    <Polyline
                        coordinates={this.state.coordinate}
                        strokeColor="#000"
                        strokeWidth={3}
                    />

                    {this.state.markers.map((marker, index) => {
                        return (
                            this.state.curRoute == index ?
                            <MapView.Marker key={index} coordinate={{latitude: marker.latitude, longitude: marker.longitude}} centerOffset={Platform.OS == 'android' ? {x: 20, y: -20} : {x: 0, y: 0}} anchor={Platform.OS == 'android' ? {x: 0.5, y: 0.5} : {x: 0, y: 0}}>
                                <Image source={require('../assets/images/marker.png')} style={{height: 40, width: 40}} resizeMethod="resize" resizeMode="contain" />
                            </MapView.Marker>
                            :
                            <MapView.Marker key={index} coordinate={{latitude: marker.latitude, longitude: marker.longitude}} anchor={{x: 0.5, y: 0.5}}>
                                <View style={[styles.markerWrap]}>
                                    <View style={styles.marker} >
                                        <Text style={{color: 'white'}}>{index+1}</Text>
                                    </View>
                                </View>
                            </MapView.Marker>
                        );
                    })}
                </MapView>
            }
            {
                this.state.orders.length > 0 ?
                <SwipeCards
                    cards={this.state.orders}
                    loop={false}
                    renderCard={(cardData) => this.renderCard(cardData)}
                    renderNoMoreCards={() => <NoMoreCards />}
                    handleYup={this.handleYup.bind(this)}
                    handleNope={this.handleNope.bind(this)}
                    nopeText="保留"
                    yupText="渡し済み"
                    showMaybe={false}
                    hasMaybeAction
                    onClickHandler={() => console.log('click card')}
                />
                :
                null
            }
            <TouchableOpacity onPress={() => this.gotoDetail()} style={styles.listSection}>
                <AntDesign name={"car"} color={"#707070"} size={20}/>
                <Text style={{color: '#747474'}}>状況確認</Text>
            </TouchableOpacity>
            <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
        </Container>
        );
    }
}

class NoMoreCards extends React.Component {
    constructor(props) {
      super(props);
    }
   
    render() {
      return (
            <View style={styles.scrollView}>
                <TouchableOpacity style={[styles.card, {padding: 20, flexDirection: 'column', justifyContent: 'space-between'}]} onPress={() => Actions.reset("root")}>
                    <Text>全て完了しました。</Text>
                    <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                        <SimpleLineIcons
                            name='home'
                            size={ 30}
                            color='black'
                        />
                        <Text style={{marginLeft: 15}}>ホームヘ</Text>
                    </View>
                    
                </TouchableOpacity>
            </View>
      )
    }
  }


const styles = StyleSheet.create({
    scrollView: {
        paddingVertical: 10,
        //backgroundColor: 'rgba(112,112, 112, 0.3)',
        justifyContent: 'center',
        alignItems: 'center',
    },
    endPadding: {
        paddingRight: width - CARD_WIDTH,
    },
    card: {
        padding: 10,
        elevation: 2,
        backgroundColor: "#FFF",
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: "hidden",
        flexDirection: 'row'
    },
    cardImage: {
        flex: 3,
        width: "100%",
        height: "100%",
        alignSelf: "center",
        marginBottom: 10
    },
    textContent: {
        flex: 1,
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    cardtitle: {
        fontSize: 12,
        marginTop: 5,
        fontWeight: "bold",
    },
    cardDescription: {
        fontSize: 12,
        color: "#444",
    },
    markerWrap: {
        alignItems: "center",
        justifyContent: "center",
    },
    marker: {
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: "#5eba56",
        justifyContent: 'center',
        alignItems: 'center'
    },
    ring: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: "rgba(130,4,150, 0.3)",
        position: "absolute",
        borderWidth: 1,
        borderColor: "rgba(130,4,150, 0.5)",
    },
    listSection: {
        position: 'absolute', 
        right: 0, 
        top: Platform.OS == 'android' ? 80 : 80 + Constants.statusBarHeight, 
        backgroundColor: 'white', 
        padding: 10, 
        justifyContent: 'center', 
        alignItems: 'center', 
        borderTopLeftRadius: 20, 
        borderBottomLeftRadius: 20
    }
});
