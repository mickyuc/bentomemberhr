import React from 'react';
import { StyleSheet, TouchableWithoutFeedback, Keyboard, Image, View, TouchableOpacity } from 'react-native';
import { Container, Content, Item, Input, Text } from 'native-base';
import { normalize, fonts, margin, form} from './../assets/styles';
import { sendPhoneConfirmCode, loginPhone } from './../api';
import { showToast } from './../shared/global';
import { _e } from './../lang';
import { connect } from "react-redux";
import { setUser } from './../actions';
import {Actions} from 'react-native-router-flux';
import Spinner_bar from 'react-native-loading-spinner-overlay';
TouchableOpacity.defaultProps = { activeOpacity: 0.8 };

class PhoneLogin extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            phone: '',
            code: '',
            phoneError: false,
            codeError: false,
            loaded: true
        };
    }
    componentDidMount(){
    }
    loginUser(){
        if(this.state.phone == ''){
            this.setState({phoneError: true})
        }else{
            this.setState({phoneError: false})
        }
        if(this.state.code == ''){
            this.setState({codeError: true})
        }else{
            this.setState({codeError: false})
        }
        if(this.state.phone != '' && this.state.code != ''){
            this.setState({phoneError: false})
            this.setState({codeError: false})
            this.setState({loaded: false});
            loginPhone(this.state.phone, this.state.code)
            .then((response) => {
                this.setState({loaded: true});
                if(response.status) {
                    this.props.setUser({
                        username: response.data.username,
                        token: response.data.token
                    });
                    Actions.reset("root")
                }
                else {
                    showToast(response.msg);
                }
            })
            .catch((error) => {
                this.setState({loaded: true});
                showToast();
            })
        }
    }

    sendCode(){
        if(this.state.phone == ''){
            this.setState({phoneError: true})
        }
        if(this.state.phone != ''){
            this.setState({phoneError: false})
            this.setState({loaded: false});
            sendPhoneConfirmCode(this.state.phone)
            .then((response) => {
                this.setState({loaded: true});
                if(response.status) {
                    showToast(response.msg, "success")
                }
                else {
                    showToast(_e.sendCodeFailed);
                }
            })
            .catch((error) => {
                this.setState({loaded: true});
                showToast();
            })
        }
    }
    render(){
        return (
            <Container>
                <Content contentContainerStyle={[styles.contentBg , styles.contentPD, {
                    flexGrow: 1,
                    flexDirection: 'column',
                    justifyContent: 'center'
                }]}>
                    <View style={{paddingBottom: 20}}>
                        <Item rounded style={ [form.item , {position: 'relative'}] }>
                            <Input
                                placeholder = "電話番号入力"
                                value = { this.state.username }
                                style = { [form.input] }
                                keyboardType="phone-pad"
                                onChangeText = {(text) => this.setState({phone: text})}
                                placeholderTextColor = '#9da8bf'
                            />
                        </Item>
                        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                            <Item rounded style={ [form.item , {position: 'relative', width: '48%'}] }>
                                <Input
                                    placeholder = "コード入力"
                                    value = { this.state.password }
                                    style = { [form.input, {width: '50%'}] }
                                    onChangeText = {(text) => this.setState({code : text})}
                                    placeholderTextColor = '#9da8bf'
                                />
                            </Item>
                            <TouchableOpacity onPress={() => this.sendCode()} style={{borderRadius: 12, width: '50%',backgroundColor:'#3497fd', height: normalize(52) }}>
                                <Text style={[styles.btnText , fonts.size15]}>認定コード受信 </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 8}}>
                            <TouchableOpacity onPress={() => this.loginUser()} style={{borderRadius: 12, width: '100%',backgroundColor:'#353a50' }}>
                                <Text style={[styles.btnText , fonts.size15]}>ログイン </Text>
                            </TouchableOpacity>
                        </View>
                        <TouchableWithoutFeedback onPress={() => { Keyboard.dismiss(); }}>
                            <View style={[margin.mt6 , { flexDirection:'row' , justifyContent:'flex-end' }]}>
                                <Text onPress={() => { this.props.navigation.navigate("ForgotPwd") }} style={[fonts.size14, {color: 'white'}]}>
                                    パスワードを忘れた方
                                </Text>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </Content>
                <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
            </Container>
        );
    }
}
const mapDispatchToProps = dispatch => {
    return {
        setUser : user => { dispatch(setUser(user)) }
    }
}

export default connect(null,mapDispatchToProps)(PhoneLogin)

const styles = StyleSheet.create({
    contentBg: {
        backgroundColor: '#3acce1'
    },
    logoImage: {
        width: normalize(81.6),
        height: normalize(81)
    },
    logoText1: {
        color: 'white'
    },
    logoBgImage: {
        width: normalize(282.8),
        height: normalize(190.2)
    },
    contentPD: {
        paddingLeft: normalize(26),
        paddingRight: normalize(22)
    },
    btnText: {
        padding: 18,
        width: "100%",
        color: '#fff',
        textAlign: 'center',
    }
});
