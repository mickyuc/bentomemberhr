import React from 'react';
import { View, Text, StyleSheet} from 'react-native';
import MainHeader from '../components/MainHeader';
import { shared, fonts, margin, normalize, getScreenWidth } from '../assets/styles';
import { Container, Content } from 'native-base';
import { Feather} from '@expo/vector-icons';
import { CalendarList, LocaleConfig, Calendar } from 'react-native-calendars';
import moment from 'moment';
import { getListByMonth } from './../api';
import { showToast } from './../shared/global';
import store from './../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
LocaleConfig.locales['zh-Hans'] = {
    monthNames: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
    monthNamesShort: ['1月', '2月', '3月', '4月', '5月', '6月', '7月', '8月', '9月', '10月', '11月', '12月'],
    dayNames: ['星期天', '星期一', '星期二', '星期三', '星期四', '星期五', '星期六'],
    dayNamesShort: ['日', '月', '火', '水', '木', '金', '土'],
    amDesignator: '上午',
    pmDesignator: '下午',
};

LocaleConfig.defaultLocale = 'zh-Hans';
let pageTitle = 'アフターケア'
const workout = {key:'workout', color: '#3497FD'};
export default class AfterCare extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        type: true,
        currentDay: moment().format(),
        markedDates: {},
        orders: [],
        selMonth: moment().month(),
        selYear: moment().year(),
        loaded: true
    };
  }
  UNSAFE_componentWillReceiveProps(){
      this.refresh()
  }
  componentDidMount(){
      this.refresh();   
  }
  refresh(){
      let token = store.getState().user.token;
      this.setState({loaded: false});
      getListByMonth(moment().format("YYYY-MM"), token)
      .then((response) => {
          this.setState({loaded: true});
          if(response.status) {
              this.setState({orders: response.data})
              this.getDataPattern(response.data, this.state.selMonth, this.state.selYear)
          }
          else {
              showToast(response.msg);
          }
      })
      .catch((error) => {
          this.setState({loaded: true});
          showToast();
      })
  }
  getDataPattern(data, month, year){
      
      let dates = {}
      let pivot = moment().month(month).year(year).startOf('month')
      const end = moment().month(month).year(year).endOf('month')
      const existPattern = {dots: [workout]}
      while(pivot.isBefore(end)) {
          data.forEach((day) => {
              if(moment(day.regDate).format("YYYY-MM-DD") == pivot.day(day).format("YYYY-MM-DD"))
                  dates[pivot.day(day).format("YYYY-MM-DD")] = existPattern
          })
          pivot.add(1, 'days')
      }
      this.setState({markedDates: dates})
  }

  selMonth(date){
      let selMonth = date[0].month-1
      let selYear = date[0].year;
      this.getDataPattern(this.state.orders, selMonth, selYear)
  }

  getDaysInMonth(day){
      Actions.push("aftercaredetail", {day: day})
  }
  render() {
    return (
      <Container style={[shared.container , {backgroundColor: '#f2f2f2'}]}>
          <MainHeader title={pageTitle}></MainHeader>
          <View style={{justifyContent: 'center', width: '100%', marginBottom: normalize(92)}}>
              <View style={{width: getScreenWidth()-34, marginHorizontal: 17}}>
              <CalendarList
                  current={this.state.currentDay}
                  minDate={this.state.currentDay}
                  monthFormat="yyyy年 MM"
                  horizontal
                  pastScrollRange={0}
                  pagingEnabled
                  monthFormat="yyyy年 MMMM"
                  onDayPress={day => {
                      this.getDaysInMonth(day.dateString)
                  }}
                  markedDates={this.state.markedDates}
                  onVisibleMonthsChange={(date) => {
                      this.selMonth(date)
                  }}
                  hideArrows={false}
                  disableMonthChange={false}
                  scrollEnabled={false}
                  markingType={"multi-dot"}
                  theme={{
                      selectedDayBackgroundColor: '#2E66E7',
                      selectedDayTextColor: '#ffffff',
                      todayTextColor: '#2E66E7',
                      backgroundColor: '#eaeef7',
                      calendarBackground: 'white',
                      textDisabledColor: '#d9dbe0',
                      arrowColor: '#3497fd',
                      textSectionTitleColor: 'black',
                      textDayFontSize: 16,
                      textMonthFontSize: 16,
                      textDayHeaderFontSize: 16,
                      'stylesheet.day.multiDot': {
                        dot: {
                          width: 10,
                          height: 10,
                          borderRadius: 5
                        }
                      }
                  }}
                  style={{marginTop: 15}}
                  calendarWidth={getScreenWidth()-34}
              />
              </View>
          </View>
          {
            /*<View style={styles.plus}>
                <Feather name={"plus"} color={"white"} size={40} />
            </View>*/
          }
          
          <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
        </Container>
      );
    }
  }

const styles = StyleSheet.create({
    closeIcon: {
        right: normalize(20),
        top: normalize(20),
        position: 'absolute'
    },
    collapseIcon: {
        right: normalize(7),
        bottom: normalize(7),
        position: 'absolute'
    },
    plus: {
        width: normalize(60),
        height: normalize(60),
        position: 'absolute',
        backgroundColor: '#82a3ea',
        borderRadius: normalize(30),
        bottom: 10,
        right: normalize(32),
        bottom: normalize(32),
        justifyContent: 'center',
        alignItems: 'center'
    }
});
