import React from 'react';
import { StyleSheet, View, TouchableOpacity} from 'react-native';
import MainHeader from '../components/MainHeader';
import { Container, Content, Item, Input, Text } from 'native-base';
import { shared, fonts, margin, normalize, form } from '../assets/styles';
import { ScrollView,  } from 'react-native-gesture-handler';

import { changeCompanyPhone } from '../api';
import { showToast } from '../shared/global';
import store from '../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
import { _e } from '../lang';

let pageTitle = '企業情報変更'

export default class EditCompanyPhone extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loaded: true,
            phone: this.props.phone
        }
    }
    componentDidMount(){
        
    }
    updateData(){
        let token = store.getState().user.token;
        this.setState({loaded: false});
        changeCompanyPhone(this.props.companyId, this.state.phone, token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                Actions.pop({refresh: {}})
                Actions.refresh();
            }
            else {
                showToast(response.msg);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        })
    }
    
    render() {
        
        return (
            <Container  style={shared.container}>
                <MainHeader title={pageTitle}></MainHeader>
                <Content contentContainerStyle={{ flex:1 , display: 'flex' , paddingTop: 10}}>
                    <ScrollView style={{padding: 0 , margin: 0}}>
                        <View style={[margin.px5, margin.mt4, margin.pb5]}>
                            <Item rounded style={ [form.item , {position: 'relative', borderWidth: 1}] }>
                                <Input
                                    value = { this.state.phone }
                                    keyboardType="phone-pad"
                                    style = { [form.input, {borderBottomWidth: 1, borderBottomColor: '#707070'}] }
                                    onChangeText = {(text) => this.setState({phone: text})}
                                />
                            </Item>
                        </View>
                        <View style={{paddingHorizontal: normalize(24), alignItems: 'center', justifyContent: 'center'}}>
                            <TouchableOpacity onPress={() => this.updateData()} style={styles.save}>
                                <Text style={[{color: 'white' }, fonts.size20]}>確定</Text>
                            </TouchableOpacity>
                        </View>
                    </ScrollView>
                    <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
                </Content>
            </Container>
        );
    }
}

EditCompanyPhone.navigationOptions = {
    header: null
}
const styles = StyleSheet.create({
    save: {
        backgroundColor: '#41cce1',
        borderRadius: normalize(12),
        zIndex: 99999,
        width: '100%',
        paddingVertical: normalize(14),
        alignItems: 'center'
    }
});