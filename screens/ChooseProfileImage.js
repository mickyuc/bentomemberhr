import React from 'react';
import { StyleSheet, Text, View,  Image, Dimensions, TouchableOpacity, FlatList} from 'react-native';
import MainHeader from '../components/MainHeader';
import { shared, normalize, fonts, getScreenWidth } from '../assets/styles';
import { Container, Content } from 'native-base';
import { Feather } from '@expo/vector-icons';
import { getImage, changeCompanyHomeImage } from '../api';
import { showToast } from '../shared/global';
import store from '../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
import { _e } from '../lang';
import Layout from '../constants/Layout';
let pageTitle = 'プロフィール画像'
export default class ChooseProfileImage extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            uri: '',
            images: [
            ],
            loaded: true,
            homeImage: this.props.homeImage
        };
    }

    refresh(){
        let token = store.getState().user.token;
        this.setState({loaded: false});
        getImage(this.props.companyId, token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                this.setState({images: response.data})
            }
            else {
                showToast(response.msg);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        })
    }

    componentDidMount(){
        this.refresh();
    }

    updateData(){
        let token = store.getState().user.token;
        this.setState({loaded: false});
        changeCompanyHomeImage(this.props.companyId, this.state.homeImage, token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                Actions.popTo("businessscreen",{refresh: {}})
                Actions.refresh();
            }
            else {
                showToast(response.msg);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        })
    }

    chooseImage(image_link){
        this.setState({homeImage : image_link})
    }

    render() {
        return (
        <Container style={[shared.container , {backgroundColor: '#f2f2f2'}]}>
            <MainHeader title={pageTitle}></MainHeader>
            <Content>
                <View style={{height: 200, margin: 15}}>
                    {
                        this.state.homeImage != '' && this.state.homeImage != null ?
                        <Image source={{uri: Layout.serverLink + this.state.homeImage}} style={{height: 200}} resizeMode="contain"/>
                        :
                        <View style={{justifyContent: 'center', alignItems: 'center', height: 200}}>
                            <Text style={{fontSize: 18}}>登録されたプロフィール画像がありません。</Text>
                        </View>
                    }
                </View>

                <View style={{paddingHorizontal: normalize(24), alignItems: 'center', justifyContent: 'center'}}>
                    <TouchableOpacity onPress={() => this.updateData()} style={styles.save}>
                        <Text style={[{color: 'white' }, fonts.size20]}>確定</Text>
                    </TouchableOpacity>
                </View>

                <View style={{paddingHorizontal: normalize(18)}}>
                    <FlatList
                        data={this.state.images}
                        renderItem={({ item }) => (
                            <TouchableOpacity onPress={() => this.chooseImage(item)} style={{ width: (getScreenWidth()-72)/3, flexDirection: 'column', margin: 6}}>
                                <Image style={styles.imageThumbnail} source={{ uri: Layout.serverLink + item }} resizeMode={"stretch"} resizeMethod={"resize"}
                                />
                            </TouchableOpacity>
                        )}
                        numColumns={3}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
                <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
            </Content>
        </Container>
        );
    }
}

const styles = StyleSheet.create({
    getImage: {
        width: normalize(92),
        height: normalize(92),
        backgroundColor: '#bababa',
        borderRadius: normalize(46),
        justifyContent: 'center',
        alignItems: 'center',
        margin: normalize(20)
    },
    imageThumbnail: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
    },
    save: {
        backgroundColor: '#41cce1',
        borderRadius: normalize(12),
        zIndex: 99999,
        width: '100%',
        paddingVertical: normalize(14),
        alignItems: 'center'
    }
});
