import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity} from 'react-native';
import MainHeader from '../components/MainHeader';
import ActionButton from '../components/ActionButton';
import { CheckBox } from 'react-native-elements';
import { shared, fonts, margin, normalize, form } from '../assets/styles';
import { Container, Content, Input } from 'native-base';
import { ScrollView } from 'react-native-gesture-handler';
import { createAfter, updateAfter, checkedAfter, getBusinessType } from '../api';
import { showToast } from '../shared/global';
import store from '../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
import { _e } from '../lang';
import RNPickerSelect from 'react-native-picker-select';
import Layout from '../constants/Layout';
let pageTitle = 'アフターケア'
export default class AddAfterDetail extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      isSelected: false,
      loaded: true,
      method: '',
      detail: '',
      bussinesType: [],
      business: ''
    };
  }
  componentDidMount(){
      let token = store.getState().user.token;
      if(this.props.detail != undefined && this.props.detail != null)
        this.setState({detail: this.props.detail})
      if(this.props.method != undefined && this.props.method != null)
        this.setState({method: this.props.method})
      if(this.props.id != undefined && this.props.id != null){
        checkedAfter(this.props.id, token)
        .then((response) => {
        })
        .catch((error) => {
            showToast();
        })
      }
      getBusinessType()
      .then((response) => {
          if(response.status) {
              let temp = []
              for(var i = 0;i<response.data.length;i++){
                  temp.push({label: response.data[i]['value'], value: response.data[i]['id']})
              }
              this.setState({bussinesType: temp})
          }
          else
              showToast(response.msg);
      })
  }
  
  addAfter(){
    let token = store.getState().user.token;
    this.setState({loaded: false});
    if(this.props.id != undefined && this.props.id != null){
        updateAfter(this.props.id, this.props.day, this.state.method, this.state.detail, this.state.business, token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                Actions.popTo("aftercare", {refresh: {}})
                Actions.refresh();
            }
            else {
                showToast(response.msg);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        })
    }else{
        createAfter(this.props.day, this.props.companyId, this.state.method, this.state.detail, 1, token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                Actions.popTo("aftercare", {refresh: {}})
                Actions.refresh();
            }
            else {
                showToast(response.msg);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        })
    }
  }

  render() {
    const placeholder = {
        label: '選択してください',
        value: null,
        color: '#9EA0A4',
    };
    return (
      <Container  style={shared.container}>
        <MainHeader title={pageTitle}></MainHeader>
        <Content contentContainerStyle={{ flex:1 , display: 'flex' , paddingTop: 10}}>
          <ScrollView style={{padding: 0 , margin: 0}}>
            <View style={margin.mt4, margin.pb5}>
                {
                    /*<View style={[{backgroundColor: '#78849E', paddingVertical: 12, width: '70%'}, margin.pl15]}>
                        <Text style={[fonts.size18, {color: 'white'}]}>{this.props.state} / {this.props.city}</Text>
                    </View>*/
                }
              
              <View style={styles.section}>
                  <Text>{this.props.day}</Text>
              </View>
              <View style={styles.section}>
                  <Text>{this.props.company}</Text>
              </View>
              
                <View style={[styles.section, {borderBottomWidth: 0}]}>
                    <RNPickerSelect
                        placeholder={placeholder}
                        items={Layout.method}
                        onValueChange={value => { this.setState({method: value}) }}
                        style={ pickerSelectStyles }
                        value={this.state.method}
                        useNativeAndroidPickerStyle={false}
                    />
              </View>
              
              {
                /*<View style={[styles.section, {borderBottomWidth: 0}]}>
                    <RNPickerSelect
                        placeholder={placeholder}
                        items={this.state.bussinesType}
                        onValueChange={value => { this.setState({business: value}) }}
                        style={ pickerSelectStyles }
                        value={this.state.business}
                        useNativeAndroidPickerStyle={false}
                    />
              </View>*/
              }
              
              <View style={[styles.section, {borderBottomWidth: 0, paddingTop: 0}]}>
                    <Input
                        placeholder = ""
                        value = { this.state.detail }
                        style = { [form.input, {borderBottomWidth: 1, borderBottomColor: '#C3C9D6'}] }
                        onChangeText = {(text) => this.setState({detail: text})}
                    />
              </View>
              <View style={{ width: '100%', backgroundColor: '#3497FD'}}>
                <TouchableOpacity onPress={() => this.addAfter()} style={{justifyContent: 'center', alignItems: 'center', width: '100%', paddingVertical: 20}}>
                  {
                    this.props.id > 0 ?
                    <Text style={[fonts.size15, {color: 'white'}]}>追加</Text>
                    :
                    <Text style={[fonts.size15, {color: 'white'}]}>更新</Text>
                  }
                    
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        </Content>
        
        <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
      </Container>
    );
  }
}

AddAfterDetail.navigationOptions = {
  header: null
}

const styles = StyleSheet.create({
    location_info: {
      marginLeft: 50
    },
    location_bottom: {
      borderBottomWidth: 1,
      borderColor: '#C3C9D6'
    },
    location_d: {
      paddingVertical: 6,
    },
    section:  {
        borderBottomColor: '#C3C9D6', borderBottomWidth: 1, 
        paddingVertical: 20, 
        marginHorizontal: 40
    }
});
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
      fontSize: normalize(15),
      paddingVertical: normalize(15),
      paddingHorizontal: normalize(24),
      borderWidth: 1,
      borderColor: '#c3c3c3',
      borderRadius: 8,
      color: '#78849E',
      paddingRight: normalize(30), // to ensure the text is never behind the icon
    },
    inputAndroid: {
      fontSize: normalize(15),
      
      paddingVertical: normalize(12),
      borderBottomWidth: 1,
      borderColor: '#c3c3c3',
      borderRadius: 8,
      color: '#78849E',
      paddingRight: 30, // to ensure the text is never behind the icon
    },
});