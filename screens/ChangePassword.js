import React from 'react';
import { StyleSheet, View, TouchableOpacity, KeyboardAvoidingView} from 'react-native';
import MainHeader from '../components/MainHeader';
import { Container, Content, Item, Input, Text } from 'native-base';
import { shared, fonts, margin, normalize, form } from '../assets/styles';
import { ScrollView,  } from 'react-native-gesture-handler';

import { changeCompanyPhone, changePassword } from '../api';
import { showToast } from '../shared/global';
import store from '../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
import { _e } from '../lang';

let pageTitle = 'パスワードを変更'

export default class ChangePassword extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            loaded: true,
            currentPassword: '',
            newPassword: '',
            confirmPassword: '',
            currendPwdErr: false,
            newPwdErr: false,
            confirmPwdErr: false
        }
    }
    
    updatePassword(){
        let valid = true
        if(this.state.currentPassword == ''){
            this.setState({currendPwdErr: true})
            valid = false
        }else{
            this.setState({currendPwdErr: false})
        }
        if(this.state.newPassword == '' || this.state.newPassword != this.state.confirmPassword){
            this.setState({newPwdErr: true})
            valid = false
        }else{
            this.setState({newPwdErr: false})
        }
        if(valid){
            this.setState({loaded: false});
            let token = store.getState().user.token;
            changePassword(this.state.currentPassword, this.state.newPassword, token)
            .then((response) => {
                this.setState({loaded: true});
                if(response.status) {
                    Actions.pop()
                }
                else {
                    showToast(response.msg);
                    this.setState({currendPwdErr: true})
                }
            })
            .catch((error) => {
                this.setState({loaded: true});
                showToast();
            })
        }
        
    }
    
    render() {
        
        return (
            <Container  style={shared.container}>
                <MainHeader title={pageTitle}></MainHeader>
                <Content contentContainerStyle={{ flex:1 , display: 'flex' , paddingTop: 10}}>
                    <ScrollView style={{padding: 0 , margin: 0}}>
                        <KeyboardAvoidingView behavior="padding" >
                            <View style={[margin.px5, margin.mt2, margin.pb2]}>
                                
                                    <Input
                                        placeholder="以前のパスワード"
                                        value = { this.state.currentPassword }
                                        secureTextEntry={true}
                                        returnKeyType="go"
                                        style = { this.state.currendPwdErr ? [form.input, {borderBottomWidth: 1, borderBottomColor: 'red', marginHorizontal: 20}]: [form.input, {borderBottomWidth: 1, borderBottomColor: '#707070', marginHorizontal: 20}] }
                                        onChangeText = {(text) => this.setState({currentPassword: text})}
                                        
                                    />
                                
                            </View>
                            <View style={[margin.px5, margin.mt2, margin.pb2]}>
                                
                                    <Input
                                        placeholder="新しいパスワード"
                                        value = { this.state.newPassword }
                                        secureTextEntry={true}
                                        returnKeyType="go"
                                        style = { this.state.newPwdErr ? [form.input, {borderBottomWidth: 1, borderBottomColor: 'red', marginHorizontal: 20}] : [form.input, {borderBottomWidth: 1, borderBottomColor: '#707070', marginHorizontal: 20}] }
                                        ref={ref => {this.newPassword = ref;}}
                                        onChangeText = {(text) => this.setState({newPassword: text})}
                                        
                                    />
                                
                            </View>
                            <View style={[margin.px5, margin.mt2, margin.pb2]}>
                                
                                    <Input
                                        placeholder="パスワードを認証"
                                        value = { this.state.confirmPassword }
                                        secureTextEntry={true}
                                        returnKeyType="go"
                                        style = { this.state.newPwdErr ? [form.input, {borderBottomWidth: 1, borderBottomColor: 'red', marginHorizontal: 20}] : [form.input, {borderBottomWidth: 1, borderBottomColor: '#707070', marginHorizontal: 20}] }
                                        ref={ref => {this.confirmPassword = ref;}}
                                        onChangeText = {(text) => this.setState({confirmPassword: text})}
                                        
                                    />
                                
                            </View>
                            <View style={{paddingHorizontal: normalize(24), alignItems: 'center', justifyContent: 'center'}}>
                                <TouchableOpacity onPress={() => this.updatePassword()} style={styles.save}>
                                    <Text style={[{color: 'white' }, fonts.size20]}>保存</Text>
                                </TouchableOpacity>
                            </View>
                        </KeyboardAvoidingView>
                    </ScrollView>
                    <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
                </Content>
            </Container>
        );
    }
}

ChangePassword.navigationOptions = {
    header: null
}
const styles = StyleSheet.create({
    save: {
        backgroundColor: '#41cce1',
        borderRadius: normalize(12),
        zIndex: 99999,
        width: '100%',
        paddingVertical: normalize(14),
        alignItems: 'center'
    }
});