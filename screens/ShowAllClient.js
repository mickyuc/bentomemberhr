import React from 'react';
import { StyleSheet, Text, View,  Animated, Image, Dimensions,} from 'react-native';
import MainHeader from '../components/MainHeader';
import { shared } from '../assets/styles';
import { Container, Content } from 'native-base';
import MapView, { Polyline } from 'react-native-maps';
import { getListByDate } from './../api';
import { showToast } from './../shared/global';
import store from './../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';

let pageTitle = '上野松が谷ルート'
const { width, height } = Dimensions.get("window");
const CARD_HEIGHT = height / 5;
const CARD_WIDTH = width - 80;
export default class ShowAllClient extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            index: 0,
            markers: [
                
            ],
            region: {
                latitude: this.props.latitude,
                longitude: this.props.longitude,
                latitudeDelta: 0.04864195044303443,
                longitudeDelta: 0.040142817690068,
            },
            coordinate: [],
            loaded: true
        };
    }
    UNSAFE_componentWillMount(){
        this.setState({index: 0})
    }
    componentDidMount(){
        let token = store.getState().user.token;
        this.setState({loaded: false});
        getListByDate(this.props.day, token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                this.setState({markers : response.data})
            }
            else {
                showToast(response.msg);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        })
    }
    render() {
        return (
        <Container style={[shared.container , {backgroundColor: '#f2f2f2'}]}>
            <MainHeader title={pageTitle}></MainHeader>
            <MapView style={{width: '100%', height: '100%'}} 
                initialRegion={this.state.region}
            >
                {this.state.markers.map((marker, index) => {
                    return (
                        marker.mainLat != undefined && marker.mainLon != undefined && marker.mainLat != null && marker.mainLon != null && marker.mainLat != '' && marker.mainLon != '' ?
                        <MapView.Marker key={index} coordinate={{latitude: parseFloat(marker.mainLat), longitude: parseFloat(marker.mainLon)}}>
                            <View style={styles.marker} >
                            </View>
                        </MapView.Marker>
                        :
                        null
                    );
                })}
            </MapView>
            <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
        </Container>
        );
    }
}

const styles = StyleSheet.create({
    scrollView: {
        position: "absolute",
        bottom: 20,
        left: 0,
        right: 0,
        paddingVertical: 10,
        backgroundColor: 'rgba(112,112, 112, 0.3)',
    },
    endPadding: {
        paddingRight: width - CARD_WIDTH,
    },
    card: {
        padding: 10,
        elevation: 2,
        backgroundColor: "#FFF",
        marginHorizontal: 10,
        shadowColor: "#000",
        shadowRadius: 5,
        shadowOpacity: 0.3,
        shadowOffset: { x: 2, y: -2 },
        height: CARD_HEIGHT,
        width: CARD_WIDTH,
        overflow: "hidden",
        flexDirection: 'row'
    },
    cardImage: {
        flex: 3,
        width: "100%",
        height: "100%",
        alignSelf: "center",
        marginBottom: 10
    },
    textContent: {
        flex: 1,
        justifyContent: 'space-between',
        paddingHorizontal: 10
    },
    cardtitle: {
        fontSize: 12,
        marginTop: 5,
        fontWeight: "bold",
    },
    cardDescription: {
        fontSize: 12,
        color: "#444",
    },
    markerWrap: {
        alignItems: "center",
        justifyContent: "center",
    },
    marker: {
        width: 20,
        height: 20,
        borderRadius: 10,
        backgroundColor: "#5eba56",
        justifyContent: 'center',
        alignItems: 'center'
    },
    ring: {
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: "rgba(130,4,150, 0.3)",
        position: "absolute",
        borderWidth: 1,
        borderColor: "rgba(130,4,150, 0.5)",
    },
});
