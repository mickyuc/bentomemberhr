import React from 'react';
import { StyleSheet, View, Text, Image, TouchableOpacity} from 'react-native';
import MainHeader from '../components/MainHeader';
import BusinessInfoItem from '../components/BusinessInfoItem';
import { shared, fonts, margin, normalize } from '../assets/styles';
import { Container, Content } from 'native-base';
import { ScrollView,  } from 'react-native-gesture-handler';
import Images from "./../assets/Images";
import { getCompanyDetail, getCompanyGroups, changeCompanyGroup, changeCompanyPredict } from '../api';
import { showToast } from '../shared/global';
import store from '../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
import { _e } from '../lang';
import RNPickerSelect from 'react-native-picker-select';
import MapIcon from './../assets/images/map.svg';

import { Feather, FontAwesome5 } from '@expo/vector-icons';

let pageTitle = '企業情報変更'
var self = null;
const predict = [
    { value: '', label: ''},
    { value: 1, label: '高'},
    { value: 2, label: '中'},
    { value: 3, label: '低'},
]
export default class BusinessEditScreen extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            country: 'uk',
            isVisibleA: false,
            isVisibleB: false,
            loaded: true,
            companyInfo: null,
            groups: null,
            homeImage: ''
        }
    }
    componentDidMount(){
        self = this;
        this.refresh();
    }

    UNSAFE_componentWillReceiveProps(){
        this.refresh();
    }

    refresh(){
        let token = store.getState().user.token;
        this.setState({loaded: false});
        getCompanyGroups(token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                let temp = [];
                for(var i = 0;i<response.data.length;i++){
                    temp.push({label: response.data[i]['value'], value: response.data[i]['id']})
                }
                this.setState({groups: temp})
            }
            else {
                showToast(response.msg);
            }
        })

        getCompanyDetail(this.props.companyId, token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                this.setState({homeImage: response.data[0]['homeImage']})
                this.setState({companyInfo: response.data[0]})
            }
            else {
                showToast(response.msg);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        })
    }
    openClose(evt) {
        if(evt == 1) {
            self.setState({
                isVisibleA: true,
                isVisibleB: false
            });
            self.refs.dropA.setVisibleState();
            self.refs.dropB.setUnVisibleState();
        }
        else {
            self.setState({
                isVisibleA: false,
                isVisibleB: true
            });
            self.refs.dropB.setVisibleState();
            self.refs.dropA.setUnVisibleState();
        }
    }
    changeVal(val, key){
        let temp = this.state.companyInfo
        if(key == 'group'){
            temp['groupId'] = val;
            let token = store.getState().user.token;
            this.setState({loaded: false});
            changeCompanyGroup(this.props.companyId, val, token)
            .then((response) => {
                this.setState({loaded: true});
                if(response.status) {
                  this.refresh();
                }
                else {
                    showToast(response.msg);
                }
            })
            .catch((error) => {
                this.setState({loaded: true});
                showToast();
            })
        }else if(key == 'predict'){
            temp['predictTypeId'] = val;
            let token = store.getState().user.token;
            this.setState({loaded: false});
            changeCompanyPredict(this.props.companyId, val, token)
            .then((response) => {
                this.setState({loaded: true});
                if(response.status) {
                  this.refresh();
                }
                else {
                    showToast(response.msg);
                }
            })
            .catch((error) => {
                this.setState({loaded: true});
                showToast();
            })
        }
        this.setState({companyInfo: temp})
    }
    render() {
        const placeholder = {
            label: '選択してください',
            value: null,
            color: '#9EA0A4',
        };
        return (
            <Container  style={shared.container}>
                <MainHeader title={pageTitle}></MainHeader>
                <Content contentContainerStyle={{ flex:1 , display: 'flex' , paddingTop: 10}}>
                    <ScrollView style={{padding: 0 , margin: 0}}>
                        {
                            this.state.companyInfo != null  ?
                            <View style={[margin.px5, margin.mt4, margin.pb5]}>
                                <TouchableOpacity onPress={() => Actions.push("editcompanytitle", {companyTitle: this.state.companyInfo.companyTitle, companyId: this.props.companyId})}>
                                    <BusinessInfoItem
                                        item="名称"
                                        value={this.state.companyInfo.companyTitle}
                                        type="text"
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Actions.push("editcompanyreceiveaddress", {receiveAddress: this.state.companyInfo.receiveAddress, companyId: this.props.companyId})}>
                                    <BusinessInfoItem
                                        item="請求書送付先"
                                        value={this.state.companyInfo.receiveAddress}
                                        type="text"
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Actions.push("editcompanyphone", {phone: this.state.companyInfo.phone, companyId: this.props.companyId})}>
                                    <BusinessInfoItem
                                        item="電話番号"
                                        value={this.state.companyInfo.phone}
                                        type="text"
                                    />
                                </TouchableOpacity>
                                <TouchableOpacity onPress={() => Actions.push("chooseprofileimage", {companyId: this.props.companyId, homeImage: this.state.homeImage})}>
                                    <BusinessInfoItem
                                        item="プロフィール画像"
                                        type="image"
                                    />
                                </TouchableOpacity>
                                <View style={[margin.mt4, {zIndex: 2000}]}>
                                    <Text>企業区分</Text>
                                    {
                                        this.state.groups != null ?
                                        <RNPickerSelect
                                            items={this.state.groups}
                                            style={ pickerSelectStyles }
                                            onValueChange={value => { this.changeVal(value, 'group') }}
                                            value={this.state.companyInfo.groupId}
                                            useNativeAndroidPickerStyle={false}
                                        />
                                        :
                                        null
                                    }
                                </View>
                                <View style={[margin.mt4, {zIndex: 1000}]}>
                                    <Text>見込み状況</Text>
                                    <RNPickerSelect
                                        placeholder={placeholder}
                                        items={predict}
                                        style={ pickerSelectStyles }
                                        onValueChange={value => { this.changeVal(value, 'predict') }}
                                        value={this.state.companyInfo.predictTypeId}
                                        useNativeAndroidPickerStyle={false}
                                    />
                                </View>
                                <View style={[{display: 'flex', flexDirection: 'row', alignItems: 'flex-end'} , margin.px4, margin.mt8]}>
                                    <TouchableOpacity style={{display: 'flex', alignItems: 'center' , width: '33%'}} onPress={() => Actions.push("galleryregister", {companyId: this.props.companyId})}>
                                        <Feather
                                            name='camera'
                                            size={34}
                                            color='black'
                                        />
                                        <Text style={[fonts.size16, margin.mt4]}>ギャラリー</Text>
                                    </TouchableOpacity>
                                    
                                    <TouchableOpacity style={{display: 'flex', alignItems: 'center' , width: '33%'}} onPress={() => Actions.push("parkregister", {companyId: this.props.companyId, title: '納品場所', type: 1,
                                        lat: this.state.companyInfo.mainLat != null && this.state.companyInfo.mainLat != "null" ? parseFloat(this.state.companyInfo.mainLat): null, 
                                        lon: this.state.companyInfo.mainLon != null && this.state.companyInfo.mainLon != "null" ? parseFloat(this.state.companyInfo.mainLon): null})}>
                                        <MapIcon width={37} height={37} />
                                        <Text style={[fonts.size16, margin.mt4]}>納品場所</Text>
                                    </TouchableOpacity>
                                    
                                    <TouchableOpacity style={{display: 'flex', alignItems: 'center' , width: '33%'}} onPress={() => Actions.push("parkregister", {companyId: this.props.companyId, title: '駐車場', type: 2,
                                        lat: this.state.companyInfo.parkLat != null && this.state.companyInfo.parkLat != "null" ? parseFloat(this.state.companyInfo.parkLat) : this.state.companyInfo.mainLat != null && this.state.companyInfo.mainLat != "null" ? parseFloat(this.state.companyInfo.mainLat): null, 
                                        lon: this.state.companyInfo.parkLon != null && this.state.companyInfo.parkLon != "null" ? parseFloat(this.state.companyInfo.parkLon) : this.state.companyInfo.mainLon != null && this.state.companyInfo.mainLon != "null" ? parseFloat(this.state.companyInfo.mainLon): null})}>
                                        <FontAwesome5
                                            name='car-alt'
                                            size={34}
                                            color='black'
                                        />
                                        <Text style={[fonts.size16, margin.mt4]}>駐車場</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            :
                            null
                        }
                        
                    </ScrollView>
                    <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
                </Content>
            </Container>
        );
    }
}

BusinessEditScreen.navigationOptions = {
    header: null
}
const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
      fontSize: normalize(15),
      paddingVertical: normalize(15),
      paddingHorizontal: normalize(24),
      borderWidth: 1,
      borderColor: '#c3c3c3',
      borderRadius: 8,
      color: '#78849E',
      paddingRight: normalize(30), // to ensure the text is never behind the icon
    },
    inputAndroid: {
      fontSize: normalize(15),
      
      paddingVertical: normalize(12),
      borderBottomWidth: 1,
      borderColor: '#c3c3c3',
      borderRadius: 8,
      color: '#78849E',
      paddingRight: 30, // to ensure the text is never behind the icon
    },
});