import React from 'react';
import { StatusBar, View } from 'react-native';
import { Container , Spinner, Text } from 'native-base';
import { Actions } from 'react-native-router-flux';
import { connect } from "react-redux";
import { splash } from './../assets/styles';
import store from './../store/configuteStore';

class SplashScreen extends React.Component {
    constructor(props){
        super(props);
        this.CheckUserLogin = this.CheckUserLogin.bind(this);
        this.moveAnotherPage = this.moveAnotherPage.bind(this);
    }
    componentDidMount() {
        let _self = this
        let _timer = setInterval(function() {
            if(_self.CheckUserLogin()) {
                clearInterval(_timer)
                Actions.reset('root')
            }
            else {
                clearInterval(_timer)
                Actions.reset('login')
            }
        }, 1000)
    }
    CheckUserLogin() {
        try {
            let token = this.props.user.token;
            console.log('token--', token)
            return token === null || token === ''
                ? false
                : true;
        } catch(error) {
            console.log(error)
        }
    }
    moveAnotherPage(pageName) {
        Actions.reset(pageName)
    }
    render() {
        if( this.props.rehydrated === true ) {
            if( !(this.props.user.token === null || this.props.user.token === '') ) {
                this.moveAnotherPage('root')
                console.log('root----')
            }
            else {
                this.moveAnotherPage('login')
                console.log('login----')
            }
            //Actions.reset('root')
        }
        return (
                    <Container style={splash.splashContainer}>
                        <StatusBar backgroundColor="white" barStyle="light-content"/>
                        <Text style={splash.splashText}>少々お待ちください</Text>
                        <Spinner color={'white'} />
                    </Container>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        user : state.user,
        rehydrated : state.rehydrated
    }
}
export default connect(mapStateToProps , null)(SplashScreen);