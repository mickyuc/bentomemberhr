import React from 'react';
import { StyleSheet, Text, View,  Image, Dimensions, TouchableOpacity, FlatList} from 'react-native';
import MainHeader from '../components/MainHeader';
import { shared, normalize, fonts, getScreenWidth } from '../assets/styles';
import { Container, Content } from 'native-base';
import { Feather } from '@expo/vector-icons';
import { getImage, updateImage } from '../api';
import { showToast } from '../shared/global';
import store from '../store/configuteStore';
import Spinner_bar from 'react-native-loading-spinner-overlay';
import { Actions } from 'react-native-router-flux';
import { _e } from '../lang';
import Layout from '../constants/Layout';
import * as Permissions from 'expo-permissions';
import * as ImagePicker from 'expo-image-picker';
let pageTitle = 'ギャラリー登録'
export default class GalleryRegister extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            uri: '',
            images: [
            ],
            loaded: true
        };
    }

    refresh(){
        let token = store.getState().user.token;
        this.setState({loaded: false});
        getImage(this.props.companyId, token)
        .then((response) => {
            this.setState({loaded: true});
            if(response.status) {
                this.setState({images: response.data})
            }
            else {
                showToast(response.msg);
            }
        })
        .catch((error) => {
            this.setState({loaded: true});
            showToast();
        })
    }

    componentDidMount(){
        this.refresh();
    }

    _pickImage = async () => {
        const {
            status: cameraRollPerm
        } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    
        // only if user allows permission to camera roll
        if (cameraRollPerm === 'granted') {
            let pickerResult = await ImagePicker.launchImageLibraryAsync({
                allowsEditing: true,
                aspect: [3, 3],
                quality: 0.08
            });
        
            this._handleImagePicked(pickerResult);
        }
    }

    _takePhoto = async () => {
        const {
            status: cameraPerm
        } = await Permissions.askAsync(Permissions.CAMERA);
    
        const {
            status: cameraRollPerm
        } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
    
        // only if user allows permission to camera AND camera roll
        if (cameraPerm === 'granted' && cameraRollPerm === 'granted') {
            let pickerResult = await ImagePicker.launchCameraAsync({
                allowsEditing: true,
                aspect: [3, 3],
                quality: 0.08
            });
        
            this._handleImagePicked(pickerResult);
        }
    };

    _handleImagePicked = async pickerResult => {
        try {
            this.setState({ loaded: false });
            let token = store.getState().user.token;
            if (!pickerResult.cancelled) {
                updateImage(pickerResult.uri, this.state.images, this.props.companyId, token)
                .then(async (response) => {
                    if(response.status == false){
                        this.setState({loaded: true});
                        showToast(response.msg)
                        return;
                    }else{
                        this.refresh();
                    }
                })
                .catch((error) => {
                    this.setState({loaded: true});
                    showToast();
                });
            }
        } catch (e) {
            showToast('イメージのアップロードに失敗しました');
            this.setState({ loaded: true });
        }
    };

    load(){
        this.setState({loaded: false})
    }

    loadEnd(){
        this.setState({loaded: true})
    }

    render() {
        return (
        <Container style={[shared.container , {backgroundColor: '#f2f2f2'}]}>
            <MainHeader title={pageTitle}></MainHeader>
            <Content>
                <View style={{justifyContent: 'center', flexDirection: 'row', }}>
                    <TouchableOpacity style={styles.getImage} onPress={this._pickImage}>
                        <Feather name={"image"} color={"white"} size={30}/>
                        <Text style={{color: 'white'}}>追加</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.getImage} onPress={this._takePhoto}>
                        <Feather name={"camera"} color={"white"} size={30}/>
                        <Text style={{color: 'white'}}>撮影</Text>
                    </TouchableOpacity>
                </View>
                <View style={{paddingHorizontal: normalize(18)}}>
                    <FlatList
                        data={this.state.images}
                        renderItem={({ item }) => (
                            <View style={{ width: (getScreenWidth()-72)/3, flexDirection: 'column', margin: 6}}>
                                <Image style={styles.imageThumbnail} source={{ uri: Layout.serverLink + item }} resizeMode={"stretch"} resizeMethod={"resize"}
                                />
                            </View>
                        )}
                        //Setting the number of column
                        numColumns={3}
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
                <Spinner_bar color={'#27cccd'} visible={!this.state.loaded} textContent={""} overlayColor={"rgba(0, 0, 0, 0.5)"} />
            </Content>
        </Container>
        );
    }
}

const styles = StyleSheet.create({
    getImage: {
        width: normalize(92),
        height: normalize(92),
        backgroundColor: '#bababa',
        borderRadius: normalize(46),
        justifyContent: 'center',
        alignItems: 'center',
        margin: normalize(20)
    },
    imageThumbnail: {
        justifyContent: 'center',
        alignItems: 'center',
        height: 100,
    },
});
