import { BACKEND_URL } from './../constants/Global';
let base_url = BACKEND_URL;
let _headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
};

function createCall(path, data = null, token = null, headers = {}, method = 'POST') {
    const merged = {
        ..._headers,
        ...headers
    };

    let body = {};
    if (data) {
        body = {
            ...body,
            ...data,
        };
    }
    if (token) {
        body.api_token = token;
    }
    let strData = JSON.stringify(body);
    const requestBody = method == "POST" ? {
        method,
        headers: merged,
        body: strData
    } : {
        method,
        headers: merged
    };

    return fetch(
        `${base_url}${path}`, requestBody
    ).then((resp) => resp.json());
}

function makeUrlEncodedPost(path, formData, headers = {}, method = 'POST') {
    let _headers1 = {
        'Accept': 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
    };
    const merged = {
        ..._headers1,
        ...headers,
    };
    //let strData = JSON.stringify(data);

    const requestBody = method == "POST" ? {
        method,
        headers: merged,
        body: formData
    } : {
        method,
        headers: merged
    };
    return fetch(
        `${base_url}${path}`, requestBody
    ).then((resp) => resp.json());
}

function createcall1(path, data = null, token = null, headers = {}, method = 'POST') {
    const merged = {
        'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        ...headers
    };

    const requestBody = method == "POST" ? {
        method,
        headers: merged,
        body: data
    } : {
        method,
        headers: merged
    };

    return fetch(
        `${base_url}${path}`, requestBody
    ).then((resp) => resp.json());
}

function uploadImage(uri, originalImage, companyId, link, token){
    let uriParts = uri.split('.');
    let fileType = uriParts[uriParts.length - 1];
    
    let formData = new FormData();
    formData.append('uploads', {
        uri,
        name: `photo.${fileType}`,
        type: `image/${fileType}`,
        test: JSON.stringify(originalImage)
    });
    formData.append('originalImages', JSON.stringify(originalImage))
    formData.append('companyId', companyId)
    
    let options = {
        method: 'POST',
        body: formData,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer '+token
        },
    };
    return fetch(`${base_url}${link}`, options).then((resp) => resp.json());
}

/* user */
export function user_signin(username, password) {
    return createCall(
        'member/auth/login/SignIn',
        {username, password},
    );
}

export function sendPhoneConfirmCode(phone) {
    return createCall(
        'third/twilio/SendPhoneConfirmCode',
        {phone},
    );
}

export function loginPhone(phone, code) {
    return createCall(
        'member/auth/login-phone/ConfirmCode',
        {phone, code}
    );
}

export function confirmCode(phone, code) {
    return createCall(
        'third/twilio/register/ConfirmCode',
        {phone, code}
    );
}

export function resetPassword(phone, code, password) {
    return createCall(
        'member/auth/forget/ResetPassword',
        {phone, code, password}
    );
}

export function getState() {
    return createCall(
        'public/GetStates',
        null, null, {}, 'GET'
    );
}

export function getCities(stateId) {
    return createCall(
        'public/GetCites',
        {stateId}
    );
}

export function getListByMonth(regMonth, token) {
    return createCall(
        'member/after/GetListByMonth',
        {regMonth}, null, { Authorization: 'Bearer '+token }
    );
}

export function getListByDate(regDate, token) {
    return createCall(
        'member/after/GetListByDate',
        {regDate}, null, { Authorization: 'Bearer '+token }
    );
}

export function getCompanyList(state, cities, token) {
    return createCall(
        'member/GetCompanyList',
        {state, cities}, null, { Authorization: 'Bearer '+token }
    );
}

export function getNotificationList(token) {
    return createCall(
        'member/notification/GetList',
        null, null, { Authorization: 'Bearer '+token }, 'GET'
    );
}

export function getBusinessOffice(token) {
    return createCall(
        'public/GetBusinessOffice',
        null, null, { Authorization: 'Bearer '+token }, 'GET'
    );
}

export function getRouteList(businessOfficeId, token) {
    return createCall(
        'member/GetRouteList',
        {businessOfficeId}, null, { Authorization: 'Bearer '+token }
    );
}

export function getStates(token) {
    return createCall(
        'public/GetStates',
        null, null, { Authorization: 'Bearer '+token }, 'GET'
    );
}

export function getCites(stateId, token) {
    return createCall(
        'public/GetCites',
        {stateId}, null, { Authorization: 'Bearer '+token }
    );
}

export function getCompanyByLocation(stateId, cityId, token) {
    return createCall(
        'member/GetCompanyByLocation',
        {stateId, cityId}, null, { Authorization: 'Bearer '+token }
    );
}

export function getBusinessType() {
    return createCall(
        'public/GetBusinessType',
        {},
        null,
        {},
        'GET'
    );
}

export function getCompanyListBySearch(token, groupIds, stateId, cityIds, businessTypeIds, predictTypeId = [1]) {
    //
    /*
    groupIds = [1];
    stateId = 1;
    cityIds = [1];
    businessTypeIds = [1];
    predictTypeId = [1];
    */
    //

    let formBody = [] , encodedKey, encodedValue;

    for(let i = 0; i < cityIds.length; i ++) {
        encodedKey = encodeURIComponent('groupIds[]');
        encodedValue = encodeURIComponent(groupIds[i]);
        formBody.push(encodedKey + "=" + encodedValue);
    }

    encodedKey = encodeURIComponent('stateId');
    encodedValue = encodeURIComponent(stateId);
    formBody.push(encodedKey + "=" + encodedValue);

    for(let i = 0; i < cityIds.length; i ++) {
        encodedKey = encodeURIComponent('cityIds[]');
        encodedValue = encodeURIComponent(cityIds[i]);
        formBody.push(encodedKey + "=" + encodedValue);
    }

    for(let i = 0; i < businessTypeIds.length; i ++) {
        encodedKey = encodeURIComponent('businessTypeIds[]');
        encodedValue = encodeURIComponent(businessTypeIds[i]);
        formBody.push(encodedKey + "=" + encodedValue);
    }

    for(let i = 0; i < predictTypeId.length; i ++) {
        encodedKey = encodeURIComponent('predictTypeIds[]');
        encodedValue = encodeURIComponent(predictTypeId[i]);
        formBody.push(encodedKey + "=" + encodedValue);
    }

    formBody = formBody.join("&");

    return makeUrlEncodedPost(
        'member/GetCompanyList',
        formBody,
        { Authorization: 'Bearer '+token },
        'POST'
    );
}

export function getMessageDetail(id, token) {
    return createCall(
        'member/notification/Detail',
        {id},
        null,
        { Authorization: 'Bearer '+token }
    );
}

export function setMessageCheck(id, token) {
    createCall(
        'member/notification/Check',
        {id},
        null,
        { Authorization: 'Bearer '+token }
    );
}

export function deleteMessage(id, token) {
    return createCall(
        'member/notification/Delete',
        {id},
        null,
        { Authorization: 'Bearer '+token }
    );
}

export function createAfter(regDate, companyId, methodId, detail, businessTypeId, token) {
    return createCall(
        'member/after/Create',
        {regDate, companyId, methodId, detail, businessTypeId}, null, { Authorization: 'Bearer '+token }
    );
}

export function updateAfter(id, regDate, methodId, detail, businessTypeId, token) {
    return createCall(
        'member/after/Update',
        {id, regDate, methodId, detail, businessTypeId}, null, { Authorization: 'Bearer '+token }
    );
}

export function checkedAfter(id, token) {
    return createCall(
        'member/after/Checked',
        {id}, null, { Authorization: 'Bearer '+token }
    );
}

export function getCompanyDetail(companyId, token) {
    return createCall(
        'member/GetCompanyDetail',
        {companyId}, null, { Authorization: 'Bearer '+token }
    );
}
export function getLastActionDate(companyId, token) {
    return createCall(
        'member/GetLastActionDate',
        {companyId}, null, { Authorization: 'Bearer '+token }
    );
}

export function getCompanyGroups(token) {
    return createCall(
        'public/GetCompanyGroups',
        null, null, { Authorization: 'Bearer '+token }, 'GET'
    );
}

export function changeCompanyPark(companyId, parkLat, parkLon, token) {
    return createCall(
        'member/ChangeCompanyInformation',
        {companyId, parkLat, parkLon}, null, { Authorization: 'Bearer '+token }
    );
}

export function changeCompanyMain(companyId, mainLat, mainLon, token) {
    return createCall(
        'member/ChangeCompanyInformation',
        {companyId, mainLat, mainLon}, null, { Authorization: 'Bearer '+token }
    );
}

export function changeCompanyPredict(companyId, predictTypeId, token) {
    return createCall(
        'member/ChangeCompanyInformation',
        {companyId, predictTypeId}, null, { Authorization: 'Bearer '+token }
    );
}

export function changeCompanyTitle(companyId, companyTitle, token) {
    return createCall(
        'member/ChangeCompanyInformation',
        {companyId, companyTitle}, null, { Authorization: 'Bearer '+token }
    );
}

export function changeCompanyPhone(companyId, phone, token) {
    return createCall(
        'member/ChangePhone',
        {companyId, phone}, null, { Authorization: 'Bearer '+token }
    );
}

export function changeCompanyReceiveAddr(companyId, receiveAddress, token) {
    return createCall(
        'member/ChangeCompanyInformation',
        {companyId, receiveAddress}, null, { Authorization: 'Bearer '+token }
    );
}

export function changeCompanyGroup(companyId, groupId, token) {
    return createCall(
        'member/ChangeCompanyInformation',
        {companyId, groupId}, null, { Authorization: 'Bearer '+token }
    );
}

export function getMemberLog(companyId, token) {
    return createCall(
        'member/GetMemberLog',
        {companyId}, null, { Authorization: 'Bearer '+token }
    );
}

export function getImage(companyId, token) {
    return createCall(
        'member/GetImage',
        {companyId}, null, { Authorization: 'Bearer '+token }
    );
}

export async function updateImage(uri, originalImage, companyId, token){
    return uploadImage(uri, originalImage, companyId,'member/UpdateImage', token)
}

export function GetRoutes(routeIds, token) {
    return createcall1(
        'member/GetRoutes',
        routeIds,
        null,
        { Authorization: 'Bearer '+token }
    );
}

export function changeOrderStatus(id, status, token) {
    return createCall(
        'member/ChangeOrderStatus',
        {id, status},
        null,
        { Authorization: 'Bearer '+token }
    );
}

export function changePassword(currentPassword, newPassword, token) {
    return createCall(
        'member/ChangePassword',
        {currentPassword, newPassword}, null, { Authorization: 'Bearer '+token }
    );
}

export function changeRouteStatus(id, status, token) {
    return createCall(
        'member/ChangeRouteStatus',
        {id, status}, null, { Authorization: 'Bearer '+token }
    );
}

export function getRouteDetail(routeId, token) {
    return createCall(
        'member/GetRouteDetail',
        {routeId}, null, { Authorization: 'Bearer '+token }
    );
}

export function changeCompanyHomeImage(companyId, homeImage, token) {
    return createCall(
        'member/ChangeCompanyHomeImage',
        {companyId, homeImage}, null, { Authorization: 'Bearer '+token }
    );
}