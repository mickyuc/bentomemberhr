import { SET_USER } from './../actionTypes';
const initialState = {
    username: null,
    token: null,
    route_list: [],
    business_list: []
}

export default user = (state = initialState , action = {}) => {
    const { user } = action;
    switch (action.type) {
        case SET_USER:
            return {
                username: user.username,
                token: user.token,
                route_list: user.route_list,
                business_list: user.business_list
            }
            break;
        default:
            return state;
    }
}
