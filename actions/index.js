import * as actions from './../actionTypes';

export const setUser = (user) => ({
    type: actions.SET_USER,
    user: user
});

export const logOut = (state) => ({
    type: "USER_LOGOUT" ,
    state: state
})