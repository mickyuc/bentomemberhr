import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import * as React from 'react';
import { Platform, StatusBar, StyleSheet, View, Text, SafeAreaView } from 'react-native';
import {Root} from 'native-base';
import useCachedResources from './hooks/useCachedResources';
// import BottomTabNavigator from './navigation/BottomTabNavigator';
// import LinkingConfiguration from './navigation/LinkingConfiguration';
import Login from './screens/Login';
import PhoneLogin from './screens/PhoneLogin';
import ResetPwd from './screens/ResetPwd';
import ForgotPwd from './screens/ForgotPwd';
import EStyleSheet from 'react-native-extended-stylesheet';
import store from './store/configuteStore';
import {connect, Provider} from 'react-redux';
import { Router, Scene, Actions } from 'react-native-router-flux'

import SplashScreen from './screens/SplashScreen'
// screens
import HomeScreen from './screens/HomeScreen'

import BusinessCateScreen from './screens/BusinessCateScreen';
import BusinessLocationScreen from './screens/BusinessLocationScreen';
import BusinessLocationDScreen from './screens/BusinessLocationDScreen';
import BusinessTypeScreen from './screens/BusinessTypeScreen';
import BusinessLevelScreen from './screens/BusinessLevelScreen';
import CompanyListScreen from './screens/CompanyListScreen';
import CompanyMapScreen from './screens/CompanyMapScreen';

import MessageScreen from './screens/MessageScreen';
import MessageDetail from './screens/MessageDetail';
import MessageBoxScreen from './screens/MessageBoxScreen';
import RouteSelectScreen from './screens/RouteSelectScreen';
import TabBarIcon from './components/TabBarIcon';
import ClipScreen from './screens/ClipScreen';
import AfterCare from './screens/AfterCare';
import AfterCareDetail from './screens/AfterCareDetail';
import ShowAllClient from './screens/ShowAllClient';
import AddAfterStates from './screens/AddAfterStates';
import AddAfterCities from './screens/AddAfterCities';
import AfterCompanyList from './screens/AfterCompanyList';
import AddAfterDetail from './screens/AddAfterDetail';
import BusinessScreen from './screens/BusinessScreen';
import BusinessEditScreen from './screens/BusinessEditScreen';
import ParkRegister from './screens/ParkRegister';
import GalleryRegister from './screens/GalleryRegister';
import EditCompanyTitle from './screens/EditCompanyTitle';
import EditCompanyReceiveAddress from './screens/EditCompanyReceiveAddress';
import EditCompanyPhone from './screens/EditCompanyPhone';
import MapDetail from './screens/MapDetail';
import MapShow from './screens/MapShow';
import OrderStatusScreen from './screens/OrderStatusScreen';
import ChangePassword from './screens/ChangePassword';
import ChooseProfileImage from './screens/ChooseProfileImage';

// define variable
EStyleSheet.build({
  $primaryColor: '#3497FD', 
  $secondaryColor: '#3ACCE1',
  $thirdColor: '#FF9057',
  $successColor: '#62BC56',
  $darkColor: '#353a50',
  $smallTextColor: '#78849E',
  $bigTextColor: '#454F63'
});

console.disableYellowBox = true;
// const Stack = createStackNavigator();
export default function App(props) {
  const isLoadingComplete = useCachedResources();

  if (!isLoadingComplete) {
    return null;
  } else {

    const RouterWithRedux = connect()(Router);
    return (
      <Root>
          <Provider store={store}>
            <RouterWithRedux>
                  <Scene hideNavBar key="hidenav">
                    <Scene key="root" hideNavBar showLabel={false}>
                      <Scene
                        key="tabbar"
                        tabs
                        tabBarStyle={styles.tabBarStyle}>
                          <Scene key="location" title="ルート" type="SimpleLineIcons" name="location-pin" icon={TabBarIcon} hideNavBar={true} >
                            <Scene key="route_select_screen" component={RouteSelectScreen} hideNavBar={true} initial />
                          </Scene>
                          <Scene key="business" title="企業検索" type="Feather" name="search" icon={TabBarIcon} hideNavBar={true} >
                            <Scene key="business_cate_screen" component={BusinessCateScreen} hideNavBar={true} initial />
                          </Scene>
                          <Scene key="home" title="ホーム" type="SimpleLineIcons" name="home" icon={TabBarIcon} hideNavBar={true} initial >
                            <Scene key="home_screen" component={HomeScreen} hideNavBar={true} initial />
                          </Scene>
                          <Scene key="message" title="メッセージ" type="AntDesign" name="message1" icon={TabBarIcon} hideNavBar={true} >
                            <Scene key="message_screen" component={MessageScreen} hideNavBar={true} initial />
                          </Scene>
                          <Scene key="clip" title="クリップ" type="Entypo" name="attachment" icon={TabBarIcon} hideNavBar={true} >
                            <Scene key="clip_screen" component={ClipScreen} hideNavBar={true} initial />
                          </Scene>
                      </Scene>
                      <Scene key="business_location_screen" component={BusinessLocationScreen} />
                      <Scene key="business_location_d_screen" component={BusinessLocationDScreen} />
                      <Scene key="business_type_screen" component={BusinessTypeScreen} />
                      <Scene key="business_level_screen" component={BusinessLevelScreen} />
                      <Scene key="company_list_screen" component={CompanyListScreen} />
                      <Scene key="company_map_screen" component={CompanyMapScreen} />
                      <Scene key="message_detail" component={MessageDetail} />
                      <Scene key="aftercare" component={AfterCare}  />
                      <Scene key="aftercaredetail" component={AfterCareDetail}  />
                      <Scene key="showallclient" component={ShowAllClient}  />
                      <Scene key="addafterstates" component={AddAfterStates}  />
                      <Scene key="addaftercities" component={AddAfterCities}  />
                      <Scene key="aftercompanylist" component={AfterCompanyList}  />
                      <Scene key="addafterdetail" component={AddAfterDetail}  />
                      <Scene key="businessscreen" component={BusinessScreen}  />
                      <Scene key="businesseditscreen" component={BusinessEditScreen}  />
                      <Scene key="parkregister" component={ParkRegister}  />
                      <Scene key="galleryregister" component={GalleryRegister}  />
                      <Scene key="editcompanytitle" component={EditCompanyTitle}  />
                      <Scene key="editcompanyreceiveaddress" component={EditCompanyReceiveAddress}  />
                      <Scene key="editcompanyphone" component={EditCompanyPhone}  />
                      <Scene key="orderstatusscreen" component={OrderStatusScreen}  />
                      <Scene key="mapdetail" component={MapDetail}/>
                      <Scene key="mapshow" component={MapShow}/>
                      <Scene key="changepassword" component={ChangePassword}/>
                      <Scene key="chooseprofileimage" component={ChooseProfileImage}/>
                    </Scene>
                    <Scene key="phonelogin" component={PhoneLogin}  />
                    <Scene key="forgotpwd" component={ForgotPwd}  />
                    <Scene key="resetpwd" component={ResetPwd}  />
                    <Scene key="login" component={Login} />
                    <Scene key="splash" component={SplashScreen} initial />
                    <Scene key="phonelogin" component={PhoneLogin} hideNavBar={true}  />
                    <Scene key="forgotpwd" component={ForgotPwd} hideNavBar={true}  />
                    <Scene key="resetpwd" component={ResetPwd} hideNavBar={true}  />
                    <Scene key="login" component={Login} hideNavBar={true} />
                    <Scene key="splash" component={SplashScreen} hideNavBar={true} />
                    <Scene key="message_detail" component={MessageDetail} hideNavBar={true} />
                    
                  </Scene>
            </RouterWithRedux>
          </Provider>
      </Root>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  tabBarStyle: {
    backgroundColor: 'white',
    height: Platform.OS == 'ios'? 60 : 60,
    paddingBottom: 8
  }
});
