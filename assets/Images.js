const Images = {
    logo_img: require('./images/logo.png'),
    logo_bg_img: require('./images/login_bg.png'),
    company_img: require('./images/company.png'),
    setting_img: require('./images/setting.png'),
    business1_img: require('./images/business1.png'),
    business2_img: require('./images/business2.png'),
    business3_img: require('./images/business3.png'),
    business4_img: require('./images/business4.png'),
    check_circle_img: require('./images/check-circle.png'),
    list_icon: require('./images/list_icon.png'),
    car_alt_img: require('./images/car-alt.png'),
    map_img: require('./images/map.png'),
    camera_img: require('./images/camera.png'),
    building_img: require('./images/building.png'),
    business_img: require('./images/business.png')
}
export default Images;